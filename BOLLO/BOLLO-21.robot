*** Settings ***
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../DATA/body.txt
Resource          schemaJson.txt
Resource          ../KEYWORDS/checkDB.robot
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../DATA/body.txt
Resource          schemaJson.txt
Resource          ../KEYWORDS/checkDB.robot
*** Variables ***
${username}       dirfptst_103
${password}       dirfptst_103
${access_token}    ${EMPTY}
${merchantOrderId}    ${EMPTY}
${transactionID}    ${EMPTY}
${ctrID_str1}     ${EMPTY}
${requestId}      ${EMPTY}
${C_SER}          21
${DAUT_str}       ${EMPTY}
${amount}         ${3027}

*** Test Cases ***
LOGIN
    Set Suite Variable    ${Product}    TPAY
    Set Suite Variable    ${obuID}    1000119691
    CheckUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLogin    ${endpoint}    ${basePath_login}    ${username}    ${password}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

PRICING
    ${pricing_json}    evaluate    json.loads('''${body_bolloPricing}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_bolloPricing}    ${pricing_json}    ${access_token}    41.255646465    9.984656    ${Product}
    SchemaValidator    pricing    ${output.json()}
    ${merchantOrderId}    Set Variable    ${output.json()['merchantOrderId']}
    ${requestId}    Set Variable    ${output.json()['requestId']}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH13_LOG_PRE_BOL WHERE C_IDE_RIC='${requestId}'    1
    Set Suite Variable    ${merchantOrderId}    ${merchantOrderId}
    Set Suite Variable    ${requestId}    ${requestId}

ORDER
    ${order_json}    evaluate    json.loads('''${body_bolloOrder}''')    json
    Set to Dictionary    ${order_json}    merchantOrderId=${merchantOrderId}
    ${output}    PostRequest    ${endpoint}    ${basePath_bolloOrder}    ${order_json}    ${access_token}    41.255646465    9.984656    ${Product}
    SchemaValidator    order    ${output.json()}
    ${id_TRX}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH13_LOG_PRE_BOL WHERE C_IDE_RIC='${requestId}' and C_IDE_TRX='${transactionID}'    1
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    conferma
    #########

LOGINX
    Set Suite Variable    ${Product}    TPAYX
    Set Suite Variable    ${obuID}    1000096139
    Set Suite Variable    ${username}    3248971259
    CheckUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLoginX    ${endpoint}    ${basePath_loginX}    ${username}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

PRICINGX
    ${pricing_json}    evaluate    json.loads('''${body_bolloPricing}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_bolloPricingX}    ${pricing_json}    ${access_token}    41.255646465    9.984656    ${Product}
    SchemaValidator    pricing    ${output.json()}
    ${merchantOrderId}    Set Variable    ${output.json()['merchantOrderId']}
    ${requestId}    Set Variable    ${output.json()['requestId']}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH13_LOG_PRE_BOL WHERE C_IDE_RIC='${requestId}'    1
    Set Suite Variable    ${merchantOrderId}    ${merchantOrderId}
    Set Suite Variable    ${requestId}    ${requestId}

ORDERX
    ${order_json}    evaluate    json.loads('''${body_bolloOrder}''')    json
    Set to Dictionary    ${order_json}    merchantOrderId=${merchantOrderId}
    ${output}    PostRequest    ${endpoint}    ${basePath_bolloOrderX}    ${order_json}    ${access_token}    41.255646465    9.984656    ${Product}
    SchemaValidator    order    ${output.json()}
    ${id_TRX}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH13_LOG_PRE_BOL WHERE C_IDE_RIC='${requestId}' and C_IDE_TRX='${transactionID}'    1
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    conferma
    #########
