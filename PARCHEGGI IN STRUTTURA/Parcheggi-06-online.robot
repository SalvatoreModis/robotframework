*** Settings ***
Resource  LibraryDbParameters.txt
Resource  PathParcheggi.txt
Resource  Body_api.txt
#Resource  Check.txt
Library   FakerLibrary
Library  SSHLibrary
Library  KDDbatchrun.py
Library    databasconnect.py
Library    controllquerylist.py
Resource    Check_python.txt
Library    obu_con_pos_list.py
*** Variables ***
${C_SER}   06
${C_TIP_MOV}    A
${endpoint}       http://kdd.test.gcp.telepass.com:8080
@{sp}    N    S
@{caronte_conv}    N    S
@{bb_no_list}    N    S
*** Test Cases ***



PostRequestParking
#    Log to Console    ${KDD05}

    FOR    ${bbono}    IN    @{bb_no_list}
        FOR    ${caronte}    IN    @{caronte_conv}
            FOR    ${f_spl_pay}    IN    @{sp}
                #Log to Console    ${f_spl_pay}
                    Continue For Loop If  (("${f_spl_pay}"=="S" and "${bbono}"=="S"))
                    ${OBU}      LibraryDbParameters.GetObuCode    ${f_spl_pay}    ${C_SER}    ${C_TIP_MOV}    ${bbono}
                    ${CON}      LibraryDbParameters.GetCONCode    ${C_SER}    ${C_TIP_MOV}    ${f_spl_pay}    ${caronte}    ${bbono}
                    ${POS}      LibraryDbParameters.GetPOSCode    ${CON}


            ${ENTRY_DATE}    LibraryDbParameters.GetEntryDate
            ${ENTRY_TIME}    LibraryDbParameters.GetEntryTime
            ${EXIT_DATE}    LibraryDbParameters.GetExitDate    ${ENTRY_DATE}
            ${EXIT_TIME}    LibraryDbParameters.GetExitTime
            ${D_RIF}      LibraryDbParameters.GetD_RIFValue
            ${D_CON}    LibraryDbParameters.GetD_CONValue    ${D_RIF}    ${EXIT_DATE}


            ${checkin_json}    evaluate    json.loads('''${body_auth_park_on}''')    json
            Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
            Set to Dictionary    ${checkin_json}    agreementId    ${CON}
            Set to Dictionary    ${checkin_json}    deviceCode    ${OBU}
            Set to Dictionary    ${checkin_json}    serviceDate    ${ENTRY_DATE}
            Set to Dictionary    ${checkin_json}    serviceTime    ${ENTRY_TIME}
            ${output}    PostRequestParking    ${endpoint}    ${basePath_authorize}    ${checkin_json}

            Continue for loop if    "${output.json()['result']}"!= "0"
            ${trx}    Set Variable    ${output.json()['authorizationId']}


        Set Suite Variable    ${transactionID}    ${trx}

       # Controllare prenotazione transazione
       Check_KDD05_State    ${transactionID}    10
       # Log: verificare che tutto e' andato a buon fine
       Check_KDD01_Log    W1    06    %${CON}%${POS}%${OBU}%${ENTRY_DATE}%${ENTRY_TIME}%    %${transactionID}%


        ${checkout_json}    evaluate    json.loads('''${body_confirmauth_park_on}''')    json



        Set to Dictionary    ${checkout_json}    pointOfService    ${POS}
        Set to Dictionary    ${checkout_json}    agreementId    ${CON}
        Set to Dictionary    ${checkout_json}    deviceCode    ${OBU}
        Set to Dictionary    ${checkout_json}    serviceDate    ${EXIT_DATE}
        Set to Dictionary    ${checkout_json}    serviceTime    ${EXIT_TIME}
        Set to Dictionary    ${checkout_json}    entryAuthorizationId    ${transactionID}

        ${output}    PostRequestParking    ${endpoint}    ${basePath_authorize}    ${checkout_json}
        Continue for loop if    "${output.json()['result']}"!= "0"

        ${daut}    Set Variable    ${output.json()['authorizationId']}
        #Continue for loop if    "${output.json()['result']}"!= "0"

        Set Suite Variable    ${d_auth}    ${daut}
        ${d_auth}    Convert to String    ${d_auth}

        # Verificare corretta autorizzazione transazione
        Check_KAA01_State    ${d_auth}    S
        Check_KDD05_State    ${transactionID}    30
        # Log: verificare che tutto e' andato a buon fine
        Check_KDD01_Log    W1    06    %${CON}%${POS}%${OBU}%${EXIT_DATE}%${EXIT_TIME}%    %${d_auth}%

        ${checkacc_json}    evaluate    json.loads('''${body_account_park_on}''')    json

        Set to Dictionary    ${checkacc_json}    pointOfService    ${POS}
        Set to Dictionary    ${checkacc_json}    agreementId    ${CON}
        Set to Dictionary    ${checkacc_json}[transactions][0]    titleCode    ${OBU}
        Set to Dictionary    ${checkacc_json}[transactions][0]    entryTime    ${ENTRY_TIME}
        Set to Dictionary    ${checkacc_json}[transactions][0]    exitDate    ${EXIT_DATE}
        Set to Dictionary    ${checkacc_json}[transactions][0]    entryDate    ${ENTRY_DATE}
        Set to Dictionary    ${checkacc_json}[transactions][0]    exitTime    ${EXIT_TIME}
        Set to Dictionary    ${checkacc_json}[transactions][0]    authorizationId    ${d_auth}

        ${output}    PostRequestParking    ${endpoint}    ${basePath_account}    ${checkacc_json}

        # Log: verificare che tutto e' andato a buon fine
        Check_KDD01_Log    W2    06    %${CON}%${POS}%${OBU}%${ENTRY_DATE}%${ENTRY_TIME}%${EXIT_DATE}%${EXIT_TIME}%ON%     %"transactions":[]%
        # Verifica inserimento nella tabella dei movimenti da contabilizzare
        Check_KDD03_State    ${d_auth}    ${transactionID}    N



        BatchRun    KDD    kddc1j01
        # Lancio del batch di contabilizzazione e verifica dell'esito
        ${esito}    BatchRun    KDD    kddc1j01
        SHOULD BE EQUAL AS STRINGS  ${esito}  fine OK
        #Log to Console    ${esito}



        #Controlli a db per verificare la corretta contabilizzazione
        Check_KDD03_State    ${d_auth}    ${transactionID}    S
        Check_KAA01_State    ${d_auth}    N



        Check_KSB01_State    ${transactionID}    ${d_auth}


        IF    "${f_spl_pay}" == "N"
            Check_KCA01_State    ${d_auth}    ${transactionID}    ${D_CON}    ${C_TIP_MOV}    N    N    N    N    N
        ELSE
            Check_KCA15_State    ${c_ser}    ${d_auth}    ${transactionID}    ${C_TIP_MOV}    ${f_spl_pay}
        END

        Check_KDD05_State    ${transactionID}    40

        Check_FVDAZV_State    ${d_auth}    ${transactionID}    ${f_spl_pay}    ${caronte}

    #    dbConnection    CPDB32
   # Le seguenti query  permettono di verificare che la transazione e' stata contabilizzata
   #     Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='40'    1
   #     Row Count Is Equal To X    SELECT * FROM KDDA.TKDD03_MOV_DA_CON WHERE D_AUT = '${d_auth}' AND C_ESI IS NULL AND T_DES_ERR IS NULL AND D_ELA IS NOT NULL    1
   #     Disconnect from Database

            END
        END
    END