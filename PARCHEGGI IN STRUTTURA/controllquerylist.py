def check_kdd05_state(transactionid, f_sta_trx):
    querypt1 = 'SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX= \''\

    querypt2 = '\' AND F_STA_TRX= \'' \

    querypt3 = '\''

    query = querypt1 + transactionid + querypt2 + f_sta_trx + querypt3

    return query


def check_kdd01_log(c_ws, c_ser, t_pay_in_like, t_pay_out_like):


    query_kdd01_log = "SELECT * FROM KDDA.TKDD01_LOG_WS WHERE C_ESI = 'OK' AND C_WS = '%s' AND C_SER = '%s' AND T_PAY_IN LIKE '%s' AND T_PAY_OUT LIKE '%s'" %(c_ws,c_ser,t_pay_in_like,t_pay_out_like)

    #querypt1 = 'SELECT * FROM KDDA.TKDD01_LOG_WS WHERE C_ESI = \'OK\' AND C_WS =\''\

    #querypt2 = '\' AND C_SER = \''\

    #querypt3 = '\' AND T_PAY_OUT LIKE \'%' \

    #querypt4 = '%\''#

    #query = querypt1 + c_ws + querypt2 + c_ser + querypt3 + t_pay_out_like + querypt4

    return query_kdd01_log



def check_kdd03_state(d_auth, transactionid, batchkddj01lanciato):

    querypt1 = 'SELECT * FROM KDDA.TKDD03_MOV_DA_CON WHERE D_AUT = \'' \

    querypt2 = '\' AND D_ELA IS NULL AND C_ESI IS NULL AND T_DES_ERR IS NULL AND C_IDE_TRX IS NULL' \

    querypt3 = '\' AND D_ELA IS NOT NULL AND C_ESI IS NULL AND T_DES_ERR IS NULL AND C_IDE_TRX = \'' \

    querypt4 = '\''

    if batchkddj01lanciato == 'S':

        query = querypt1 + d_auth + querypt3 + transactionid + querypt4

    else:

        query = querypt1 + d_auth + querypt2

    return query






def check_kaa01_state(d_auth, d_con_is_null_or_not):

    querypt1 = 'SELECT * FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT = \''\

    querypt2 = '\' AND D_CON IS NULL'

    querypt3 = '\' AND D_CON IS NOT NULL'

    if d_con_is_null_or_not == 'S':

        query = querypt1 + d_auth + querypt2

    else:

        query = querypt1 + d_auth + querypt3

    return query




def check_ksb01_state(c_ide_txn,d_auth):

    querypt1 = 'SELECT * FROM KSBA.TKSB01_MOV WHERE C_IDE_TXN = \''\

    querypt2 = '\' AND C_IDE_AUT = \''\

    querypt3 = '\' AND D_CON = CURRENT DATE' \

    #querypt4 = '\''

    query = querypt1 + c_ide_txn + querypt2 + d_auth + querypt3

    return query

#def check_kca15_state(c_ide_txn,d_auth,c_sta_mov):

    #querypt1 = 'SELECT * FROM KCAA.TKCA15_MOV_NO_SAP WHERE C_IDE_TXN = \''\

    #querypt2 = '\' AND D_AUT = \''\

    #querypt3 = '\' AND C_STA_MOV = \''\

    #querypt4 = '\''

    #query = querypt1 + c_ide_txn + querypt2 + d_auth + querypt3 + c_sta_mov + querypt4

    #return query


def check_kca01_state(d_auth, c_ide_tx, d_con,c_tip_mov, batch_kcaj05_launched, batch_kcaj01_launched, batch_kcaj04_launched,
                    batch_kcaj07_launched, batch_kcaj0699_launched):

    query_start = "SELECT * FROM KCAA.TKCA01_MOV WHERE D_AUT = '%s' AND C_IDE_TXN = '%s' AND D_CON = '%s' AND C_TIP_MOV = '%s' AND T_PRD IS NOT NULL " % (d_auth, c_ide_tx, d_con,c_tip_mov)

    query_kcaj05 = "AND D_INV_SAP IS %s" % ("NOT NULL " if batch_kcaj05_launched == 'S' else "NULL ")

    query_kcaj01 = "AND I_CMS IS %s" % ("NOT NULL " if batch_kcaj01_launched == 'S' else "NULL ")

    query_kcaj04 = "AND D_REN_CON IS %s" % ("NOT NULL " if batch_kcaj04_launched == 'S' else "NULL ")

    query_kcaj07 = "AND D_REN_PER IS %s" % ("NOT NULL " if batch_kcaj07_launched == 'S' else "NULL ")

    query_kcaj0699 = "AND D_SIN IS %s" % ("NOT NULL " if batch_kcaj0699_launched == 'S' else "NULL ")


    query = query_start + query_kcaj05 + query_kcaj01 + query_kcaj04 + query_kcaj07 + query_kcaj0699

    return query


def check_kca15_state(c_ser,d_auth, c_ide_trx, c_tip_mov, f_spl_pay):

    query = "SELECT * FROM KCAA.TKCA15_MOV_NO_SAP WHERE C_SER = '%s' AND D_AUT = '%s' AND C_IDE_TXN = '%s'  AND C_TIP_MOV = '%s' AND F_SPL_PAY = '%s' AND T_PRD IS NOT NULL " % (c_ser, d_auth, c_ide_trx, c_tip_mov, f_spl_pay)

    return query


def check_fvdazv_state(d_auth,c_ide_txn,f_spl_pay,caronte):

    query_start = "SELECT * FROM FVDA.TFVDZV_PAG_ACQ WHERE D_AUT = '%s' and C_IDE_TXN = '%s' AND F_SPL_PAY = '%s' AND T_PRD IS NOT NULL" % (d_auth, c_ide_txn, f_spl_pay)
    query_tip_ser = " AND C_TIP_SER %s" % (" IN ('C','T')" if caronte == 'S' else "IS NULL")

    query = query_start + query_tip_ser

    return query

    #"AND C_TIP_SER '%s'" % (d_auth, c_ide_txn, f_spl_pay, "IS NULL " if caronte == 'N' else "IN ('C','T')")""
