def convenzione(c_ser, c_tip_mov, f_spl_pay, caronte, bbono):

    querystart = "SELECT C_CON FROM KDAA.TKDA05_TRX WHERE C_SER = '%s' AND F_STA_TRX = '80' AND C_TIP_MOV = '%s' AND F_SPL_PAY = '%s' " % (c_ser, c_tip_mov,f_spl_pay)

    query_caronte = "AND C_CON %s " % ("IN (SELECT VAL_DE FROM KDDA.VKDD02_PARM_GEN WHERE C_OGG = 'C_CON_TRAGH') " if caronte == 'S' else "NOT IN (SELECT VAL_DE FROM KDDA.VKDD02_PARM_GEN WHERE C_OGG = 'C_CON_TRAGH')")

    query_bbono = "AND C_PRO LIKE '%s' " % ("B%" if bbono == 'S' else "F%")

    query_end = " ORDER BY  D_INS DESC LIMIT 1"


    #querypt1 = 'SELECT C_CON FROM KDAA.TKDA05_TRX WHERE C_SER = \''\

    #querypt2 = '\' AND F_STA_TRX = \'80\' AND C_TIP_MOV = \'' \

    #querypt3 = '\' AND F_SPL_PAY = \'' \

    #querypt4 = 'AND C_CON IN (SELECT VAL_DE FROM KDDA.VKDD02_PARM_GEN WHERE C_OGG = \'C_CON_TRAGH\')' \

    #querypt5 = '\' AND C_PRO LIKE \'B%\' '\

    #querypt6 = '\' AND C_PRO LIKE \'F%\' ' \

    #querypt7 = ' ORDER BY D_INS DESC LIMIT 1'

    #if f_spl_pay == 'S':
    #    if caronte == 'S':
    #        if bbono == 'S':
    #            query = ""
    #        else:
    #            query = querypt1 + c_ser + querypt2 + c_tip_mov + querypt3 + f_spl_pay + querypt6 + querypt4 + querypt7
    #    else:
    #        if bbono == 'S':
    #            query = ""
    #        else:
    #            query = querypt1 + c_ser + querypt2 + c_tip_mov + querypt3 + f_spl_pay + querypt6 + querypt7
    #else:
    #    if caronte == 'S':
    #        if bbono == 'S':
    #            query = querypt1 + c_ser + querypt2 + c_tip_mov + querypt3 + f_spl_pay + querypt5 + querypt4 + querypt7
    #        else:
    #            query = querypt1 + c_ser + querypt2 + c_tip_mov + querypt3 + f_spl_pay + querypt6 + querypt4 + querypt7
    #    else:
    #        if bbono == 'N':
    #            query = querypt1 + c_ser + querypt2 + c_tip_mov + querypt3 + f_spl_pay + querypt5 + querypt7
    #        else:
    #            query = querypt1 + c_ser + querypt2 + c_tip_mov + querypt3 + f_spl_pay + querypt6 + querypt7

    query = querystart + query_bbono + query_caronte  + query_end

    return query


def obu(f_spl_pay, c_ser, c_tip_mov, bbono):

    querypt1 = 'SELECT C_SUP FROM KDAA.TKDA05_TRX WHERE C_SER = \'' \

    querypt2 = '\'  AND F_STA_TRX = \'80\' AND C_TIP_MOV = \'' \

    querypt3 = '\' AND F_SPL_PAY = \'' \

    querypt4 = '\' AND C_PRO LIKE \'B%\' ' \

    querypt5 = '\' AND C_PRO LIKE \'F%\' ' \

    querypt6 = 'ORDER BY D_INS DESC LIMIT 1'

    if f_spl_pay == 'S':
        if bbono == 'S':
            query = ""
        else:
            query = querypt1 + c_ser + querypt2 + c_tip_mov + querypt3 + f_spl_pay + querypt5 + querypt6
    else:
        if bbono == 'S':
            query = querypt1 + c_ser + querypt2 + c_tip_mov + querypt3 + f_spl_pay + querypt4 + querypt6
        else:
            query = querypt1 + c_ser + querypt2 + c_tip_mov + querypt3 + f_spl_pay + querypt5 + querypt6
    return query



def puntoerogazione(c_con):

    querypt1 = 'SELECT C_PUN_ERO_SER FROM KDAA.TKDA05_TRX WHERE C_CON = \''\

    querypt2 = '\' LIMIT 1'\

    query = querypt1 + c_con + querypt2

    return query



def c_ide_trx_parcheggi_offline(entry_date, exit_date, entry_time, exit_time, c_obu, c_con, c_pos):

    query_start = "SELECT C_IDE_TRX FROM KDDA.TKDD03_MOV_DA_CON WHERE D_MOV_ENT = '%s' AND D_MOV = '%s' AND O_MOV_ENT = '%s' AND O_MOV = '%s' AND C_SUP = '%s' AND C_CON = '%s' AND C_PUN_ERO_SER = '%s'" % (entry_date, exit_date,entry_time,exit_time,c_obu,c_con,c_pos)

    query_end = " AND C_ESI IS NULL AND T_DES_ERR IS NULL AND D_ELA IS NOT NULL AND C_IDE_TRX IS NOT NULL"

    query = query_start + query_end

    return query

def d_auth_parcheggi_offline(c_ide_trx):

    query = "SELECT D_AUT FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX = '%s'" % (c_ide_trx)

    return query

def exit_date(entry_date):

    query = "SELECT D_MOV_USC FROM KDAA.TKDA05_TRX WHERE D_MOV_ENT = '%s' AND D_MOV_USC IS NOT NULL AND F_STA_TRX = '70' AND D_INS > CURRENT DATE - 3 MONTHS ORDER BY RANDOM LIMIT 1" % (entry_date)

    return query

