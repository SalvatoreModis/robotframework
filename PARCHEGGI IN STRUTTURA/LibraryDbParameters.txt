*** Settings ***
Library           DateTime
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           RESTLibrary    ${endpoint}
Library           JSONLibrary
Library           DatabaseLibrary
Library           OperatingSystem
Library           String
Library           XML
Library           obu_con_pos_list.py

*** Variables ***
${dbApiModule}    ibm_db_dbi
&{usernameDB}     user06=cpdb06_modifier_atech    user17=cpdb17_modifier_atech    user18=cpdb18_modifier_atech    user19=cpdb19_modifier_atech    user28=cpdb28_modifier_atech    user29=cpdb29_modifier_atech    user32=cpdb32_modifier_atech
&{passwordDB}     pass06=LyFvPq3PbdzCteYb4Nsz    pass17=9pCAoTh9Mihh9xRuCeC9    pass18=hr9FqoeMdLV7FyCcXhd3    pass19=hr9Fqoe012V7FyCcXhd3    pass28=Ra3eRiEdaVRgtptKyqse    pass29=uJawxdV97WfijVmbXx7F    pass32=JKmbqiFwEekJdmWNy7je
&{dbHost}         host06=10.153.236.34    host17=10.153.236.22    host18=10.153.236.23    host19=cpdb19-node1.test.gcp.telepass.com    host28=10.153.236.24    host29=10.153.236.25    host32=10.153.236.27
${dbPort}         50000

*** Keywords ***
dbConnection
    [Arguments]    ${dbName}
    ${db}    Set Variable    ${dbName}
    IF    "${db}" == "CPDB06"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb06_modifier_atech    dbPassword=LyFvPq3PbdzCteYb4Nsz    dbHost=10.153.236.34    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB17"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb17_modifier_atech    dbPassword=9pCAoTh9Mihh9xRuCeC9    dbHost=10.153.236.22    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB18"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb18_modifier_atech    dbPassword=hr9FqoeMdLV7FyCcXhd3    dbHost=10.153.236.23    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB19"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb19_modifier_atech    dbPassword=hr9Fqoe012V7FyCcXhd3    dbHost=cpdb19-node1.test.gcp.telepass.com    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB28"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb28_modifier_atech    dbPassword=Ra3eRiEdaVRgtptKyqse    dbHost=10.153.236.24    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB29"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb29_modifier_atech    dbPassword=uJawxdV97WfijVmbXx7F    dbHost=10.153.236.25    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB32"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb32_modifier_atech    dbPassword=JKmbqiFwEekJdmWNy7je    dbHost=10.153.236.27    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB35"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb35_modifier_atech    dbPassword=tVg9rcvmNharCRcYUHgP    dbHost=10.153.237.227    dbPort=${dbPort}
    END



PostRequestParking
    [Arguments]    ${endpoint}    ${basepath}    ${body}
    Create Session  mysession    ${endpoint}
    ${response}=    POST On Session    mysession    ${basepath}   json=${body}
    Should Be Equal As Strings    ${response.status_code}    200
    [Return]    ${response}


GetCONCode

    [Arguments]    ${c_ser}    ${c_tip_mov}    ${f_spl_pay}    ${caronte}    ${bbono}
    dbConnection    CPDB32
    ${CON_QUERY}    obu_con_pos_list.convenzione    ${c_ser}    ${c_tip_mov}    ${f_spl_pay}    ${caronte}    ${bbono}
    IF    "${CON_QUERY}" != "${EMPTY}"
    ${CON}    Query    ${CON_QUERY}
    ${CON}    Convert to String    ${CON}[0][0]
    ELSE
    Skip
    END
    [Return]    ${CON}


GetObuCode

    [Arguments]    ${f_spl_pay}    ${c_ser}    ${c_tip_mov}    ${bbono}
    dbConnection    CPDB32
    ${OBU_QUERY}    obu_con_pos_list.obu    ${f_spl_pay}    ${c_ser}    ${c_tip_mov}    ${bbono}
    IF    "${OBU_QUERY}" != "${EMPTY}"
    ${OBU}    Query    ${OBU_QUERY}
    #La riga di codice seguente permette di recuperare un elemento in una tupla dentro una tupla
    ${OBU}    Evaluate    [x[0] for x in ${OBU}]
    ${OBU}    Convert to String    ${OBU}[0]
    ELSE
    #Log to Console    "Caso di test non disponibile al momento"
    Skip
    END
    [Return]    ${OBU}

GetPOSCode
    [Arguments]    ${CON}
    dbConnection    CPDB32
    ${POS_QUERY}    obu_con_pos_list.puntoerogazione    ${CON}
    IF    "${POS_QUERY}" != "${EMPTY}"
    ${POS}    Query    ${POS_QUERY}
    ${POS}    Convert to String    ${POS}[0][0]
    #Log to Console    ${POS}
    ELSE
    Skip
    #Log to Console    "Caso di test non disponibile al momento"
    END
    [Return]    ${POS}


GetEntryDate

    dbConnection    CPDB32
    ${ENTRY_DATE}    Query    SELECT D_MOV_ENT FROM KDAA.TKDA05_TRX WHERE D_MOV_ENT IS NOT NULL AND F_STA_TRX = '70' AND D_INS > CURRENT DATE - 3 MONTHS ORDER BY RANDOM LIMIT 1    false    true
    ${ENTRY_DATE}    Get from Dictionary    ${ENTRY_DATE}[0]    D_MOV_ENT
    ${ENTRY_DATE}    Convert to String    ${ENTRY_DATE}
    #${ENTRY_DATE}    Add time to date    ${ENTRY_DATE}    1 days    result_format=%Y-%m-%d
    #${ENTRY_DATE}    Convert to String    ${ENTRY_DATE}
    [Return]    ${ENTRY_DATE}

GetEntryTime

    dbConnection    CPDB32
    ${ENTRY_TIME}    Query    SELECT O_MOV_ENT FROM KDAA.TKDA05_TRX WHERE O_MOV_ENT IS NOT NULL AND F_STA_TRX = '70' ORDER BY RANDOM LIMIT 1    false    true
    ${ENTRY_TIME}    Get from Dictionary    ${ENTRY_TIME}[0]    O_MOV_ENT
    ${ENTRY_TIME}    Convert to String    ${ENTRY_TIME}
    #${ENTRY_TIME}    Subtract time from time    ${ENTRY_TIME}    5 minutes
    #${ENTRY_TIME}    Convert Time    ${ENTRY_TIME}    timer    exclude_milles=yes
    [Return]    ${ENTRY_TIME}

GetExitDate
    [Arguments]    ${ENTRY_DATE}
    dbConnection    CPDB32
    ${EXIT_DATE_QUERY}    obu_con_pos_list.exit_date    ${ENTRY_DATE}
    ${EXIT_DATE}    Query    ${EXIT_DATE_QUERY}    true    true
    ${EXIT_DATE}    Get from Dictionary    ${EXIT_DATE}[0]    D_MOV_USC
    ${EXIT_DATE}    Convert to String    ${EXIT_DATE}
    ${EXIT_DATE}    Add time to date    ${EXIT_DATE}    2 days    result_format=%Y-%m-%d
    #${ENTRY_DATE}    Convert to String    ${ENTRY_DATE}
    [Return]    ${EXIT_DATE}

GetExitTime

    dbConnection    CPDB32
    ${EXIT_TIME}    Query    SELECT O_MOV_USC FROM KDAA.TKDA05_TRX WHERE O_MOV_USC IS NOT NULL AND F_STA_TRX = '70' ORDER BY RANDOM LIMIT 1    false    true
    ${EXIT_TIME}    Get from Dictionary    ${EXIT_TIME}[0]    O_MOV_USC
    ${EXIT_TIME}    Convert to String    ${EXIT_TIME}
    ${EXIT_TIME}    Subtract time from time    ${EXIT_TIME}    1 minutes
    ${EXIT_TIME}    Convert Time    ${EXIT_TIME}    timer    exclude_milles=yes
    [Return]    ${EXIT_TIME}


GetD_RIFValue

    dbConnection    CPDB28
    ${D_RIF}    Query    SELECT VAL_DA FROM KCAA.VKCA02_PARM_GEN WHERE C_OGG = 'D_RIF'    false    true
    ${D_RIF}    Get from Dictionary    ${D_RIF}[0]    VAL_DA
    ${D_RIF}    Convert to String    ${D_RIF}
    ${D_RIF}    Convert Date    ${D_RIF}    result_format=%Y-%m-%d
    [Return]    ${D_RIF}


GetD_CONValue
    [Arguments]    ${D_RIF}    ${EXIT_DATE}
    IF    '${D_RIF}'>='${EXIT_DATE}'
    ${D_CON}    Set Variable    ${D_RIF}
    ELSE
    ${D_CON}    Set Variable    ${EXIT_DATE}
    END
    [Return]    ${D_CON}



BatchRun
    [Arguments]    ${HOST}    ${BATCH}
    ${Batch_output}    KDDbatchrun.batchrun    ${HOST}    ${BATCH}
    [Return]    ${Batch_output}


Get_c_ide_park_off

    dbConnection    CPDB32
    [Arguments]    ${ENTRY_DATE}    ${EXIT_DATE}    ${ENTRY_TIME}    ${EXIT_TIME}    ${OBU}    ${CON}    ${POS}
    ${C_IDE_TRX_PARC_OFF_QUERY}    obu_con_pos_list.c_ide_trx_parcheggi_offline    ${ENTRY_DATE}    ${EXIT_DATE}    ${ENTRY_TIME}    ${EXIT_TIME}    ${OBU}    ${CON}    ${POS}
    ${transactionID}    Query    ${C_IDE_TRX_PARC_OFF_QUERY}
    ${transactionID}    Evaluate    [x[0] for x in ${transactionID}]
    ${transactionID}    Convert to String    ${transactionID}[0]
    [Return]    ${transactionID}


Get_d_auth_park_ogg

    dbConnection    CPDB32
    [Arguments]    ${transactionID}
    ${D_AUTH_PARCHEGGI_OFF}    obu_con_pos_list.d_auth_parcheggi_offline    ${transactionID}
    ${d_auth}     Query    ${D_AUTH_PARCHEGGI_OFF}
    ${d_auth}    Evaluate    [x[0] for x in ${d_auth}]
    ${d_auth}    Convert to String    ${d_auth}[0]
    ${d_auth}    Convert Date    ${d_auth}    result_format=%Y-%m-%d-%H.%M.%S.%f
    [Return]     ${d_auth}



