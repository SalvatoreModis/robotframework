import ibm_db
import pandas as pd


def datidb(db):
    dbport = 50000
    if db == 'cpdb06':
        dbUsername = 'cpdb06_modifier_atech'
        dbPassword = 'LyFvPq3PbdzCteYb4Nsz'
        dbHost = '10.153.236.34'
        dbPort = dbport
    elif db == 'cpdb17':
        dbUsername = 'cpdb17_modifier_atech'
        dbPassword = '9pCAoTh9Mihh9xRuCeC9'
        dbHost = '10.153.236.22'
        dbPort = dbport
    elif db == 'cpdb18':
        dbUsername = 'cpdb18_modifier_atech'
        dbPassword = 'hr9FqoeMdLV7FyCcXhd3'
        dbHost = '10.153.236.23'
        dbPort = dbport
    elif db == 'cpdb19':
        dbUsername = 'cpdb19_modifier_atech'
        dbPassword = 'hr9Fqoe012V7FyCcXhd3'
        dbHost = '10.153.237.249'
        dbPort = dbport
    elif db == 'cpdb28':
        dbUsername = 'cpdb28_modifier_atech'
        dbPassword = 'Ra3eRiEdaVRgtptKyqse'
        dbHost = '10.153.236.24'
        dbPort = dbport
    elif db == 'cpdb29':
        dbUsername = 'cpdb29_modifier_atech'
        dbPassword = 'uJawxdV97WfijVmbXx7F'
        dbHost = '10.153.236.25'
        dbPort = dbport
    elif db == 'cpdb30':
        dbUsername = 'cpdb30_viewer_atech'
        dbPassword = 'qonzwXXuFvneu3UuTunH'
        dbHost = '10.153.236.26'
        dbPort = dbport
    elif db == 'cpdb32':
        dbUsername = 'cpdb32_modifier_atech'
        dbPassword = 'JKmbqiFwEekJdmWNy7je'
        dbHost = '10.153.236.27'
        dbPort = dbport
    else:
        print('db non presente')

    dbdata = {"user": dbUsername, "pwd": dbPassword, "host": dbHost, "dbPort": dbPort}
    return dbdata


def connessionedb(db):
    daticon = datidb(db)
    host = daticon['host']
    user = daticon['user']
    pwd = daticon['pwd']
    port = daticon['dbPort']
    connstring = 'DATABASE=' + db + ';HOSTNAME=' + host + ';PORT=' + str(
        port) + ';PROTOCOL=TCPIP;UID=' + user + ';PWD=' + pwd + ';'
    connection = ibm_db.connect(
        connstring,
        "", "")
    return connection


def queryexec(query, conn, n=1, **kwargs):
    stmt = ibm_db.exec_immediate(conn, query)
    dict = []
    for i in range(n):
        x = ibm_db.fetch_assoc(stmt)
        dict.append(x)
    tab = pd.DataFrame(dict)
    campi = kwargs.get('campi')
    if campi:
        tab = tab[campi]
    return tab

# conn = connessionedb('cpdb28')
# T = queryexec("SELECT COUNT(*) FROM KCCA.TKCC02_CON", conn)
# n = T['1'].values[0]
# T2 = queryexec(query="SELECT * FROM KCCA.TKCC02_CON", conn=conn, n=30)
