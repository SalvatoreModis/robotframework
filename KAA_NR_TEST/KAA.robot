*** Settings ***
Resource       ../Resources/base_url.robot
Resource       ../Resources/libraries.robot

*** Variables ***
&{headers}         Content-Type=application/json      Accept=application/json     X-Sistema-Chiamante=KDB


*** Keywords ***
###################### Keywords per Setup ######################
SetupImportoPositivo
    ${var}     Evaluate    "%.2f" % random.uniform(1,20)
    ${var1}    Evaluate    ${var}+random.uniform(-0.5,0.5)
    ${var1}    Evaluate    "%.2f" % $var1
    ${var}    Convert To Number   ${var}
    ${var1}   Convert To Number   ${var1}
    Set Suite Variable    ${amount}    ${var}
    Set Suite Variable    ${amount_new}    ${var1}

SetupImportoNegativo
    SetupImportoPositivo
    Set Suite Variable    ${amount}    -${amount}
    Set Suite Variable    ${amount_new}    -${amount_new}

SetupImportoPrenotazione
    [Arguments]    ${c_ser}
    SetupImportoPositivo
    dbconnection    CPDB17
    ${toll_conf}    query    SELECT C_TOLL_CONF FROM KAAA.TKAA05_MIN_STR WHERE C_SER =${c_ser} AND C_TOLL_CONF IS NOT null    false    true
    disconnect from database
    IF    "${toll_conf}"!="[]"
        ${toll_conf}    Get From Dictionary    ${toll_conf}[0]      C_TOLL_CONF
        IF    ${amount_new}>${amount}+${toll_conf}
            ${amount_new}    Evaluate    ${amount}+${toll_conf}-0.01
        END
    ELSE
        ${amount_new}    Set Variable    ${amount}
    END
    ${amount_new}    Evaluate    "%.2f" % $amount_new
    ${amount_new}    Convert To Number   ${amount_new}
    Set Suite Variable    ${amount_new}    ${amount_new}

SetupImportoNonNullo
    ${var}     Evaluate    "%.2f" % random.uniform(-10,20)
    ${var}    Set Variable If    ${var}==0    0.01    ${var}
    ${var1}    Evaluate    ${var}+random.uniform(-1,1)
    ${var1}    Evaluate    "%.2f" % $var1
    ${var}    Convert To Number   ${var}
    ${var1}   Convert To Number   ${var1}
    Set Suite Variable    ${amount}    ${var}
    Set Suite Variable    ${amount_new}    ${var1}


###################### Chiamate KAA e KBB ######################
KBB_plafond
    create session    mysession     ${base_url_KBB}
    ${endpoint}    convert to string    /TelepassKBB/kbb/plafonds
    &{req_body}    create dictionary    codiceSupporto=${c_sup}	    tipoSupporto=SM    timestampRiferimento=${null} 	codiceServizio=${c_ser}
    &{header}      create dictionary    Content-Type=application/json
    ${response}     POST on session      mysession   ${endpoint}    json=${req_body}    headers=${headers}

    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['result']}     0
    [Return]    ${response.content}



healthCheck
    create session  mysession   ${base_url_KAA}
    ${endpoint}=        convert to string    /TelepassKAA/kaa/healthcheck
    &{req_body}=        create dictionary    codiceServizio=12      codiceSupporto=1000093573       codiceTipoTitolo=SM
    set to dictionary   ${headers}     X-Sistema-Chiamante=KT

    ${response}=        POST on session      mysession   ${endpoint}    json=${req_body}    headers=${headers}

    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['result']}     00
    [Return]    ${response.content}


ottieniAutorizzazione
    [Documentation]    Input:   flagPrenotazione       importo
    [Arguments]     ${flagPrenotazione}     ${amount}
    ${timestamp}               get time    epoch
    ${timestampErogazione}      get current date    result_format=%d-%m-%Y-%H.%M.%S.%f
    ${dataPagamento}            get current date    result_format=%Y-%m-%d
    ${oraPagamento}             get current date    result_format=%H:%M:%S

    create session    mysession     ${base_url_KAA}
    ${endpoint}=    convert to string    /TelepassKAA/kaa/ottieniautorizzazione
    #${body_req}     evaluate    json.loads('''${reqBodyAut}''')     json
    ${body_req}     Create Dictionary    causaleAddebito=${null}  codiceApplicazione=KDB   codiceEnteErogatore=25571086   codiceRete=${null}  codiceTerminale=KDB   codiceTipoTitolo=SM   codiceValuta=EUR   identificativoMovimentoMerchant=${null}  identificativoSistema=KDB   indirizzoErogazione=${null}  insegnaEsercente=CANNE EST   sistemaChiamante=KDB   codicePuntoErogazione=44  codiceConvenzione=2  flagSCA=N   targa=AA000AA   nazioneTarga=I   latitudine=${null}  longitudine=${null}  saltaVerificheTitolo=false

    Set To Dictionary    ${body_req}     prenotazione               ${flagPrenotazione}
    Set To Dictionary    ${body_req}     codiceServizio             ${c_ser}
    Set To Dictionary    ${body_req}     importo                    ${amount}
    Set To Dictionary    ${body_req}     codiceSupporto             ${c_sup}
    Set To Dictionary    ${body_req}     identificativoTransazione    KDB_H783B10T-973H-${timestamp}
    Set To Dictionary    ${body_req}     timestampErogazione        ${timestampErogazione}
    Set To Dictionary    ${body_req}     timestampRichiesta         ${timestampErogazione}
    Set To Dictionary    ${body_req}     dataPagamento              ${dataPagamento}
    Set To Dictionary    ${body_req}     oraPagamento               ${oraPagamento}

    ${response}=    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}   200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['status']}     00
    [Return]    ${response.content}

Autorizzazione Secca
    [Documentation]    Input:   importo
    [Arguments]    ${amount}
    ${result}     ottieniautorizzazione    ${false}    ${amount}
    [Return]    ${result}

Prenotazione
    [Documentation]    Input:   importo
    [Arguments]    ${amount}
    ${result}     ottieniautorizzazione    ${true}    ${amount}
    [Return]    ${result}


confermaPrenotazione
    [Documentation]    Input:   importo
    [Arguments]    ${amount}
    create session    mysession     ${base_url_KAA}
    ${endpoint}=    convert to string    /TelepassKAA/kaa/confermaprenotazione

    ${body_req}     Create Dictionary    sistemaChiamante=KDB   identificativoMovimentoMerchant=${null}  codiceEnteErogatore=25571086   codiceTerminale=KDB   insegnaEsercente=CANNE EST   indirizzoErogazione=Insegna
    ${timestampErogazione}      get current date    result_format=%d-%m-%Y-%H.%M.%S.%f
    Set To Dictionary    ${body_req}     identificativoTransazione          ${transactionID}
    Set To Dictionary    ${body_req}     identificativoAutorizzazione       ${d_aut}
    Set To Dictionary    ${body_req}     importoConfermato                  ${amount}
    Set To Dictionary    ${body_req}     timestampErogazione                ${timestampErogazione}

    ${response}=    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['status']}     00
    [Return]    ${response.content}


modificaAutorizzazione
    [Documentation]    Input:   importo
    [Arguments]    ${amount}
    create session    mysession     ${base_url_KAA}
    ${endpoint}    convert to string    /TelepassKAA/kaa/modificaautorizzazione

    ${body_req}     create dictionary    codiceApplicazione=KMB     codiceServizio=12      codiceTipoTitolo=SM     saltaVerificheTitolo=false
    Set To Dictionary    ${body_req}     importo                        ${amount}
    Set To Dictionary    ${body_req}     codiceSupporto                 ${c_sup}
    Set To Dictionary    ${body_req}     identificativoAutorizzazione   ${d_aut}
    Set To Dictionary    ${body_req}     identificativoTransazione      ${transactionID}
    Set To Dictionary    ${body_req}     codiceServizio                 ${c_ser}

    ${response}    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['status']}     00
    [Return]    ${response.content}


ottieniStorno
    [Documentation]    Input:    importo    #identificativoTransazione   identificativoAutorizzazione
    [Arguments]     ${amount}      #${transactionID}   ${d_aut}
    create session    mysession     ${base_url_KAA}
    ${endpoint}=    convert to string    /TelepassKAA/kaa/ottienistorno

    ${body_req}     Create Dictionary    sistemaChiamante=KDB       identificativoMovimentoMerchant=${null}     codiceEnteErogatore=${null}        codiceTerminale=KDB      identificativoAutorizzazione=${null}
    Set To Dictionary    ${body_req}     identificativoTransazione      ${transactionID}
    Set To Dictionary    ${body_req}     importo                        ${amount}

    ${response}    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['status']}     00
    [Return]    ${response.content}


modificaPrenotazione
    [Arguments]       ${amount}
    create session    mysession     ${base_url_KAA}
    ${endpoint}    convert to string    /TelepassKAA/kaa/modificaautorizzazioneprenotata

    ${dataPagamento}           get current date    result_format=%Y-%m-%d     #2021-07-16
    ${oraPagamento}            get current date    result_format=%H:%M:%S     #09:33:07
    ${timestampErogazione}     get current date    result_format=%Y-%m-%d-%H.%M.%S.%f  #2022-02-18-14.01.02.000000
    ${body_req}     create dictionary    identificativoTransazione=${transactionID}    importo=${amount}	identificativoAutorizzazionePrecedente=${d_aut}	timestampErogazione=${timestampErogazione}	codiceApplicazione=KDB	codiceEnteErogatore=25571086	codiceServizio=${c_ser} 	codiceSupporto=${c_sup}	codiceTerminale=KDB	 codiceTipoTitolo=SM	codiceValuta=EUR	dataPagamento=${dataPagamento}	indirizzoErogazione=${null} 	insegnaEsercente=CANNE EST	oraPagamento=${oraPagamento}	timestampRichiesta=${timestampErogazione}	codicePuntoErogazione=${null}	latitudine=${null}	longitudine=${null}	targa=LB00031	nazioneTarga=ITA	codiceConvenzione=${null}	identificativoMovimentoMerchant=${null}

    ${response}    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['status']}     00
    [Return]    ${response.content}


confermaMultipla
    [Arguments]    ${amount}
        ${amount_new} 	Get Variable Value	${amount_new}	${amount}
        ${d_aut_new} 	Get Variable Value	${d_aut_new}	${d_aut}
        ${transactionID_new} 	Get Variable Value	${transactionID_new}	${transactionID}
    create session    mysession     ${base_url_KAA}
    ${endpoint}    convert to string    /TelepassKAA/kaa/confermamultipla

    ${amount}    Convert To Number    ${amount}
    ${amount1}    evaluate    "%.2f" % ($amount*random.uniform(0.2,0.5))
    ${var}    Convert To Number   ${amount1}
    ${amount2}    evaluate    "%.2f" % ($amount-$var)
    Set Suite Variable    @{listaImporti}    ${amount1}    ${amount2}

    ${timestampPagamento}     get current date    result_format=%Y-%m-%d-%H.%M.%S.%f  #2022-02-18-14.01.02.000000
    ${transazione1}      Create Dictionary    codiceEnteErogatore=25571086    identificativoTransazione=${transactionID_new}-001    codiceTerminale=KDB    importoConfermato=${amount1}    insegnaEsercente=CANNE EST    timestampPagamento=${timestampPagamento}    codicePuntoErogazione=${null}    latitudine=${null}    targa=${null}    nazioneTarga=${null}    longitudine=${null}    codiceConvenzione=${null}
    ${timestampPagamento}     get current date    result_format=%Y-%m-%d-%H.%M.%S.%f  #2022-02-18-14.01.02.000000
    ${transazione2}      Create Dictionary    codiceEnteErogatore=25571086    identificativoTransazione=${transactionID_new}-002    codiceTerminale=KDB    importoConfermato=${amount2}    insegnaEsercente=CANNE EST    timestampPagamento=${timestampPagamento}    codicePuntoErogazione=${null}    latitudine=${null}    targa=${null}    nazioneTarga=${null}    longitudine=${null}    codiceConvenzione=${null}
    ${listaTrx}    create list    ${transazione1}    ${transazione2}

    ${body_req}      Create Dictionary    sistemaChiamante=KDB    identificativoAutorizzazionePrincipale=${d_aut_new}    identificativoTransazionePrincipale=${transactionID_new}    listaAutorizzazioni=${listaTrx}
    ${response}    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['status']}     00
    [Return]    ${response.content}


ottieniAutRateizzazione
    [Documentation]
    [Arguments]
    ${dataPagamento}    get current date    result_format=%Y-%m-%d    #2021-12-27
    ${oraPagamento}     get current date    result_format=%H:%M:%S    #10:32:24
    ${timestamp}               get time    epoch
    ${timestampErogazione}     get current date    result_format=%d-%m-%Y-%H.%M.%S.%f
    ${some_uuid}   Uuid 4

    Set Suite Variable    ${amount}        ${42.5}
    Set Suite Variable    @{listaImporti}    ${27.5}    ${10.0}    ${5.0}

    ${timestampPagamento}     get current date    result_format=%d-%m-%Y-%H.%M.%S.%f  #27-12-2021-10.32.24.000725
    ${rata}        Create Dictionary    causaleAddebito=${null}    codiceEnteErogatore=${25271226}    codiceRete=${null}    codiceTerminale=1    codiceServizio=44    identificativoMovimentoMerchant=${null}    identificativoTransazione=KDB_A2A73426-${timestamp}_R1    importo=${27.5}    indirizzoErogazione=Foro Bonaparte, 61    insegnaEsercente=CANNE EST    dataPagamento=${dataPagamento}    oraPagamento=${oraPagamento}    sistemaChiamante=KDB    timestampErogazione=${timestampPagamento}    identificativoAutorizzazione=${null}    prenotazione=${false}    rata=${1}
    ${timestampPagamento}     get current date    result_format=%d-%m-%Y-%H.%M.%S.%f  #27-12-2021-10.32.24.000725
    ${spesa1}      Create Dictionary    causaleAddebito=${null}    codiceEnteErogatore=${25271226}    codiceRete=${null}    codiceTerminale=1    codiceServizio=44    identificativoMovimentoMerchant=${null}    identificativoTransazione=KDB_A2A73426-${timestamp}_S1    importo=${10.0}    indirizzoErogazione=Foro Bonaparte, 61    insegnaEsercente=CANNE EST    dataPagamento=${dataPagamento}    oraPagamento=${oraPagamento}    sistemaChiamante=KDB    timestampErogazione=${timestampPagamento}    identificativoAutorizzazione=${null}    prenotazione=${false}    rata=${null}
    ${timestampPagamento}     get current date    result_format=%d-%m-%Y-%H.%M.%S.%f  #27-12-2021-10.32.24.000725
    ${spesa2}      Create Dictionary    causaleAddebito=${null}    codiceEnteErogatore=${25271241}    codiceRete=${null}    codiceTerminale=1    codiceServizio=44    identificativoMovimentoMerchant=${null}    identificativoTransazione=KDB_A2A73426-${timestamp}_S2    importo=${5.0}    indirizzoErogazione=Foro Bonaparte, 61    insegnaEsercente=CANNE EST    dataPagamento=${dataPagamento}    oraPagamento=${oraPagamento}    sistemaChiamante=KDB    timestampErogazione=${timestampPagamento}    identificativoAutorizzazione=${null}    prenotazione=${false}    rata=${null}
    ${listaTrx}    create list    ${rata}    ${spesa1}    ${spesa2}

    ${body_req}      Create Dictionary    codicePuntoErogazione=${null}    latitudine=${null}    longitudine=${null}    targa=${null}    nazioneTarga=${null}    codiceConvenzione=${null}    flagSCA=${null}    flagRata=${null}    sistemaChiamante=KDB    codiceSupporto=${c_sup}    codiceTipoTitolo=SM    timestampRichiesta=${timestampErogazione}    causaleAddebito=BK    importoTotale=${42.5}    codiceServizio=44    insegnaEsercente=CANNE EST    dataPagamento=${dataPagamento}    oraPagamento=${oraPagamento}    codiceValuta=EUR    codiceEnteErogatore=${25271226}    codiceRete=${null}    codiceTerminale=1    timestampErogazione=${timestampErogazione}    indirizzoErogazione=Foro Bonaparte, 61    identificativoTransazione=KDB_A2A73426-${timestamp}    identificativoMovimentoMerchant=${some_uuid}    codiceApplicazione=KDB    identificativoSistema=KDB    identificativoAutorizzazione=${null}    prenotazione=${false}    identificativoAutorizzazionePrincipale=${null}    numeroRata=${null}    saltaVerificheTitolo=${null}    numeroRateTotali=${12}    nuovaAttivazione=true    numeroTransazioni=${3}    listTransazioni=${listaTrx}
    ${endpoint}    convert to string    /TelepassKAA/kaa/ottieniautrateizzazione
    create session    mysession     ${base_url_KAA}
    ${response}    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['status']}     00
    [Return]    ${response.content}


OttieniStornoRateizzazione
    [Arguments]
    create session    mysession     ${base_url_KAA}
    ${endpoint}    convert to string    /TelepassKAA/kaa/ottienistornorateizzazione
    ${body_req}      Create Dictionary    identificativoAutorizzazione=${d_aut}    identificativoTransazione=${transactionID}    sistemaChiamante=KDB    codicePuntoErogazione=${null}    latitudine=${null}    longitudine=${null}    targa=${null}    nazioneTarga=${null}    codiceConvenzione=${null}    flagSCA=${null}    flagRata=${null}    identificativoMovimentoMerchant=${null}    codiceEnteErogatore=${null}    codiceTerminale=${null}    importo=${null}
    ${response}    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['status']}     00
    [Return]    ${response.content}


