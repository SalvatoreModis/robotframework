*** Settings ***
Resource       ../Resources/base_url.robot
Resource       ../Resources/libraries.robot

*** Keywords ***
###################### Keyword per controlli a DB  ######################
ContrattoETitolo
    [Arguments]    ${c_sup}=${c_sup}
    dbconnection    CPDB06
    ${query}      query    SELECT C_CTR, C_PRO FROM EVDA.TETSJU_ATT_PNG WHERE C_OBU_ID=${c_sup}    false   true
    ${c_ctr}    Get from Dictionary    ${query}[0]    C_CTR
    ${titleID}    query    SELECT C_TIT FROM evda.TETSD2_TIT WHERE C_CTR =${c_ctr} AND C_TIP_TIT = 'SM'    false    true
    Disconnect From Database
    ${c_pro}    Get from Dictionary    ${query}[0]    C_PRO
    Set Suite Variable    ${c_ctr}      ${c_ctr}
    Set Suite Variable    ${c_pro}      ${c_pro}
    ${titleID_str}    Get from Dictionary    ${titleID}[0]    C_TIT
    Set Suite Variable    ${c_tit}    ${titleID_str}


ControlloVKBB01
    [Documentation]    Suite variables aggiuntive necessarie:
                ...     -   per modificaPrenotazione -    transactionID_new, d_aut_new
                ...     -   per confermaMultipla     -    lista_trx, lista_d_aut (con le transazioni legate alla principale) - vedi CreaListeTrxDaResponse
                ...     -   per abbonamento          -    lista_trx, lista_d_aut (con le transazioni legate alla principale) - vedi CreaListeTrxDaResponse
                ...    Input: tipoMovimento IN (prenotazione, autorizzazione, conferma, storno, modificaPrenotazione, confermaMultipla, modifica, abbonamento, stornoAbbonamento)
    [Arguments]     ${tipoMovimento}    ${c_ser}=${c_ser}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}    ${c_sup}=${c_sup}
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    dbConnection        CPDB29
    ${query}   query    SELECT C_SUP, C_TIT, C_CTR, C_TIP_OPE_PAG, C_SER, C_IDE_TXN, F_ADD_ACC, D_TMS_PAG, C_TIP_TIT, F_AUT, F_FAT, C_STA_MOV, D_CON, D_TMS_CON, C_IDE_CON, D_ADD, D_TMS_ADD, C_IDE_ADD, D_FAT, D_TMS_FAT, C_IDE_FAT FROM KBBA.VKBB01_MOV WHERE C_IDE_AUT ='${d_aut}'    false    true
    Disconnect From Database

    &{VKBB01}    Create Dictionary      C_SUP=${c_sup}    C_TIT=${c_tit}    C_CTR=${c_ctr}    C_TIP_OPE_PAG=B	  C_SER=${c_ser}    C_IDE_TXN=${transactionID}	  F_ADD_ACC=A     D_TMS_PAG=${null}	  C_TIP_TIT=SM	  F_AUT=S	  F_FAT=S	  C_STA_MOV=A${space}  	  D_CON=${null}	  D_TMS_CON=${null}	  C_IDE_CON=${null}	  D_ADD=${null}	  D_TMS_ADD=${null}	  C_IDE_ADD=${null}	  D_FAT=${null}	  D_TMS_FAT=${null}	  C_IDE_FAT=${null}
    IF       "${tipoMovimento}" == "prenotazione"
        Set Suite Variable     ${C_TIP_OPE_PAG}    B${space}
        Set Suite Variable     ${d_tms_pag}        ${null}
        Set To Dictionary    ${VKBB01}      C_TIP_OPE_PAG=${C_TIP_OPE_PAG}
    ELSE IF     "${tipoMovimento}" == "autorizzazione" or "${tipoMovimento}" == "modifica" or "${tipoMovimento}" == "conferma"
        Set Suite Variable     ${C_TIP_OPE_PAG}    P${space}
        ${d_tms_pag_query}    Get from Dictionary    ${query}[0]    D_TMS_PAG
        Should Be True    "${d_tms_pag_query}"!=${null}
        Set Suite Variable     ${d_tms_pag}    ${d_tms_pag_query}
        Set To Dictionary    ${VKBB01}      C_TIP_OPE_PAG=${C_TIP_OPE_PAG}     D_TMS_PAG=${d_tms_pag}
    ELSE IF     "${tipoMovimento}" == "storno"
        Set To Dictionary    ${VKBB01}      C_TIP_OPE_PAG=${C_TIP_OPE_PAG}     D_TMS_PAG=${d_tms_pag}    F_ADD_ACC=R
    END

    IF    "${tipoMovimento}"=="modificaPrenotazione"
        ControlloVKBB01    storno    ${c_ser}    ${d_aut}    ${transactionID}
        ControlloVKBB01    prenotazione        ${c_ser}    ${d_aut_new}    ${transactionID_new}
    ELSE IF    "${tipoMovimento}"=="confermaMultipla"
        ${d_aut_new} 	Get Variable Value	${d_aut_new}	${d_aut}
        ${transactionID_new} 	Get Variable Value	${transactionID_new}	${transactionID}
        ControlloVKBB01    storno    ${c_ser}    ${d_aut_new}    ${transactionID_new}
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut}    IN ZIP    ${lista_trx}    ${lista_d_aut}
            ControlloVKBB01    autorizzazione    ${c_ser}    ${d_aut}    ${transactionID}
        END
    ELSE IF    "${tipoMovimento}"=="abbonamento"
        dbConnection        CPDB29
        Row Count Is 0    SELECT * FROM KBBA.VKBB01_MOV WHERE C_IDE_AUT ='${d_aut}'
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut}    IN ZIP    ${lista_trx}    ${lista_d_aut}
            ControlloVKBB01    autorizzazione    ${c_ser}    ${d_aut}    ${transactionID}
        END
    ELSE IF    "${tipoMovimento}"=="stornoAbbonamento"
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
         FOR    ${transactionID}    ${d_aut}    IN ZIP    ${lista_trx}    ${lista_d_aut}
           ControlloVKBB01    storno        ${c_ser}    ${d_aut}    ${transactionID}
        END
    ELSE
        ${query0}    Create Dictionary    &{query[0]}
        Should Be Equal   ${VKBB01}    ${query0}
        ${rowcount}    get length    ${query}
        IF    ${rowcount}>1    #controllo in caso di storno
            ${query1}    Create Dictionary    &{query[1]}
            Set To Dictionary    ${VKBB01}    C_TIP_OPE_PAG=R${space}    D_TMS_PAG=${null}
            Set To Dictionary    ${query1}    D_TMS_PAG=${null}     #in caso di modifica prenotazione uno non è valorizzato
            Should Be Equal   ${VKBB01}    ${query1}
        END
    END


AzzeraSoglieContratto
    [Arguments]    ${c_ctr}=${c_ctr}
    dbconnection    CPDB29
    Execute Sql String    UPDATE KBBA.TKBB01_CRE_CTR SET I_CRE=0 WHERE C_CTR=${c_ctr} AND D_FIN_VAL > CURRENT date
    Disconnect From Database

CercaNomeSoglia
    [Arguments]    ${c_ser}=${c_ser}    ${c_sup}=${c_sup}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    DbConnection    CPDB30
    ${nomeSoglia}   query    SELECT DISTINCT vs.C_SER FROM KBBA.VKBB01_SOG vs JOIN KBBA.VKBBMP_SER_GRP vsg ON vsg.C_GRP_SER=vs.C_SER WHERE vsg.C_SER ='${c_ser}' AND (C_PRO='${c_pro}' OR C_CTR='${c_ctr}' ) AND vs.C_SER NOT IN ('XX') AND vs.D_FIN_VAL>CURRENT timestamp    false    true
    Disconnect From Database
    IF    len(${nomeSoglia})>0
        ${nomeSoglia}    Get From Dictionary    ${nomeSoglia}[0]    C_SER
    ELSE
        ${nomeSoglia}   Set Variable    ${null}
    END
    Set Suite Variable    ${nomeSoglia}     ${nomeSoglia}

SetupSoglieServizio
    [Arguments]    ${c_ser}=${c_ser}    ${c_sup}=${c_sup}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    SetupSogliaXX    ${c_ctr}
    CercaNomeSoglia    ${c_ser}    ${c_sup}
    IF    '${nomeSoglia}'!='${null}'
        dbconnection    CPDB29
        ${query}    query    select I_CRE,N_TRX,C_TIP_SOG from KBBA.TKBB01_CRE_CTR where C_CTR = '${c_ctr}' and c_tip_sog != '00' and D_FIN_VAL > current timestamp and C_SER = '${nomeSoglia}' order by c_tip_sog desc    false    true
        Disconnect From Database
        ${row_count}    Get Length    ${query}

        Set Suite Variable    ${cre_g}      0
        Set Suite Variable    ${cre_m}      0
        Set Suite Variable    ${cre_a}      0
        Set Suite Variable    ${ntrx_g}     0
        Set Suite Variable    ${ntrx_m}     0
        Set Suite Variable    ${ntrx_a}     0
        IF    ${row_count}>0
            ${cre_a}     Get from Dictionary    ${query}[0]    I_CRE
            ${ntrx_a}    Get from Dictionary    ${query}[0]    N_TRX
            IF    ${row_count}>1
                ${cre_m}     Get from Dictionary    ${query}[1]    I_CRE
                ${ntrx_m}    Get from Dictionary    ${query}[1]    N_TRX
                IF    ${row_count}>2
                    ${cre_g}     Get from Dictionary    ${query}[2]    I_CRE
                    ${ntrx_g}    Get from Dictionary    ${query}[2]    N_TRX
                END
            END
        END
        Set Suite Variable    ${cre_g0}      ${cre_g}
        Set Suite Variable    ${cre_m0}      ${cre_m}
        Set Suite Variable    ${cre_a0}      ${cre_a}
        Set Suite Variable    ${ntrx_g0}     ${ntrx_g}
        Set Suite Variable    ${ntrx_m0}     ${ntrx_m}
        Set Suite Variable    ${ntrx_a0}     ${ntrx_a}
    END

CheckSoglieServizio
    [Documentation]    tipoMovimento viene usato solo per controllare se è storno/stornoAbbonamento o no
    [Arguments]      ${amount}     ${tipoMovimento}    ${c_ctr}=${c_ctr}
    CheckSogliaXX        ${amount}    ${tipoMovimento}
    IF    '${nomeSoglia}'!='${null}'
        dbconnection    CPDB29
        ${query}    query    select I_CRE,N_TRX from KBBA.TKBB01_CRE_CTR where C_CTR = '${c_ctr}' and c_tip_sog != '00' and D_FIN_VAL > current timestamp and C_SER = '${nomeSoglia}' order by c_tip_sog desc    false    true
        Disconnect From Database

        ${row_count}    Get Length    ${query}

        Set Suite Variable    ${cre_g}      0
        Set Suite Variable    ${cre_m}      0
        Set Suite Variable    ${cre_a}      0
        Set Suite Variable    ${ntrx_g}     0
        Set Suite Variable    ${ntrx_m}     0
        Set Suite Variable    ${ntrx_a}     0
        IF    ${row_count}>0
            ${cre_a}     Get from Dictionary    ${query}[0]    I_CRE
            ${ntrx_a}    Get from Dictionary    ${query}[0]    N_TRX
             IF    "${tipoMovimento}"!="storno" and "${tipoMovimento}"!="stornoAbbonamento"
                ${cre_a}     evaluate    ${cre_a}-${amount}
                ${ntrx_a}    evaluate    ${ntrx_a}-1
            END
            Should Be Equal As Numbers    ${cre_a0}      ${cre_a}
            Should Be Equal As Numbers    ${ntrx_a0}     ${ntrx_a}
            IF    ${row_count}>1
                ${cre_m}     Get from Dictionary    ${query}[1]    I_CRE
                ${ntrx_m}    Get from Dictionary    ${query}[1]    N_TRX
                IF    "${tipoMovimento}"!="storno" and "${tipoMovimento}"!="stornoAbbonamento"
                    ${cre_m}     evaluate    ${cre_m}-${amount}
                    ${ntrx_m}    evaluate    ${ntrx_m}-1
                END
                Should Be Equal As Numbers    ${cre_m0}      ${cre_m}
                Should Be Equal As Numbers    ${ntrx_m0}     ${ntrx_m}
                IF    ${row_count}>2
                    ${cre_g}     Get from Dictionary    ${query}[2]    I_CRE
                    ${ntrx_g}    Get from Dictionary    ${query}[2]    N_TRX
                    IF    "${tipoMovimento}"!="storno" and "${tipoMovimento}"!="stornoAbbonamento"
                        ${cre_g}     evaluate    ${cre_g}-${amount}
                        ${ntrx_g}    evaluate    ${ntrx_g}-1
                    END
                    Should Be Equal As Numbers    ${cre_g0}      ${cre_g}
                    Should Be Equal As Numbers    ${ntrx_g0}     ${ntrx_g}
                END
            END
        END
    END

SetupSogliaXX
    [Arguments]    ${c_ctr}=${c_ctr}
    dbconnection    CPDB29
    ${crediti}  query    select I_CRE,N_TRX from KBBA.TKBB01_CRE_CTR where C_CTR = ${c_ctr} and c_tip_sog != '00' and D_FIN_VAL > current timestamp and C_SER = 'XX' order by c_tip_sog   false   true
    disconnect from database
    ${row_count}    Get Length    ${crediti}
    IF    ${row_count}>0
        ${i_cre0}    Get from Dictionary    ${crediti}[0]    I_CRE
    ELSE
        ${i_cre0}    Set Variable    ${0}
    END
    Set Suite Variable    ${i_cre0}     ${i_cre0}

CheckSogliaXX
    [Arguments]    ${amount}   ${tipoMovimento}    ${c_ctr}=${c_ctr}
    dbconnection    CPDB29
    ${crediti}  query    select I_CRE,N_TRX,C_TIP_SOG from KBBA.TKBB01_CRE_CTR where C_CTR = ${c_ctr} and c_tip_sog != '00' and D_FIN_VAL > current timestamp and C_SER = 'XX' order by c_tip_sog   false   true
    Disconnect From Database
    ${i_cre}    get from dictionary    ${crediti}[0]    I_CRE
        IF  "${tipoMovimento}"!="storno" and "${tipoMovimento}"!="stornoAbbonamento"
            ${i_cre}    evaluate    ${i_cre}-${amount}
        END
    should be equal as numbers    ${i_cre0}     ${i_cre}


ControlloRiskShield
    [Documentation]    tipoMovimento IN (prenotazione, autorizzazione, conferma, modifica, storno , modificaPrenotazione, confermaMultipla, abbonamento e stornoAbbonamento)
    [Arguments]     ${amount}    ${tipoMovimento}    ${c_ser}=${c_ser}    ${c_sup}=${c_sup}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    dbconnection    CPDB17
    ${flag_RSenabled}    query    SELECT VAL_CH FROM KAAA.VKAA02_PARM_GEN WHERE C_OGG LIKE 'RISKSHIELD_ENABLED'    false    true
    Disconnect From Database
    IF    "${flag_RSenabled}[0]"=="{'VAL_CH': 'TRUE${space*26}'}"
        ${F_INV}    Set Variable    S
    ELSE
        ${F_INV}    Set Variable    N
    END

    dbconnection    CPDB17
    ${flag_ServiceEnabled}    Row Count    SELECT * FROM KAAA.TKAA05_MIN_STR WHERE C_SER=${c_ser} AND F_INV_RS='N'      false
    Disconnect From Database
    IF    ${flag_ServiceEnabled}>0
        dbconnection    CPDB17
        Row Count Is 0       SELECT * FROM KAAA.TKAA08_LOG_RSK_SLD WHERE C_IDE_TRX ='${transactionID}'
        Disconnect From Database
    ELSE IF    ${flag_ServiceEnabled}==0
        dbconnection    CPDB17
        ${query}    query    SELECT C_TIP_MSG, C_RES_KAA, C_CTR, C_SUP, C_ERR, T_MSG, T_ECC, F_INV FROM KAAA.TKAA08_LOG_RSK_SLD WHERE C_IDE_TRX ='${transactionID}' ORDER BY D_INS DESC     false    true
        ${query_amount}    query    SELECT I_TRX FROM KAAA.TKAA08_LOG_RSK_SLD WHERE C_IDE_TRX ='${transactionID}' ORDER BY D_INS DESC     false    true
        Disconnect From Database
        IF    "${tipoMovimento}"=="autorizzazione" or "${tipoMovimento}" == "modifica" or "${tipoMovimento}" == "abbonamento"
            ${c_tip_msg}    set variable    AUTH
        ELSE IF     "${tipoMovimento}" == "prenotazione" or "${tipoMovimento}" == "modificaPrenotazione"
            ${c_tip_msg}    set variable    PRE
        ELSE IF     "${tipoMovimento}" == "conferma" or "${tipoMovimento}" == "confermaMultipla"
            ${c_tip_msg}    set variable    CONF
        ELSE IF     "${tipoMovimento}"=="storno" or "${tipoMovimento}" == "stornoAbbonamento"
            ${c_tip_msg}    set variable    CANC
        END
        &{TKAA08}    Create Dictionary    C_TIP_MSG=${c_tip_msg}		C_RES_KAA=OK		C_CTR=${c_ctr}		C_SUP=${c_sup}		C_ERR=${null}		T_MSG=${null}		T_ECC=${null}		F_INV=${F_INV}    #C_RES=OK     T_HNT=

        ${query}    Create Dictionary    &{query[0]}
        Should Be Equal   ${TKAA08}    ${query}

        ${I_TRX}    Get from Dictionary    ${query_amount}[0]    I_TRX
        Should Be Equal As Numbers    ${amount}    ${I_TRX}
    END




Controllo_TKAA01_TKAA06
    [Documentation]    Input: tipoMovimento IN (prenotazione, autorizzazione, conferma, modifica, storno, modificaPrenotazione, confermaMultipla, abbonamento, stornoAbbonamento)
                ...    Suite variables aggiuntive necessarie:
                ...     -   per modificaPrenotazione -    amount_new, transactionID_new e d_aut_new
                ...     -   per confermaMultipla     -    lista_trx e lista_d_aut e listaImporti  (con le transazioni legate alla principale)
                ...     -   per abbonamento          -    lista_trx e lista_d_aut e listaImporti  (con le transazioni legate alla principale)
    [Arguments]     ${tipoMovimento}    ${amount}=${amount}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}    ${c_sup}=${c_sup}    ${c_ser}=${c_ser}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    IF    "${tipoMovimento}"=="modificaPrenotazione"
        Controllo_TKAA01_TKAA06    storno    ${amount}    ${d_aut}    ${transactionID}
        ControlloTKAA01    prenotazione    ${amount_new}    ${d_aut_new}    ${transactionID_new}
        Run Keyword If    ("${c_pro}"=="FC" or "${c_pro}"=="FE" or "${c_pro}"=="FF")     ControlloTKAA06    prenotazione    ${amount_new}    ${d_aut_new}    ${transactionID_new}

    ELSE IF    "${tipoMovimento}"=="confermaMultipla"
        ${amount_new} 	Get Variable Value	${amount_new}	${amount}
        ${d_aut_new} 	Get Variable Value	${d_aut_new}	${d_aut}
        ${transactionID_new} 	Get Variable Value	${transactionID_new}	${transactionID}
        ControlloTKAA01    conferma    ${amount_new}    ${d_aut_new}    ${transactionID_new}
        Run Keyword If    ("${c_pro}"=="FC" or "${c_pro}"=="FE" or "${c_pro}"=="FF")     ControlloTKAA06    conferma    ${amount_new}    ${d_aut_new}    ${transactionID_new}
        #controllo D_AUT_PRI e N_RAT
        ${count_rate}    Get Length    ${lista_trx}
        dbConnection        CPDB17
        Row Count Is Equal To X    SELECT * FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut_new}' AND D_AUT_PRI='${d_aut_new}' AND N_RAT='${count_rate}' ORDER BY D_AUT DESC      1
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut}    ${amount}    IN ZIP    ${lista_trx}    ${lista_d_aut}    ${listaImporti}
            ControlloTKAA01    autorizzazione    ${amount}    ${d_aut}    ${transactionID}
            #controllo D_AUT_PRI e N_RAT
            dbConnection        CPDB17
            Row Count Is Equal To X    SELECT * FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' AND N_RAT IS NULL AND D_AUT_PRI='${d_aut_new}' ORDER BY D_AUT DESC      1
            Disconnect From Database
        END

    ELSE IF    "${tipoMovimento}"=="abbonamento"
        Controllo_TKAA01_TKAA06    autorizzazione        ${amount}    ${d_aut}    ${transactionID}
        dbConnection        CPDB17
        Row Count Is Equal To X    SELECT D_AUT_PRI,N_RAT FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' AND D_AUT_PRI='${d_aut}' AND N_RAT='${12}' ORDER BY D_AUT DESC      1
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut_i}    ${amount}    IN ZIP    ${lista_trx}    ${lista_d_aut}    ${listaImporti}
            ControlloTKAA01    autorizzazione    ${amount}    ${d_aut_i}    ${transactionID}
            #controllo D_AUT_PRI e N_RAT
            ${count_rate}    Set Variable If    ${amount}==${27.5}    ='1'    IS NULL
            dbConnection        CPDB17
            Row Count Is Equal To X    SELECT D_AUT_PRI,N_RAT FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut_i}' AND D_AUT_PRI='${d_aut}' AND N_RAT ${count_rate} ORDER BY D_AUT DESC     1
            Disconnect From Database
        END

    ELSE IF    "${tipoMovimento}"=="stornoAbbonamento"
        Set Suite Variable    ${i_pag_aut}      ${amount}    #serve per controllo in TKAA01
        Set Suite Variable    ${i_pag_conf}     ${amount}    #serve per controllo in TKAA01
        ControlloTKAA01    ${tipoMovimento}    ${amount}    ${d_aut}    ${transactionID}
        Run Keyword If    ("${c_pro}"=="FC" or "${c_pro}"=="FE" or "${c_pro}"=="FF")     ControlloTKAA06    ${tipoMovimento}
        #controllo D_AUT_PRI e N_RAT
        ${count}    Get Length    ${lista_trx}
        dbConnection        CPDB17
        Row Count Is Equal To X    SELECT D_AUT_PRI,N_RAT FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' AND D_AUT_PRI='${d_aut}' AND N_RAT=12 ORDER BY D_AUT DESC     1
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut_i}    ${amount}    IN ZIP    ${lista_trx}    ${lista_d_aut}    ${listaImporti}
            Set Suite Variable    ${i_pag_aut}      ${amount}    #serve per controllo in TKAA01
            Set Suite Variable    ${i_pag_conf}     ${amount}    #serve per controllo in TKAA01
            ControlloTKAA01    stornoAbbonamento    ${amount}    ${d_aut_i}    ${transactionID}
##            #controllo D_AUT_PRI e N_RAT
            ${count_rate}    Set Variable If    ${amount}==${27.5}    ='1'    IS NULL
            dbConnection        CPDB17
            Row Count Is Equal To X    SELECT * FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut_i}' AND D_AUT_PRI='${d_aut}' AND N_RAT ${count_rate} ORDER BY D_AUT DESC      1
            Disconnect From Database
        END

    ELSE
        ControlloTKAA01    ${tipoMovimento}    ${amount}    ${d_aut}    ${transactionID}
        Run Keyword If    ("${c_pro}"=="FC" or "${c_pro}"=="FE" or "${c_pro}"=="FF")     ControlloTKAA06    ${tipoMovimento}    ${d_aut}    ${transactionID}
    END


ControlloTKAA01
    [Arguments]     ${tipoMovimento}    ${amount}=${amount}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}    ${c_sup}=${c_sup}    ${c_ser}=${c_ser}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    #creo i dizionari di base
    &{TKAA01}           Create Dictionary    C_TIP_AUT=03		C_SER=${c_ser}    D_PAG=${null}		T_INS_ESE=CANNE EST    C_IDE_TXN_ESE=${transactionID}		C_TIT=${c_tit}		C_TIP_TIT=SM    C_SUP=${c_sup}${space*10}    C_CTR=${c_ctr}		N_PRG_RAG=${null}		F_STA_AUT=B		F_AUT=S		D_CON=${null}		C_ERR=${null}		T_ANO=${null}		F_FAT=S		C_PRO=${c_pro}		#D_PAG=2021-07-16
    #faccio le query a DB
    dbConnection        CPDB17
    ${query_01}             query    SELECT C_TIP_AUT, C_SER, D_PAG, T_INS_ESE, C_IDE_TXN_ESE, C_TIT, C_TIP_TIT, C_SUP, C_CTR, N_PRG_RAG, F_STA_AUT, F_AUT, D_CON, C_ERR,T_ANO,F_FAT,C_PRO FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' ORDER BY D_AUT DESC    false    true
    ${query_01_amount}     query    SELECT I_PAG_AUT, I_PAG_CONF FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' ORDER BY D_AUT DESC   false    true
    ${query_01_date}        query    SELECT D_CONF, D_STR FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' ORDER BY D_AUT DESC    false    true
    Disconnect From Database
    #setup D_CONF e D_STR
    ${d_conf}   get from dictionary    ${query_01_date}[0]     D_CONF
    ${d_str}    get from dictionary    ${query_01_date}[0]     D_STR
    Set Suite Variable    ${d_conf}     ${d_conf}
    Set Suite Variable    ${d_str}      ${d_str}
    #setup dizionario TKAA01 e controllo su D_CONF e D_STR
    IF       "${tipoMovimento}" == "prenotazione"
        Set Suite Variable    ${c_tip_aut}     03
        Set Suite Variable    ${f_sta_aut}     B
        Set Suite Variable    ${i_pag_aut}     ${amount}
        Set Suite Variable    ${i_pag_conf}    ${null}
    ELSE IF     "${tipoMovimento}" == "autorizzazione"
        Set Suite Variable    ${c_tip_aut}     01
        Set Suite Variable    ${f_sta_aut}     I
        Set Suite Variable    ${i_pag_aut}     ${amount}
        Set Suite Variable    ${i_pag_conf}    ${amount}
    ELSE IF     "${tipoMovimento}" == "conferma"
        Set Suite Variable    ${f_sta_aut}     P
        Set Suite Variable    ${i_pag_conf}    ${amount_new}

        Should Not Be Equal As Strings    ${d_conf}     ${null}
    ELSE IF    "${tipoMovimento}"=="storno" or "${tipoMovimento}"=="stornoAbbonamento"
        Set Suite Variable    ${f_sta_aut}     R

        Should Not Be Equal As Strings    ${d_str}     ${null}
    ELSE IF     "${tipoMovimento}" == "modifica"
        Set Suite Variable    ${f_sta_aut}     M
        Set Suite Variable    ${i_pag_aut}     ${amount_new}
        Set Suite Variable    ${i_pag_conf}    ${amount_new}
    END
    ${dataPagamento}      get current date    result_format=%Y-%m-%d
    Set To Dictionary     ${TKAA01}             C_TIP_AUT=${c_tip_aut}    F_STA_AUT=${f_sta_aut}     D_PAG=${dataPagamento}
    #trasformo la data nella query in stringa
    ${query_01}             Create Dictionary    &{query_01[0]}
    ${dataPagamento}    Convert To String    ${query_01['D_PAG']}
    set to dictionary    ${query_01}    D_PAG=${dataPagamento}
    #controllo TKAA01
    Should Be Equal    ${TKAA01}    ${query_01}
    ${query_01_amount}     Create Dictionary    &{query_01_amount[0]}
    ${i_pag_aut_query}     Get From Dictionary    ${query_01_amount}    I_PAG_AUT
    ${i_pag_conf_query}    Get From Dictionary    ${query_01_amount}    I_PAG_CONF
    Should Be Equal As Numbers    ${i_pag_aut}    ${i_pag_aut_query}
    IF    "${i_pag_conf}"!="None"
        Should Be Equal As Numbers    ${i_pag_conf}    ${i_pag_conf_query}
    ELSE
        Should Be Equal As Strings    ${i_pag_conf}    ${i_pag_conf_query}
    END


ControlloTKAA06
    [Arguments]     ${tipoMovimento}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}
    #creo i dizionari di base
    &{TKAA06}           Create Dictionary    C_WS_3PA=${null}    T_ECC=${null}       C_ESI=OK

    #setup C_WS_3PA
    IF       "${tipoMovimento}" == "prenotazione"
        Set Suite Variable    ${c_ws_3pa}      01
    ELSE IF     "${tipoMovimento}" == "autorizzazione"
        IF         ${amount}>0
            Set Suite Variable    ${c_ws_3pa}    04
        ELSE IF    ${amount}<0
            Set Suite Variable    ${c_ws_3pa}    06
        ELSE
            Set Suite Variable    ${c_ws_3pa}    nullo
        END
    ELSE IF     "${tipoMovimento}" == "conferma"
        IF    ${amount_new}==0
            Set Suite Variable    ${c_ws_3pa}    02
        ELSE
            Set Suite Variable    ${c_ws_3pa}    03
        END
    ELSE IF    "${tipoMovimento}"=="storno"
        IF         "${c_ws_3pa}"=="06"
            Set Suite Variable    ${c_ws_3pa}    04
        ELSE IF    "${c_ws_3pa}"=="02" or "${c_ws_3pa}"=="nullo"     #è 02 se ho fatto conferma nulla
            Set Suite Variable    ${c_ws_3pa}    nullo
        ELSE IF    "${c_ws_3pa}"=="03"
            Set Suite Variable    ${c_ws_3pa}    05
        ELSE IF    "${c_ws_3pa}"=="01"
            Set Suite Variable    ${c_ws_3pa}    02
        ELSE
            Set Suite Variable    ${c_ws_3pa}    06
        END
    ELSE IF     "${tipoMovimento}" == "modifica"
        ${var}     Set Variable    ${c_ws_3pa}
        IF    ${amount_new}<${amount}
            ${c_ws_3pa}     Set Variable    06
        ELSE IF    ${amount_new}==${amount}
            ${c_ws_3pa}     Set Variable    nullo
        ELSE
            ${c_ws_3pa}     Set Variable    04
        END
        IF    "${var}"=="nullo" and ${amount_new}!=0
            Set Suite Variable    ${c_ws_3pa}    ${c_ws_3pa}
        ELSE IF    "${var}"=="nullo" and ${amount_new}==0
            Set Suite Variable    ${c_ws_3pa}    nullo
        END
    ELSE IF    "${tipoMovimento}"=="stornoAbbonamento"
        Set Suite Variable    ${c_ws_3pa}    05
    END
    Set To Dictionary    ${TKAA06}    C_WS_3PA=${c_ws_3pa}

    IF      "${c_ws_3pa}"!="nullo"
        dbConnection        CPDB17
        ${queryBNL}    query    SELECT C_WS_3PA, T_ECC, C_ESI FROM KAAA.TKAA06_LOG_AUT_BNL WHERE D_AUT ='${d_aut}' ORDER BY D_INS DESC LIMIT 1    false    true
        Disconnect From Database
        ${queryBNL}    Create Dictionary    &{queryBNL[0]}
        Should Be Equal   ${TKAA06}    ${queryBNL}
        IF    "${tipoMovimento}"=="prenotazione" or "${tipoMovimento}"=="conferma"
            dbConnection        CPDB17
            ${N_IMP}        query    SELECT N_IMP FROM KAAA.TKAA06_LOG_AUT_BNL WHERE D_AUT ='${d_aut}' ORDER BY D_INS DESC LIMIT 1    false    true
            ${c_ide_aut}    query    SELECT C_IDE_AUT FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' ORDER BY D_AUT DESC LIMIT 1    false    true
            Disconnect From Database
            ${N_IMP}        Get from Dictionary    ${N_IMP}[0]    N_IMP
            ${c_ide_aut}    Get from Dictionary    ${c_ide_aut}[0]    C_IDE_AUT
            Should Be Equal As Strings    ${N_IMP}      ${c_ide_aut}
        END
    ELSE
        DbConnection    CPDB17
        Row Count Is 0    SELECT C_WS_3PA, T_ECC, C_ESI FROM KAAA.TKAA06_LOG_AUT_BNL WHERE D_AUT ='${d_aut}' ORDER BY D_INS DESC    false
        Disconnect From Database
    END


ControlloTKAA02
    [Arguments]    ${n_chiamate}=1    ${transactionID}=${transactionID}
    DbConnection    CPDB17
    Row Count Is Equal To X    SELECT * FROM KAAA.TKAA02_LOG_OPE_ETF WHERE C_TRA_ID='${transactionID}' AND T_ECC='' AND C_ESI_OPE='S'    ${n_chiamate}
    Disconnect From Database



CreaListeTrxDaResponse
    [Arguments]    ${response}    ${key_lista}=listaAutorizzazioni    ${key_trx}=codiceTransazioneMerchant    ${key_d_aut}=autorizzazioneId
    ${lista}    get from dictionary       ${response}     ${key_lista}
    ${lista_trx}    Create List
    ${lista_d_aut}    Create List
    FOR    ${x}    IN    @{lista}
        ${trx}    get from dictionary    ${x}    ${key_trx}
        ${d_aut}    get from dictionary    ${x}    ${key_d_aut}
        Append To List    ${lista_trx}    ${trx}
        Append To List    ${lista_d_aut}    ${d_aut}
    END
    Set Suite Variable    ${lista_trx}    ${lista_trx}
    Set Suite Variable    ${lista_d_aut}    ${lista_d_aut}




ControlloTKAA02_old
    ########## da migliorare il controllo a DB
    [Arguments]     ${tipoMovimento}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}
    #creo i dizionari di base
    &{TKAA02}      Create Dictionary    C_TRA_ID=${transactionID}    C_ESI_OPE=S    T_ECC=
    #faccio le query a DB
    dbConnection        CPDB17
    ${query_02}    query    SELECT C_TRA_ID, C_ESI_OPE, T_ECC FROM KAAA.TKAA02_LOG_OPE_ETF WHERE D_AUT ='${d_aut}' ORDER BY D_OPE DESC    false    true
    Disconnect From Database
    #controllo TKAA02
    ${query_02}    Create Dictionary    &{query_02[0]}
    Should Be Equal   ${TKAA02}    ${query_02}
