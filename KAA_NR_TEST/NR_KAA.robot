*** Settings ***
Resource    ../Resources/libraries.robot
Variables   ../Resources/base_url.robot
Resource    ../Resources/KAA.robot
Resource    ../Resources/checkDB.robot


*** Variables ***
@{c_obu}          1000026292    #1000115129     #        1000093573
@{c_ser_aut}        12
@{c_ser_aut_neg}    23
@{c_ser_pre}        18    21    #23    64     56    #38    40    57    63    26    35

${sleep_time}    2s

*** Test Cases ***
TC0
    [Tags]    testKAA
    FOR    ${c_sup}    IN    @{c_obu}
        FOR    ${c_ser}             IN    @{c_ser_aut}
            SetupImportoPositivo
            CicloAutSecca    ${c_sup}    ${c_ser}

            Set Suite Variable    ${amount}    ${0}
            Set Suite Variable    ${amount_new}    ${0.01}
            CicloAutSecca    ${c_sup}    ${c_ser}

            Set Suite Variable    ${amount}    ${0.01}
            Set Suite Variable    ${amount_new}    ${0}
            CicloAutSecca    ${c_sup}    ${c_ser}
        END
        FOR    ${c_ser}             IN    @{c_ser_aut_neg}
            SetupImportoNegativo
            CicloAutSecca    ${c_sup}    ${c_ser}
        END
        FOR    ${c_ser}             IN    @{c_ser_pre}
            SetupImportoPrenotazione    ${c_ser}
            CicloPrenotazione        ${c_sup}    ${c_ser}
        END
    SetupImportoPrenotazione    33
    CicloSkipass        ${c_sup}    33

    CicloATM      ${c_sup}    44
    END



*** Keywords ***
CicloAutSecca
    [Arguments]    ${c_sup}    ${c_ser}
    Set Suite Variable    ${c_sup}     ${c_sup}
    Set Suite Variable    ${c_ser}          ${c_ser}
    ## setup controlli a DB ##
    ContrattoETitolo
    SetupSoglieServizio

    #################### Autorizzazione  ####################
    ${res_body_I}     Autorizzazione Secca      ${amount}

    #dati transazione
    ${res_body_I}   convert string to json    ${res_body_I}
    Set Suite Variable    ${transactionID}       ${res_body_I['codiceTransazioneMerchant']}
    Set Suite Variable    ${d_aut}    ${res_body_I['autorizzazioneId']}

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     autorizzazione
    #controllo CPDB17
    Controllo_TKAA01_TKAA06     autorizzazione
    ControlloTKAA02    1
    ControlloRiskShield    ${amount}   autorizzazione
    ## controlli TKBB01
    CheckSoglieServizio    ${amount}   autorizzazione

    #################### Modifica autorizzazione  ####################
    ${res_body_M}     ModificaAutorizzazione    ${amount_new}

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     modifica
    #controllo CPDB17
    Controllo_TKAA01_TKAA06     modifica
    ControlloRiskShield    ${amount_new}   modifica
    ControlloTKAA02    2
    ## controlli TKBB01
    CheckSoglieServizio    ${amount_new}   modifica

    #################### Storno  ####################
    ${res_body_S}     ottieniStorno    ${amount_new}

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     storno
    #controllo CPDB17
    Controllo_TKAA01_TKAA06     storno
    ControlloRiskShield    ${amount_new}   storno
    ControlloTKAA02    3
    ## controlli TKBB01
    CheckSoglieServizio    ${amount_new}   storno

CicloPrenotazione
    [Arguments]    ${c_sup}    ${c_ser}
    Set Suite Variable    ${c_sup}     ${c_sup}
    Set Suite Variable    ${c_ser}          ${c_ser}
     ## setup controlli a DB ##
    ContrattoETitolo
    SetupSoglieServizio

    #################### Prenotazione ####################
    ${res_body_P}     Prenotazione      ${amount}

    #dati transazione
    ${res_body_P}   convert string to json    ${res_body_P}
    Set Suite Variable    ${transactionID}       ${res_body_P['codiceTransazioneMerchant']}
    Set Suite Variable    ${d_aut}    ${res_body_P['autorizzazioneId']}

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     prenotazione
    ## controllo CPDB17
    Controllo_TKAA01_TKAA06     prenotazione
    ControlloRiskShield    ${amount}   prenotazione
    ControlloTKAA02    1
    ## controlli TKBB01
    CheckSoglieServizio    ${amount}   prenotazione

    #################### Conferma ####################
    ${res_body_C}          ConfermaPrenotazione    ${amount_new}

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     conferma
    ## controllo CPDB17
    Controllo_TKAA01_TKAA06     conferma
    ControlloRiskShield    ${amount_new}   conferma
    ControlloTKAA02    2
    ## controlli TKBB01
    CheckSoglieServizio    ${amount_new}   conferma

    #################### Storno ####################
    ${res_body_R}       OttieniStorno    ${amount_new}

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     storno
    ## controllo CPDB17
    Controllo_TKAA01_TKAA06     storno
    ControlloRiskShield    ${amount_new}   storno
    ControlloTKAA02    3
    ## controlli TKBB01
    CheckSoglieServizio    ${amount_new}   storno

CicloSkipass
    [Arguments]    ${c_sup}    ${c_ser}
    Set Suite Variable    ${c_sup}     ${c_sup}
    Set Suite Variable    ${c_ser}          ${c_ser}
    ## setup controlli a DB ##
    ContrattoETitolo
    SetupSoglieServizio

    #################### Prenotazione ####################
    ${res_body_P}     Prenotazione      ${amount}

    #dati transazione
    ${res_body_P}   convert string to json    ${res_body_P}
    Set Suite Variable    ${transactionID}    ${res_body_P['codiceTransazioneMerchant']}
    Set Suite Variable    ${d_aut}            ${res_body_P['autorizzazioneId']}

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     prenotazione
    ## controllo CPDB17
    Controllo_TKAA01_TKAA06     prenotazione
    ControlloRiskShield    ${amount}   prenotazione
    ControlloTKAA02    1
    ## controlli TKBB01
    CheckSoglieServizio    ${amount}   prenotazione

    #################### Modifica Prenotazione  ####################
    ${res_body_M}          ModificaPrenotazione     ${amount_new}
    #dati nuova transazione
    ${res_body_M}   convert string to json    ${res_body_M}
    Set Suite Variable    ${transactionID_new}    ${res_body_M['codiceTransazioneMerchant']}
    Set Suite Variable    ${d_aut_new}            ${res_body_M['autorizzazioneId']}

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     modificaPrenotazione
    ## controllo CPDB17
    Controllo_TKAA01_TKAA06     modificaPrenotazione
    ControlloRiskShield    ${amount_new}   modificaPrenotazione
    ControlloTKAA02    2
    ## controlli TKBB01
    CheckSoglieServizio    ${amount_new}   modificaPrenotazione

    #################### Conferma Multipla  ####################
    ${res_body_C}       ConfermaMultipla    ${amount_new}
    #dati lista nuove transazioni
    ${res_bodyC}   convert string to json    ${res_bodyC}
    CreaListeTrxDaResponse    ${res_bodyC}    listaAutorizzazioni

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     confermaMultipla
    ## controllo CPDB17
    Controllo_TKAA01_TKAA06     confermaMultipla
    ControlloRiskShield    ${amount_new}   confermaMultipla
    ControlloTKAA02    3
    ## controlli TKBB01
    CheckSoglieServizio    ${amount_new}   confermaMultipla


CicloATM
    [Arguments]    ${c_sup}    ${c_ser}
    Set Suite Variable    ${c_sup}     ${c_sup}
    Set Suite Variable    ${c_ser}     ${c_ser}
     ## setup controlli a DB ##
    ContrattoETitolo
    SetupSoglieServizio

    #################### autorizzazione abbonamento ####################
    ${res_body}     ottieniAutRateizzazione

    #dati transazione principale
    ${res_body}   convert string to json    ${res_body}
    Set Suite Variable    ${transactionID}    ${res_body['codiceTransazioneMerchant']}
    Set Suite Variable    ${d_aut}            ${res_body['autorizzazioneId']}
    #dati lista nuove transazioni
    CreaListeTrxDaResponse    ${res_body}    listTransazioni

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     abbonamento
    #controllo CPDB17
    Controllo_TKAA01_TKAA06     abbonamento
    ControlloRiskShield    ${amount}   abbonamento
    ControlloTKAA02    1
    ## controlli TKBB01
    CheckSoglieServizio    ${amount}   abbonamento

    #################### storno abbonamento ####################
    ${res_body}     ottienistornorateizzazione

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     stornoAbbonamento
    #controllo CPDB17
    Controllo_TKAA01_TKAA06     stornoAbbonamento
    ControlloRiskShield    ${amount}   stornoAbbonamento
    ControlloTKAA02    2
    ## controlli TKBB01
    CheckSoglieServizio    ${amount}   stornoAbbonamento
