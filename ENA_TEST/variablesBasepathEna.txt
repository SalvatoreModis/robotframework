*** Variables ***
&{headers}        Accept=application/json    Content-Type=application/json
${basepath_register}    /TelepassENA_REST/ena/noauth-services/register
${basepath_sendCustomerOBU}    /TelepassENA_REST/ena/services/send/send-notification-to-customer-obu
${basepath_sendCustomerCTR}    /TelepassENA_REST/ena/services/send/send-notification-to-customer-ctr
${basepath_sendCustomerTIT}    /TelepassENA_REST/ena/services/send/send-notification-to-customer-tit
${basepath_sendCustomerUSR}    /TelepassENA_REST/ena/services/send/send-notification-to-customer-usr
${basepath_sendCustomerDEV}    /TelepassENA_REST/ena/services/send/send-notification-to-customer-dev
${basepath_getLastNOT}    /TelepassENA_REST/ena/noauth-services/notification/getLastNotificationsSent
${basepath_logout}    /TelepassENA_REST/ena/noauth-services/logout-customer-usr
${basepath_logout_obu}    /TelepassENA_REST/ena/noauth-services/logout-customer-obu
