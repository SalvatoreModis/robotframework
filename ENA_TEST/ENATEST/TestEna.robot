*** Settings ***
Resource          ../LoadLibrary.txt
Resource          ReestRequestENA.txt

*** Variables ***
${basepath}       /TelepassENA_REST/ena/noauth-services/notification/getLastNotificationsSent

*** Test Cases ***
CreateNotification
    RestRequest    1000119691

Connection
    #verify whether a table exists or not
    dbConnection    CPDB06
    Table Must Exist    TENA05_NOT
    Table Must Exist    TENA07_INV_NOT
    #saving last notify and its ID for android and ios
    ${Notif_IOS}    Query    SELECT T05.C_APP, T05.C_IDE_NOT, T05.T_TIT, T05.T_TES, T07.C_TIP_DEV, T05.D_INV, T07.C_STA_INV FROM ENAA.TENA05_NOT AS T05 INNER JOIN ENAA.TENA07_INV_NOT AS T07 ON (T07.C_IDE_NOT = T05.C_IDE_NOT) WHERE T05.C_IDE_NOT = (SELECT MAX(T005.C_IDE_NOT) FROM ENAA.TENA05_NOT T005 INNER JOIN ENAA.TENA07_INV_NOT AS T007 ON(T007.C_IDE_NOT = T005.C_IDE_NOT) WHERE T007.C_TIP_DEV = 'IO'AND T005.D_INV >= CURRENT_TIMESTAMP - 1 DAYS)    false    true
    Log    ${Notif_IOS}
    ${Notif_AND}    Query    SELECT T05.C_APP, T05.C_IDE_NOT, T05.T_TIT, T05.T_TES, T07.C_TIP_DEV, T05.D_INV, T07.C_STA_INV FROM ENAA.TENA05_NOT AS T05 INNER JOIN ENAA.TENA07_INV_NOT AS T07 ON (T07.C_IDE_NOT = T05.C_IDE_NOT) WHERE T05.C_IDE_NOT = (SELECT MAX(T005.C_IDE_NOT) FROM ENAA.TENA05_NOT T005 INNER JOIN ENAA.TENA07_INV_NOT AS T007 ON(T007.C_IDE_NOT = T005.C_IDE_NOT) WHERE T007.C_TIP_DEV = 'AN'AND T005.D_INV >= CURRENT_TIMESTAMP - 1 DAYS)    false    true
    Log    ${Notif_AND}
    ${Notif_IOS_ID}    Get From Dictionary    ${Notif_IOS}[0]    C_IDE_NOT
    ${Notif_AND_ID}    Get From Dictionary    ${Notif_AND}[0]    C_IDE_NOT
    Set Suite Variable    ${NOT_AND_ID}    ${Notif_AND_ID}
    Set Suite Variable    ${NOT_IOS_ID}    ${Notif_IOS_ID}
    Disconnect From Database

Rest
    #take notification ID from Rest request
    Create Session    mysession    ${endpoint}
    ${response}=    GET On Session    mysession    ${basepath}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    Should Be Equal As Strings    ${response.json()['status']}    00
    FOR    ${item}    IN    ${response.json()['notifications']}
    FOR    ${subitem}    IN    @{item}
        IF    "${subitem}[deviceType]" == "IO"
        ${notification_ID_IO}    Get From Dictionary    ${subitem}    notificationId
        ELSE IF    "${subitem}[deviceType]" == "AN"
        ${notification_ID_AN}    Get From Dictionary    ${subitem}    notificationId
    END
    END
    END
    #comparing results from query and rest request
    #Should Be Equal    ${notification_ID_IO}    ${NOT_IOS_ID}
    #Should Be Equal    ${notification_ID_AN}    ${NOT_AND_ID}
