*** Settings ***
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/Body.txt

*** Variables ***
${endpoint}       https://ws-test.telepass.com
#${endpoint}      http://rtf-test.com
${access_token}    ${EMPTY}
#${obuID}         1000116366
${obuID}          1000119691
${transactionID}    ${EMPTY}
${update_response}    ${EMPTY}
@{dettagli}
${outputUpdatebody}    ${EMPTY}
${C_SER}          23
${PROD}           TPAY
${amount}         150000

*** Test Cases ***
KO-PLAFOND_INSUFFICIENTE LOGIN
    CollectingINFO    CPDB06    ${obuID}
    ######################impostazione spesa mensile al valore limite del plafond
    dbConnection    CPDB29
    Execute Sql String    UPDATE KBBA.TKBB01_CRE_CTR SET I_CRE=1500 WHERE C_SER = 'XX' AND C_CTR ='${ctrID_str1}' AND D_FIN_VAL> CURRENT timestamp
    Disconnect from Database
    ######################login
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

KO-PLAFOND_INSUFFICIENTE PRENOTAZIONE
    #######################prenotazione con amount superiore al plafond
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Corso Italia n°333,20123 Milano MI,Italia
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    merchantOrderId=1
    ${amount}    Convert to Integer    ${amount}
    Set to Dictionary    ${checkin_json}    amount    ${amount}
    Run Keyword And Expect Error    STARTS: HTTPError: 403    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    dbConnection    CPDB17
    ${transactionID}    Query    SELECT C_IDE_TXN_ESE FROM KAAA.TKAA01_AUT_PAG WHERE D_REG BETWEEN CURRENT timestamp - 5 SECOND AND CURRENT TIMESTAMP and C_SUP= ${obuID} and F_AUT='N' ORDER BY D_AUT DESC LIMIT 1    false    true
    ${transactionID}    Get from Dictionary    ${transactionID}[0]    C_IDE_TXN_ESE
    Disconnect from Database
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='98' and C_SUP= ${obuID}    1
    Disconnect from Database
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KSBA.TKSB01_MOV WHERE C_IDE_TXN='${transactionID}' AND C_TIP_OPE_PAG='K2' and C_SUP= ${obuID}    1
    Disconnect from Database

KO-PLAFOND_INSUFFICIENTE AUTORIZZAZONE DIRETTA
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Corso Italia n°333,20123 Milano MI,Italia
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    merchantOrderId=1
    ${amount}    Convert to Integer    ${amount}
    Set to Dictionary    ${checkin_json}    amount    ${amount}
    Run Keyword And Expect Error    STARTS: HTTPError: 403    PostRequest3part    ${endpoint}    ${basepath_authdiretta3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    dbConnection    CPDB17
    ${transactionID}    Query    SELECT C_IDE_TXN_ESE FROM KAAA.TKAA01_AUT_PAG WHERE D_REG BETWEEN CURRENT timestamp - 5 SECOND AND CURRENT TIMESTAMP and C_SUP= ${obuID} and F_AUT='N' ORDER BY D_AUT DESC LIMIT 1    false    true
    ${transactionID}    Get from Dictionary    ${transactionID}[0]    C_IDE_TXN_ESE
    Disconnect from Database
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='98' and C_SUP= ${obuID}    1
    Disconnect from Database
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KSBA.TKSB01_MOV WHERE C_IDE_TXN='${transactionID}' AND C_TIP_OPE_PAG='K2' and C_SUP= ${obuID}    1
    Disconnect from Database

KO-PLAFOND_INSUFFICIENTE INSTALMENTS
    ${traghetti_json}    evaluate    json.loads('''${body_traghetti}''')    json
    ${C_SER}    Set Variable    37
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    serviceCode=${C_SER}
    Set to Dictionary    ${traghetti_json}    pointOfService    ${POS}
    ${time}    Evaluate    int(round(time.time() * 1000))
    ${time1}    Evaluate    ${time}+ 432000000
    ${random} =    Evaluate    random.randint(0, 99999999)
    Set to Dictionary    ${traghetti_json}    externalId=WS${random}
    Set to Dictionary    ${traghetti_json}    validityEndDate=${time1}
    Set to Dictionary    ${traghetti_json}    validityStartDate=${time}
    Run Keyword And Expect Error    STARTS: HTTPError: 403    PostRequest3part    ${endpoint}    ${basepath_instalments}    ${traghetti_json}    ${access_token}    ${obuID}    ${PROD}
    dbConnection    CPDB17
    ${transactionID}    Query    SELECT C_IDE_TXN_ESE FROM KAAA.TKAA01_AUT_PAG WHERE D_REG BETWEEN CURRENT timestamp - 5 SECOND AND CURRENT TIMESTAMP and C_SUP= '${obuID}' and I_PAG_AUT=0 ORDER BY D_AUT DESC LIMIT 1    false    true
    ${transactionID}    Get from Dictionary    ${transactionID}[0]    C_IDE_TXN_ESE
    Disconnect from Database
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX_PRI='${transactionID}' AND F_STA_TRX='98'    3
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX a JOIN KDAA.TKDA03_OPE_REQ_PRD b ON a.D_INS=b.D_INS WHERE a.C_IDE_TRX_PRI='${transactionID}' AND C_TIP_TRX='S'    4
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA25_RAT_SPE WHERE C_IDE_TRX_PRI='${transactionID}'    0
    Disconnect from Database

PLAFONDOK
    dbConnection    CPDB29
    Execute Sql String    UPDATE KBBA.TKBB01_CRE_CTR SET I_CRE=0 WHERE C_SER = 'XX' AND C_CTR ='${ctrID_str1}' AND D_FIN_VAL> CURRENT timestamp
    Disconnect from Database
