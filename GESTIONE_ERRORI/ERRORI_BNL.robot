*** Settings ***
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/Body.txt
Resource          ../DATA/Mock_BNL_endpoint.txt

*** Variables ***
${endpoint}       http://rtf-test.com
#${endpoint}      https://ws-test.telepass.com
${access_token}    ${EMPTY}
${obuID}          1000116366
#${obuID}         1000119691
${transactionID}    ${EMPTY}
${update_response}    ${EMPTY}
@{dettagli}
${outputUpdatebody}    ${EMPTY}
${C_SER}          23
${PROD}           TPAYX
${amount}         1

*** Test Cases ***
SETUP MOCK E IBAN-RichiestaImp
    CollectingINFO    CPDB06    ${obuID}
    dbConnection    CPDB17
    Execute Sql String    UPDATE KAAA.TKAA07_WS_3PA SET T_URL='${path_mock_RichiestaImpegno}' WHERE T_DES='RICHIESTA_IMPEGNO_BNL'
    Row Count Is Equal To X    Select * From KAAA.TKAA07_WS_3PA where T_URL='${path_mock_RichiestaImpegno}'    1
    Disconnect from Database
    dbConnection    CPDB06
    Execute Sql String    UPDATE KSAA.TKSASF_CTR_PAY SET C_IBAN='IT52X0100538100000000000481' WHERE C_CTR='${ctrID_str1}'
    Disconnect from Database

KO-ERRORI BNL -RICHIESTA IMPEGNO
    ###########login 3part
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}
    ########checkin 3part
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Corso Italia n°333,20123 Milano MI,Italia
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    merchantOrderId=1
    ##########verifica transazione vada in errore
    Run Keyword And Expect Error    STARTS: HTTPError: 403    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    #########prendo TRX da KAA01
    Sleep    2
    dbConnection    CPDB17
    ${transactionID}    Query    SELECT C_IDE_TXN_ESE FROM KAAA.TKAA01_AUT_PAG WHERE D_REG BETWEEN CURRENT timestamp - 5 SECOND AND CURRENT TIMESTAMP and C_SUP= '${obuID}' ORDER BY D_REG_SIS_ORI DESC LIMIT 1    false    true
    ${transactionID}    Get from Dictionary    ${transactionID}[0]    C_IDE_TXN_ESE
    Disconnect from Database
    ${codiceErrore}    ErroriBNL    ${transactionID}
    IF    "${codiceErrore}"=="599"
    ${F_STA_TRX}    Set Variable    97
    ${C_TIP_OPE}    Set Variable    K5
    ELSE IF    "${codiceErrore}"=="211"
    ${F_STA_TRX}    Set Variable    98
    ${C_TIP_OPE}    Set Variable    K4
    END
    #    #######################
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='${F_STA_TRX}' and C_SUP= '${obuID}'    1
    Disconnect from Database
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KSBA.TKSB01_MOV WHERE C_IDE_TXN='${transactionID}' AND C_TIP_OPE_PAG='${C_TIP_OPE}' and C_SUP= ${obuID}    1
    Disconnect from Database

SETUP MOCK E IBAN-AuthDiretta
    CollectingINFO    CPDB06    ${obuID}
    dbConnection    CPDB17
    Execute Sql String    UPDATE KAAA.TKAA07_WS_3PA SET T_URL='${path_mock_ConfermaImpegnoSenzaAuth}' WHERE T_DES='CONFERMA_ADDEBITO_SENZA_AUTORIZZAZIONE_BNL'
    Row Count Is Equal To X    Select * From KAAA.TKAA07_WS_3PA where T_URL='${path_mock_ConfermaImpegnoSenzaAuth}'    1
    Disconnect from Database
    dbConnection    CPDB06
    Execute Sql String    UPDATE KSAA.TKSASF_CTR_PAY SET C_IBAN='IT52X0100538100000000000484' WHERE C_CTR='${ctrID_str1}'
    Disconnect from Database

KO-ERRORI BNL-AUTORIZZAZIONE DIRETTA
    ###########login 3part
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}
    ########checkin 3part
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Corso Italia n°333,20123 Milano MI,Italia
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    merchantOrderId=1
    ##########verifica transazione vada in errore
    Run Keyword And Expect Error    STARTS: HTTPError: 403    PostRequest3part    ${endpoint}    ${basepath_authdiretta3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    #########prendo TRX da KAA01
    dbConnection    CPDB17
    ${transactionID}    Query    SELECT C_IDE_TXN_ESE FROM KAAA.TKAA01_AUT_PAG WHERE D_REG BETWEEN CURRENT timestamp - 5 SECOND AND CURRENT TIMESTAMP and C_SUP= '${obuID}' ORDER BY D_REG_SIS_ORI DESC LIMIT 1    false    true
    ${transactionID}    Get from Dictionary    ${transactionID}[0]    C_IDE_TXN_ESE
    Disconnect from Database
    ${codiceErrore}    ErroriBNL    ${transactionID}
    IF    "${codiceErrore}"=="599"
    ${F_STA_TRX}    Set Variable    97
    ${C_TIP_OPE}    Set Variable    K5
    ELSE IF    "${codiceErrore}"=="211"
    ${F_STA_TRX}    Set Variable    98
    ${C_TIP_OPE}    Set Variable    K4
    ELSE IF    "${codiceErrore}"=="204"    ####transazione duplicata in realtà 204 è plafond insuff
    ${F_STA_TRX}    Set Variable    98
    ${C_TIP_OPE}    Set Variable    K6
    END
    #    #######################
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='${F_STA_TRX}' and C_SUP= ${obuID}    1
    Disconnect from Database
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KSBA.TKSB01_MOV WHERE C_IDE_TXN='${transactionID}' AND C_TIP_OPE_PAG='${C_TIP_OPE}' and C_SUP= ${obuID}    1
    Disconnect from Database

SETUP MOCK E IBAN-CONFERMA IMPEGNO
    CollectingINFO    CPDB06    ${obuID}
    dbConnection    CPDB17
    Execute Sql String    UPDATE KAAA.TKAA07_WS_3PA SET T_URL='${path_mock_ConfermaImpegno}' WHERE T_DES='CONFERMA_ADDEBITO_IMPEGNO_BNL'
    Row Count Is Equal To X    Select * From KAAA.TKAA07_WS_3PA where T_URL='${path_mock_ConfermaImpegno}'    1
    Disconnect from Database
    dbConnection    CPDB06
    Execute Sql String    UPDATE KSAA.TKSASF_CTR_PAY SET C_IBAN='IT52X0100538100000000000483' WHERE C_CTR='${ctrID_str1}'
    Disconnect from Database

KO-ERRORI BNL CONFERMA
    ###########login 3part
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}
    ########checkin 3part
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Corso Italia n°333,20123 Milano MI,Italia
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    merchantOrderId=1
    ${output}    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}
    ${output}    Set to Dictionary    ${output.json()}    terminalId=12455    merchantOrderId=AAAA-BBBB.
    ${outputUpdate}    PutRequest3part    ${endpoint}    ${basepath_aggiornamento3part}${transactionID}    ${output}    ${access_token}    ${obuID}    ${PROD}
    ${details}    CreateDictionaryList    ${C_SER}
    ${outputUpdatebody}    Set to Dictionary    ${outputUpdate.json()}    details=${details}
    Set Suite Variable    ${outputUpdatebody}    ${outputUpdatebody}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='10'    1
    Disconnect from Database
    ##########verifica transazione vada in errore
    Run Keyword And Expect Error    STARTS: HTTPError: 403    PostRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}/authorize    ${outputUpdatebody}    ${access_token}    ${obuID}    ${PROD}
    ######################verifica codice di errore
    ${codiceErrore}    ErroriBNL    ${transactionID}
    IF    "${codiceErrore}"=="599"
    ${F_STA_TRX}    Set Variable    20
    ${C_TIP_OPE}    Set Variable    K5
    ELSE IF    "${codiceErrore}"=="211"
    ${F_STA_TRX}    Set Variable    20
    ${C_TIP_OPE}    Set Variable    K4
    ELSE IF    "${codiceErrore}"=="204"    ####transazione duplicata in realtà 204 è plafond insuff
    ${F_STA_TRX}    Set Variable    20
    ${C_TIP_OPE}    Set Variable    K6
    END
    #    #######################
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='${F_STA_TRX}' and C_SUP= ${obuID}    1
    Disconnect from Database
    ##no controlli sulla tksb01 mov in quanto l'errore su conferma non viene registrato

IBANOK
    CollectingINFO    CPDB06    ${obuID}
    dbConnection    CPDB06
    Execute Sql String    UPDATE KSAA.TKSASF_CTR_PAY SET C_IBAN='IT13S0100502802000000000030'    WHERE C_CTR='${ctrID_str1}'
    Disconnect from Database
