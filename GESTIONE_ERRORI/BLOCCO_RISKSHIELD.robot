*** Settings ***
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/Body.txt

*** Variables ***
#${endpoint}      https://ws-test.telepass.com
${access_token}    ${EMPTY}
#${obuID}         1000116366
${obuID}          1000005981
${transactionID}    ${EMPTY}
${update_response}    ${EMPTY}
@{dettagli}
${outputUpdatebody}    ${EMPTY}
${C_SER}          23
${PROD}           TPAY

*** Test Cases ***
KO-BLOCCO RISKSHIELD PRENOTAZIONE TPAY
    Set Suite Variable    ${obuID}    1000005981
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Corso Italia n°333,20123 Milano MI,Italia
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    merchantOrderId=1
    Run Keyword And Expect Error    STARTS: HTTPError: 403    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    dbConnection    CPDB17
    ${transactionID}    Query    SELECT C_IDE_TXN_ESE FROM KAAA.TKAA01_AUT_PAG WHERE D_REG BETWEEN CURRENT timestamp - 5 SECOND AND CURRENT TIMESTAMP and C_SUP= ${obuID} ORDER BY D_AUT DESC LIMIT 1    false    true
    ${transactionID}    Get from Dictionary    ${transactionID}[0]    C_IDE_TXN_ESE
    Disconnect from Database
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='98' and C_SUP= ${obuID}    1
    Disconnect from Database
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KSBA.TKSB01_MOV WHERE C_IDE_TXN='${transactionID}' AND C_TIP_OPE_PAG='K3' and C_SUP= ${obuID}    1
    Disconnect from Database
    dbConnection    CPDB06

KO-BLOCCO RISKSHIELD PRENOTAZIONE TPAYX
    Set Suite Variable    ${obuID}    1000110732
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Corso Italia n°333,20123 Milano MI,Italia
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    merchantOrderId=1
    Run Keyword And Expect Error    STARTS: HTTPError: 403    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    dbConnection    CPDB17
    ${transactionID}    Query    SELECT C_IDE_TXN_ESE FROM KAAA.TKAA01_AUT_PAG WHERE D_REG BETWEEN CURRENT timestamp - 5 SECOND AND CURRENT TIMESTAMP and C_SUP= ${obuID} ORDER BY D_AUT DESC LIMIT 1    false    true
    ${transactionID}    Get from Dictionary    ${transactionID}[0]    C_IDE_TXN_ESE
    Disconnect from Database
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='98' and C_SUP= ${obuID}    1
    Disconnect from Database
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KSBA.TKSB01_MOV WHERE C_IDE_TXN='${transactionID}' AND C_TIP_OPE_PAG='K3' and C_SUP= ${obuID}    1
    Disconnect from Database
    dbConnection    CPDB06
