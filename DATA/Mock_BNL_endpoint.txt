*** Settings ***
Resource          LibraryDBParamRestReq.txt

*** Variable ***
${path_bnl_RichiestaImpegno}    https://apimcoll-tpp.bnl.it/telepass/tpay/impegnoservice/richiestaimpegno
${path_bnl_ConfermaImpegno}    https://apimcoll-tpp.bnl.it/telepass/tpay/pagamentoservice/confermaaddebitoimpegno
${path_bnl_ConfermaImpegnoSenzaAuth}    https://apimcoll-tpp.bnl.it/telepass/tpay/pagamentoservice/confermaaddebitosenzaautorizzazione
${path_mock_RichiestaImpegno}    http://stubs.test.gcp.telepass.com:8080/telepass/tpay/impegnoservice/richiestaimpegno
${path_mock_ConfermaImpegno}    http://stubs.test.gcp.telepass.com:8080/telepass/tpay/pagamentoservice/confermaaddebitoimpegno
${path_mock_ConfermaImpegnoSenzaAuth}    http://stubs.test.gcp.telepass.com:8080/telepass/tpay/pagamentoservice/confermaaddebitosenzaautorizzazione

*** Keywords ***
ErroriBNL
    [Arguments]    ${trx}
    LibraryDBParamRestReq.Db Connection    CPDB17
    ${PAYOUT}    Query    SELECT T_DES_OUT FROM KAAA.TKAA02_LOG_OPE_ETF WHERE C_TRA_ID ='${trx}' and C_ESI_OPE='N' order by D_OPE asc LIMIT 1    false    true
    ${PAYOUT_str}    Get from dictionary    ${PAYOUT}[0]    T_DES_OUT
    ${Err}    Evaluate    json.loads('''${PAYOUT_str}''')
    ${Err1}    Get From Dictionary    ${Err}    responseBody
    ${codiceCausaleErrore}    Get From Dictionary    ${Err1}    codiceCausaleErrore
    [Return]    ${codiceCausaleErrore}
