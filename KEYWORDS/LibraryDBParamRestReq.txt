*** Settings ***
Library           DateTime
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           REST    ${endpoint}
Library           JSONLibrary
Library           DatabaseLibrary
Library           OperatingSystem
Library           String
Library           XML
Library           tokenTpayX.py
Library           FakerLibrary

*** Variables ***
${dbApiModule}    ibm_db_dbi
&{usernameDB}     user06=cpdb06_modifier_atech    user17=cpdb17_modifier_atech    user18=cpdb18_modifier_atech    user19=cpdb19_modifier_atech    user28=cpdb28_modifier_atech    user29=cpdb29_modifier_atech    user32=cpdb32_modifier_atech
&{passwordDB}     pass06=LyFvPq3PbdzCteYb4Nsz    pass17=9pCAoTh9Mihh9xRuCeC9    pass18=hr9FqoeMdLV7FyCcXhd3    pass19=hr9Fqoe012V7FyCcXhd3    pass28=Ra3eRiEdaVRgtptKyqse    pass29=uJawxdV97WfijVmbXx7F    pass32=JKmbqiFwEekJdmWNy7je
&{dbHost}         host06=10.153.236.34    host17=10.153.236.22    host18=10.153.236.23    host19=cpdb19-node1.test.gcp.telepass.com    host28=10.153.236.24    host29=10.153.236.25    host32=10.153.236.27
${dbPort}         50000
&{headers_mulesoft_login}    Accept=2fa    X-TPay-Latitude=1    X-TPay-OS-Type=Android    X-TPay-Mobile-Network-Type=1    X-TPay-GPS-Error=1    X-TPay-Longitude=1    X-TPay-App-Millis=1    X-TPay-Connection-Type=1    X-TPay-App-Version=3.0.0    Authorization=Basic dHBheS1hcHA6dHBheS1zM2NyM3Q=    X-TPay-OS-Version=1    X-TPay-Device-Id=32232332d32dewdw23    Content-Type=application/x-www-form-urlencoded
&{headers_mulesoft}    Accept=application/json    X-TPay-Latitude=41.852461    X-TPay-OS-Type=Android    X-TPay-Mobile-Network-Type=1    X-TPay-GPS-Error=1    X-TPay-Longitude=12.4669735    X-TPay-App-Millis=1    X-TPay-Connection-Type=1    X-TPay-App-Version=3.0.0    X-TPay-OS-Version=1    X-TPay-Device-Id=32232332d32dewdw23    Content-Type=application/json
&{headersCALLBACK}    Contet-Type=text/xml    X-Caller-System-Id=TST
&{BASICTOKEN}     saleforce=c2FsZXNmb3JjZS1zZXJ2ZXI6TXIycXBEcTZ1UjV5TUNxeg==    k1=azEtc2VydmVyOjZnOXI5dmJxS201N2VBcg==    22=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMjI6bGVvbmFyZG8=    23=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMjM6bGVvbmFyZG8=    26=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMjY6bGVvbmFyZG8=    33=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMzg6bGVvbmFyZG8=    34=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMzM6bGVvbmFyZG8=    35=dGVyemUtcGFydGk6dGVyemUtcGFydGk=    36=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMzM6bGVvbmFyZG8=    56=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMzg6bGVvbmFyZG8=    39=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMzg6bGVvbmFyZG8=    44=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMjM6bGVvbmFyZG8=    45=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMzg6bGVvbmFyZG8=    64=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMzg6bGVvbmFyZG8=    63=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMzg6bGVvbmFyZG8=    37=dGVzdC1tZXJjaGFudC1zZXJ2ZXItMjM6bGVvbmFyZG8=    24=Y2FzaGJhY2s6Y2FzaGJhY2s=24=Y2FzaGJhY2s6Y2FzaGJhY2s=    28=Ym5sLXNlcnZlcjpXcnNUZFAzZGZXSHMzU1o4
&{headers_mulesoft3partTpay}    x-title-type=SM    X-User-Identification-Code=34648145-66DA-42BC-A411-B792F68B0803    Accept=application/json    X-TPay-Latitude=41.852461    X-TPay-OS-Type=Android    X-TPay-Mobile-Network-Type=1    X-TPay-GPS-Error=1    X-TPay-Longitude=12.4669735    X-TPay-App-Millis=1    X-TPay-Connection-Type=1    X-TPay-App-Version=3.0.0    X-TPay-OS-Version=1    X-TPay-Device-Id=32232332d32dewdw23    Content-Type=application/json
&{headers_mulesoft3partTpayX}    x-title-type=SM    X-User-Identification-Code=34648145-66DA-42BC-A411-B792F68B0803    Accept=application/json    X-TEvo-Latitude=41.852461    X-TEvo-OS-Type=Android    X-TEvo-Mobile-Network-Type=1    X-TEvo-GPS-Error=1    X-TEvo-Longitude=12.4669735    X-TEvo-App-Millis=1    X-TEvo-Connection-Type=1    X-TEvo-App-Version=3.0.0    X-TEvo-OS-Version=1    X-TEvo-Device-Id=32232332d32dewdw23    Content-Type=application/json
&{headers_mulesoft_loginX}    X-TEvo-Latitude=1    X-TEvo-OS-Type=Android    X-TEvo-Mobile-Network-Type=1    X-TEvo-GPS-Error=1    X-TEvo-Longitude=1    X-TEvo-App-Millis=1    X-TEvo-Connection-Type=1    X-TEvo-App-Version=3.0.0    Authorization=Basic dGV2by1hcHA6dGV2by1zM2NyM3Q=    X-TEvo-OS-Version=1    X-TEvo-Device-Id=32232332d32dewdw23    Content-Type=application/x-www-form-urlencoded
&{headers_mulesoftX}    Accept=application/json    X-TEvo-Latitude=41.852461    X-TEvo-OS-Type=Android    X-TEvo-Mobile-Network-Type=1    X-TEvo-GPS-Error=1    X-TEvo-Longitude=12.4669735    X-TEvo-App-Millis=1    X-TEvo-Connection-Type=1    X-TEvo-App-Version=3.0.0    X-TEvo-OS-Version=1    X-TEvo-Device-Id=085d3d34c8450eef    Content-Type=application/json

*** Keywords ***
dbConnection
    [Arguments]    ${dbName}
    ${db}    Set Variable    ${dbName}
    IF    "${db}" == "CPDB06"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb06_modifier_atech    dbPassword=LyFvPq3PbdzCteYb4Nsz    dbHost=10.153.236.34    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB17"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb17_modifier_atech    dbPassword=9pCAoTh9Mihh9xRuCeC9    dbHost=10.153.236.22    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB18"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb18_modifier_atech    dbPassword=hr9FqoeMdLV7FyCcXhd3    dbHost=10.153.236.23    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB19"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb19_modifier_atech    dbPassword=hr9Fqoe012V7FyCcXhd3    dbHost=cpdb19-node1.test.gcp.telepass.com    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB28"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb28_modifier_atech    dbPassword=Ra3eRiEdaVRgtptKyqse    dbHost=10.153.236.24    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB29"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb29_modifier_atech    dbPassword=uJawxdV97WfijVmbXx7F    dbHost=10.153.236.25    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB32"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb32_modifier_atech    dbPassword=JKmbqiFwEekJdmWNy7je    dbHost=10.153.236.27    dbPort=${dbPort}
    ELSE IF    "${db}" == "CPDB30"
    Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=cpdb30_viewer_atech    dbPassword=qonzwXXuFvneu3UuTunH    dbHost=10.153.236.26    dbPort=${dbPort}
    END

PostRequestLogin
    [Arguments]    ${endpoint}    ${basepath}    ${username}    ${password}
    ${data}    Create Dictionary    client_id=tpay-app    grant_type=2fa    device_id=566A11B5589AB4DD    username=${username}    password=${password}
    Create Session    mysession    ${endpoint}
    ${response}=    POST On Session    mysession    ${basepath}    headers=${headers_mulesoft_login}    data=${data}
    Should Be Equal As Strings    ${response.status_code}    200
    [Return]    ${response}

PostRequestLogin3part
    [Arguments]    ${endpoint}    ${basepath}    ${service}
    ${ser}=    Set Variable    ${service}
    ${BASICTOKENValue}=    Get From Dictionary    ${BASICTOKEN}    ${ser}
    ${data}    Create Dictionary    grant_type=client_credentials
    Set to Dictionary    ${headers_mulesoft_login}    Authorization=Basic ${BASICTOKENValue}
    Create Session    mysession    ${endpoint}
    ${response}=    POST On Session    mysession    ${basepath}    headers=${headers_mulesoft_login}    data=${data}
    Should Be Equal As Strings    ${response.status_code}    200
    [Return]    ${response}

PostRequest
    [Arguments]    ${endpoint}    ${basepath}    ${body}    ${access_token}    ${LAT}    ${LON}    ${PRODUCT}
    ${LAT}    Convert to String    ${LAT}
    ${LON}    Convert to String    ${LON}
    ${headers_mulesoft}    SetHeadersStandard    ${PRODUCT}    ${LAT}    ${LON}
    Set to Dictionary    ${headers_mulesoft}    Authorization=Bearer ${access_token}
    Create Session    mysession    ${endpoint}
    ${response}=    POST On Session    mysession    ${basepath}    headers=${headers_mulesoft}    json=${body}
    Should Be True    """${response.status_code}""" in """200201202"""
    Should Be True    """${response.status_code}""" in """200201"""
    [Return]    ${response}

PostRequestLoginX
    [Arguments]    ${endpoint}    ${basepath}    ${username}
    dbConnection    CPDB18
    ${C_ACT_TOK}    Query    SELECT C_ACT_TKN FROM KSQA.TKSQ19_TKN_EVO WHERE C_NUM_TEL ='${username}'    false    true
    ${C_TOK}    Query    SELECT C_TKN FROM KSQA.TKSQ19_TKN_EVO WHERE C_NUM_TEL ='${username}'    false    true
    ${C_ACT_TOK_str}    Get from Dictionary    ${C_ACT_TOK}[0]    C_ACT_TKN
    ${C_TOK_str}    Get from Dictionary    ${C_TOK}[0]    C_TKN
    ${tokenTpayX}    tokenTpayX    ${C_ACT_TOK_str}    ${C_TOK_str}
    DbConnection    CPDB18
    ${deviceID}    Query    SELECT C_DEV FROM KSQA.TKSQ19_TKN_EVO WHERE C_NUM_TEL LIKE '${username}'    false    true
    Disconnect From Database
    ${deviceID}    Get From Dictionary    ${deviceID}[0]    C_DEV
    Set Suite Variable    ${deviceID}    ${deviceID}
    ${data}    Create Dictionary    client_id=tevo-app    grant_type=token_evo    device_id=${deviceID}    token=${tokenTpayX}
    Create Session    mysession    ${endpoint}
    ${response}=    POST On Session    mysession    ${basepath}    headers=${headers_mulesoft_loginX}    data=${data}
    Should Be Equal As Strings    ${response.status_code}    200
    [Return]    ${response}

PostRequest3part
    [Arguments]    ${endpoint}    ${basepath}    ${body}    ${access_token}    ${obuID}    ${PROD}    ${OPTIONAL}=default
    ${headers_mulesoft3part}    SetHeaders    ${obuID}    ${PROD}
    Set to Dictionary    ${headers_mulesoft3part}    Authorization=Bearer ${access_token}
    IF    '${OPTIONAL}' =='default'
    IF    '${PROD}'=='TPAY'
    Set to Dictionary    ${body}    paymentTypeCode=KTH
    ELSE IF    '${PROD}'=='TPAYX'
    Set to Dictionary    ${body}    paymentTypeCode=KTP
    END
    ELSE
    Remove from Dictionary    ${body}    paymentTypeCode
    END
    Create Session    mysession    ${endpoint}
    ${response}=    POST On Session    mysession    ${basepath}    headers=${headers_mulesoft3part}    json=${body}
    #Should Be True    """${response.status_code}""" in """200201"""
    [Return]    ${response}

PatchRequest3part
    [Arguments]    ${endpoint}    ${basepath}    ${body}    ${access_token}    ${obuID}    ${PROD}
    ${headers_mulesoft3part}    SetHeaders    ${obuID}    ${PROD}
    Set to Dictionary    ${headers_mulesoft3part}    Authorization=Bearer ${access_token}
    Create Session    mysession    ${endpoint}
    ${response}=    PATCH On Session    mysession    ${basepath}    headers=${headers_mulesoft3part}    json=${body}
    #Should Be True    """${response.status_code}""" in """200201"""
    [Return]    ${response}

PutRequest3part
    [Arguments]    ${endpoint}    ${basepath}    ${body}    ${access_token}    ${obuID}    ${PROD}
    ${headers_mulesoft3part}    SetHeaders    ${obuID}    ${PROD}
    Set to Dictionary    ${headers_mulesoft3part}    Authorization=Bearer ${access_token}
    Create Session    mysession    ${endpoint}
    ${response}=    PUT On Session    mysession    ${basepath}    headers=${headers_mulesoft3part}    json=${body}
    Should Be Equal As Strings    ${response.status_code}    200
    [Return]    ${response}

GetRequest
    [Arguments]    ${endpoint}    ${basepath}
    Create Session    mysession    ${endpoint}
    ${response}=    GET On Session    mysession    ${basepath}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    [Return]    ${response}

DeleteRequest3part
    [Arguments]    ${endpoint}    ${basepath}    ${body}    ${access_token}    ${obuID}    ${PROD}
    ${headers_mulesoft3part}    SetHeaders    ${obuID}    ${PROD}
    Set to Dictionary    ${headers_mulesoft3part}    Authorization=Bearer ${access_token}
    Create Session    mysession    ${endpoint}
    ${response}=    DELETE On Session    mysession    ${basepath}    headers=${headers_mulesoft3part}    json=${body}
    #Should Be True    """${response.status_code}""" in """200201"""
    [Return]    ${response}

CollectingINFO
    [Arguments]    ${dbName}    ${obuID}
    #Connect To Database    dbapiModuleName=${dbApiModule}    dbName=${dbName}    dbUsername=${username_06}    dbPassword=${password_06}    dbHost=${dbHost_06}    dbPort=${dbPort}
    dbConnection    ${dbName}
    ${info}    Query    SELECT C_USR_ID,C_CTR,C_CTP_CLI,C_PRO from EVDA.TETSJU_ATT_PNG WHERE C_OBU_ID = ${obuID}    false    true
    ${userID_str}    Get From Dictionary    ${info}[0]    C_USR_ID
    Set Suite Variable    ${userID_str1}    ${userID_str}
    ${ctrID_str}    Get from Dictionary    ${info}[0]    C_CTR
    Set Suite Variable    ${ctrID_str1}    ${ctrID_str}
    ${ctpCliID_str}    Get from Dictionary    ${info}[0]    C_CTP_CLI
    Set Suite Variable    ${ctpCli_str1}    ${ctpCliID_str}
    ${c_pro_str}    Get from Dictionary    ${info}[0]    C_PRO
    Set Suite Variable    ${c_pro}    ${c_pro_str}
    ${titleID}    Query    SELECT C_TIT FROM evda.TETSD2_TIT WHERE C_CTR =${ctrID_str} AND C_TIP_TIT = 'SM'    false    true
    ${titleID_str}    Get from Dictionary    ${titleID}[0]    C_TIT
    Set Suite Variable    ${titleID_str1}    ${titleID_str}
    [Return]    ${userID_str1}    ${ctrID_str1}    ${ctpCli_str1}    ${titleID_str1}

CreateDictionaryList
    [Arguments]    ${service}
    ${SER}    Set Variable    ${service}
    Create List    ${dettagli}
    IF    "${SER}"=="23"
    &{1}    Create Dictionary    uom=pz    productName=Via dAtech n. 23/a    quantity=1    productId=1    unitPrice=${20}
    ELSE IF    "${SER}"=="35"
    &{1}    Create Dictionary    uom=pz    quantity=1    productId=1    unitPrice=${20}    productName=A/R - Torino P - Firenze S. - 2022-01-20
    ELSE
    &{1}    Create Dictionary    uom=pz    productName=Via dAtech n. 23/a    quantity=1    productId=1    unitPrice=${20}
    END
    Append to List    ${dettagli}    ${1}
    [Return]    ${dettagli}

GetUSERIDCODE
    [Arguments]    ${obuID}
    dbConnection    CPDB18
    ${UserIdCode}    Query    SELECT C_UTE_MER FROM KSQA.TKSQ13_COD_UTE_MER WHERE C_OBU_ID = ${obuID}    false    true
    ${UserIdCode_str}    Get from Dictionary    ${UserIdCode}[0]    C_UTE_MER
    [Return]    ${UserIdCode_str}

GetPOSCode
    [Arguments]    ${service}
    dbConnection    CPDB32
    ${service}    Convert to String    ${service}
    ${POS}    Query    SELECT C_PUN_ERO_SER FROM KDAA.TKDA05_TRX WHERE C_SER='${service}' AND F_STA_TRX IN ('30','70') ORDER BY D_INS DESC LIMIT 1    false    true
    ${POS}    Get from Dictionary    ${POS}[0]    C_PUN_ERO_SER
    ${POS}    Convert to String    ${POS}
    [Return]    ${POS}

SetHeaders
    [Arguments]    ${obuID}    ${PRODUCT}
    IF    "${Product}"== "TPAY"
    &{headers_mulesoft3part}=    Copy Dictionary    ${headers_mulesoft3partTpay}    False
    ELSE IF    "${Product}"== "TPAYX"
    &{headers_mulesoft3part}=    Copy Dictionary    ${headers_mulesoft3partTpayX}    False
    END
    ${userIDCode}    GetUSERIDCODE    ${obuID}
    Set to Dictionary    ${headers_mulesoft3part}    X-User-Identification-Code=${userIDCode}
    [Return]    ${headers_mulesoft3part}

SetHeadersStandard
    [Arguments]    ${PRODUCT}    ${LAT}    ${LON}
    IF    "${Product}"== "TPAY"
    &{headers_mulesoft}=    Copy Dictionary    ${headers_mulesoft}    False
    Set to Dictionary    ${headers_mulesoft}    X-TPay-Latitude=${LAT}
    Set to Dictionary    ${headers_mulesoft}    X-TPay-Longitude=${LON}
    ELSE IF    "${Product}"== "TPAYX"
    &{headers_mulesoft}=    Copy Dictionary    ${headers_mulesoftX}    False
    Set to Dictionary    ${headers_mulesoft}    X-TEvo-Latitude=${LAT}
    Set to Dictionary    ${headers_mulesoft}    X-TEvo-Longitude=${LON}
    ${deviceID}    Get Variable Value    ${deviceID}
    IF    """${deviceID}""" != 'None'
    Set to Dictionary    ${headers_mulesoft}    X-TEvo-Device-Id=${deviceID}
    END
    END
    [Return]    ${headers_mulesoft}

CheckUSER
    [Arguments]    ${obuID}
    ###raccolta info
    CollectingINFO    CPDB06    ${obuID}
    Set Suite Variable    ${c_pro}    ${None}
    ###check stato contratto
    dbConnection    CPDB06
    Row Count Is Equal To X    SELECT * FROM EVDA.TETS27_CTR WHERE C_CTR ='${ctrID_str1}' and C_STA_CTR ='01'    1
    ###check stato titolo
    Row Count Is Equal To X    SELECT * FROM EVDA.TETSB6_STR_AUT_SER WHERE C_TIT ='${titleID_str1}' and C_TIP_AUT ='01'    1
    ###check soglia
    dbConnection    CPDB29
    Execute Sql String    UPDATE KBBA.TKBB01_CRE_CTR SET I_CRE=0 WHERE C_SER = 'XX' AND C_CTR ='${ctrID_str1}' AND D_FIN_VAL> CURRENT timestamp
    Execute Sql String    UPDATE KBBA.TKBB01_CRE_CTR SET N_TRX=0 WHERE C_CTR='${ctrID_str1}' AND C_SER='BO' AND D_FIN_VAL> CURRENT timestamp
    Execute Sql String    UPDATE KBBA.TKBB01_CRE_CTR SET N_TRX=0 WHERE C_CTR='${ctrID_str1}' AND C_SER='FU' AND D_FIN_VAL> CURRENT timestamp
    Disconnect from Database
