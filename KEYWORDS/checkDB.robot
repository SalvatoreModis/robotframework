*** Settings ***


*** Keywords ***
############################# Keyword per controlli a DB  #############################
ContrattoETitolo
    [Arguments]    ${supportCode}=${c_sup}
    IF    ${supportCode}<6000000000
        dbconnection    CPDB06
        ${query}      query    SELECT C_CTR, C_PRO FROM EVDA.TETSJU_ATT_PNG WHERE C_OBU_ID=${supportCode}    false   true
        ${c_ctr}    Get from Dictionary    ${query}[0]    C_CTR
        ${titleID}    query    SELECT C_TIT FROM evda.TETSD2_TIT WHERE C_CTR =${c_ctr} AND C_TIP_TIT = 'SM'    false    true
        Disconnect From Database
        ${c_pro}    Get from Dictionary    ${query}[0]    C_PRO
        ${titleID_str}    Get from Dictionary    ${titleID}[0]    C_TIT
        Set Suite Variable    ${c_ctr}      ${c_ctr}
        Set Suite Variable    ${c_pro}      ${c_pro}
        Set Suite Variable    ${c_tit}    ${titleID_str}
        Set Suite Variable    ${c_tip_tit}    SM
    ELSE
        ${serviceCode}    Get Variable Value    ${c_ser}    12
        ${contractInfo}    PEGA    ${serviceCode}    ${supportCode}
        ${contractInfo}   convert string to json    ${contractInfo}
        ${contractData}    Set Variable    ${contractInfo['contractData']}
        ${titleData}    Set Variable    ${contractInfo['titleData']}
        Set Suite Variable    ${c_ctr}    ${contractData['contractCode']}
        Set Suite Variable    ${c_pro}    ${contractData['productCode']}
        Set Suite Variable    ${c_tit}    ${titleData['supportId']}
        Set Suite Variable    ${c_tip_tit}    ${titleData['supportType']}
    END

CreaListeTrxDaResponse
    [Arguments]    ${response}    ${key_lista}=listaAutorizzazioni    ${key_trx}=codiceTransazioneMerchant    ${key_d_aut}=autorizzazioneId
    ${lista}    get from dictionary       ${response}     ${key_lista}
    ${lista_trx}    Create List
    ${lista_d_aut}    Create List
    FOR    ${x}    IN    @{lista}
        ${trx}    get from dictionary    ${x}    ${key_trx}
        ${d_aut}    get from dictionary    ${x}    ${key_d_aut}
        Append To List    ${lista_trx}    ${trx}
        Append To List    ${lista_d_aut}    ${d_aut}
    END
    Set Suite Variable    ${lista_trx}    ${lista_trx}
    Set Suite Variable    ${lista_d_aut}    ${lista_d_aut}

############################# Controllo crediti KBB - DB29 #############################

ControlloVKBB01
    [Documentation]    Suite variables aggiuntive necessarie:
                ...     -   per modificaPrenotazione -    transactionID_new, d_aut_new
                ...     -   per confermaMultipla     -    lista_trx, lista_d_aut (con le transazioni legate alla principale) - vedi CreaListeTrxDaResponse
                ...     -   per abbonamento          -    lista_trx, lista_d_aut (con le transazioni legate alla principale) - vedi CreaListeTrxDaResponse
                ...    Input: tipoMovimento IN (prenotazione, autorizzazione, conferma, storno, modificaPrenotazione, confermaMultipla, modifica, abbonamento, stornoAbbonamento)
    [Arguments]     ${tipoMovimento}    ${c_ser}=${c_ser}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}    ${c_sup}=${c_sup}
##+
	dbConnection    CPDB32
    ${DAUT}    Query    SELECT D_AUT FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${DAUT_str}    Get from Dictionary    ${DAUT}[0]    D_AUT
    ${Daut}    Convert to String    ${DAUT_str}
    ${Daut}    Replace String    ${Daut}    ${SPACE}    -
    ${Daut}    Replace String    ${Daut}    :    .
    Set Suite Variable    ${d_aut}    ${Daut}
##+
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}
    dbConnection        CPDB29
    ${query}   query    SELECT C_SUP, C_TIT, C_CTR, C_TIP_OPE_PAG, C_SER, C_IDE_TXN, F_ADD_ACC, D_TMS_PAG, C_TIP_TIT, F_AUT, F_FAT, C_STA_MOV, D_CON, D_TMS_CON, C_IDE_CON, D_ADD, D_TMS_ADD, C_IDE_ADD, D_FAT, D_TMS_FAT, C_IDE_FAT FROM KBBA.VKBB01_MOV WHERE C_IDE_AUT ='${d_aut}'    false    true
    Disconnect From Database

    &{VKBB01}    Create Dictionary      C_SUP=${c_sup}    C_TIT=${c_tit}    C_CTR=${c_ctr}    C_TIP_OPE_PAG=B	  C_SER=${c_ser}    C_IDE_TXN=${transactionID}	  F_ADD_ACC=A     D_TMS_PAG=${null}	  C_TIP_TIT=${c_tip_tit}	  F_AUT=S	  F_FAT=S	  C_STA_MOV=A${space}  	  D_CON=${null}	  D_TMS_CON=${null}	  C_IDE_CON=${null}	  D_ADD=${null}	  D_TMS_ADD=${null}	  C_IDE_ADD=${null}	  D_FAT=${null}	  D_TMS_FAT=${null}	  C_IDE_FAT=${null}
    IF       "${tipoMovimento}" == "prenotazione"
        Set Suite Variable     ${C_TIP_OPE_PAG}    B${space}
        Set Suite Variable     ${d_tms_pag}        ${null}
        Set To Dictionary    ${VKBB01}      C_TIP_OPE_PAG=${C_TIP_OPE_PAG}
    ELSE IF     "${tipoMovimento}" in ("autorizzazione","modifica","conferma")
        Set Suite Variable     ${C_TIP_OPE_PAG}    P${space}
        ${d_tms_pag_query}    Get from Dictionary    ${query}[0]    D_TMS_PAG
        Should Be True    "${d_tms_pag_query}"!=${null}
        Set Suite Variable     ${d_tms_pag}    ${d_tms_pag_query}
        Set To Dictionary    ${VKBB01}      C_TIP_OPE_PAG=${C_TIP_OPE_PAG}     D_TMS_PAG=${d_tms_pag}
    ELSE IF     "${tipoMovimento}" == "storno"
        Set To Dictionary    ${VKBB01}      C_TIP_OPE_PAG=${C_TIP_OPE_PAG}     D_TMS_PAG=${d_tms_pag}    F_ADD_ACC=R
    END

    IF    "${tipoMovimento}"=="modificaPrenotazione"
        ControlloVKBB01    storno    ${c_ser}    ${d_aut}    ${transactionID}
        ControlloVKBB01    prenotazione        ${c_ser}    ${d_aut_new}    ${transactionID_new}
    ELSE IF    "${tipoMovimento}"=="confermaMultipla"
        ${d_aut_new} 	Get Variable Value	${d_aut_new}	${d_aut}
        ${transactionID_new} 	Get Variable Value	${transactionID_new}	${transactionID}
        ControlloVKBB01    storno    ${c_ser}    ${d_aut_new}    ${transactionID_new}
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut}    IN ZIP    ${lista_trx}    ${lista_d_aut}
            ControlloVKBB01    autorizzazione    ${c_ser}    ${d_aut}    ${transactionID}
        END
    ELSE IF    "${tipoMovimento}"=="abbonamento"
        dbConnection        CPDB29
        Row Count Is 0    SELECT * FROM KBBA.VKBB01_MOV WHERE C_IDE_AUT ='${d_aut}'
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut}    IN ZIP    ${lista_trx}    ${lista_d_aut}
            ControlloVKBB01    autorizzazione    ${c_ser}    ${d_aut}    ${transactionID}
        END
    ELSE IF    "${tipoMovimento}"=="stornoAbbonamento"
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
         FOR    ${transactionID}    ${d_aut}    IN ZIP    ${lista_trx}    ${lista_d_aut}
           ControlloVKBB01    storno        ${c_ser}    ${d_aut}    ${transactionID}
        END
    ELSE
        ${query0}    Create Dictionary    &{query[0]}
        Should Be Equal   ${VKBB01}    ${query0}
        ${rowcount}    get length    ${query}
        IF    ${rowcount}>1    #controllo in caso di storno
            ${query1}    Create Dictionary    &{query[1]}
            Set To Dictionary    ${VKBB01}    C_TIP_OPE_PAG=R${space}    D_TMS_PAG=${null}
            Set To Dictionary    ${query1}    D_TMS_PAG=${null}     #in caso di modifica prenotazione uno non è valorizzato
            Should Be Equal   ${VKBB01}    ${query1}
        END
    END


AzzeraSoglieContratto
    [Arguments]    ${c_ctr}=${c_ctr}
    dbconnection    CPDB29
    Execute Sql String    UPDATE KBBA.TKBB01_CRE_CTR SET I_CRE=0 WHERE C_CTR=${c_ctr} AND D_FIN_VAL > CURRENT date
    Disconnect From Database

CercaNomeSoglia
    [Arguments]    ${c_ser}=${c_ser}    ${c_sup}=${c_sup}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    DbConnection    CPDB30
    ${nomeSoglia}   query    SELECT DISTINCT vs.C_SER FROM KBBA.VKBB01_SOG vs JOIN KBBA.VKBBMP_SER_GRP vsg ON vsg.C_GRP_SER=vs.C_SER WHERE vsg.C_SER ='${c_ser}' AND (C_PRO='${c_pro}' OR C_CTR='${c_ctr}' ) AND vs.C_SER NOT IN ('XX') AND vs.D_FIN_VAL>CURRENT timestamp    false    true
    Disconnect From Database
    IF    len(${nomeSoglia})>0
        ${nomeSoglia}    Get From Dictionary    ${nomeSoglia}[0]    C_SER
    ELSE
        ${nomeSoglia}   Set Variable    ${null}
    END
    Set Suite Variable    ${nomeSoglia}     ${nomeSoglia}

SetupSoglieServizio
    [Arguments]    ${c_ser}=${c_ser}    ${c_sup}=${c_sup}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    SetupSogliaXX    ${c_ctr}
    CercaNomeSoglia    ${c_ser}    ${c_sup}
    IF    '${nomeSoglia}'!='${null}'
        dbconnection    CPDB29
        ${query}    query    select I_CRE,N_TRX,C_TIP_SOG from KBBA.TKBB01_CRE_CTR where C_CTR = '${c_ctr}' and c_tip_sog != '00' and D_FIN_VAL > current timestamp and C_SER = '${nomeSoglia}' order by c_tip_sog desc    false    true
        Disconnect From Database
        ${row_count}    Get Length    ${query}

        Set Suite Variable    ${cre_g}      0
        Set Suite Variable    ${cre_m}      0
        Set Suite Variable    ${cre_a}      0
        Set Suite Variable    ${ntrx_g}     0
        Set Suite Variable    ${ntrx_m}     0
        Set Suite Variable    ${ntrx_a}     0
        IF    ${row_count}>0
            ${cre_a}     Get from Dictionary    ${query}[0]    I_CRE
            ${ntrx_a}    Get from Dictionary    ${query}[0]    N_TRX
            IF    ${row_count}>1
                ${cre_m}     Get from Dictionary    ${query}[1]    I_CRE
                ${ntrx_m}    Get from Dictionary    ${query}[1]    N_TRX
                IF    ${row_count}>2
                    ${cre_g}     Get from Dictionary    ${query}[2]    I_CRE
                    ${ntrx_g}    Get from Dictionary    ${query}[2]    N_TRX
                END
            END
        END
        Set Suite Variable    ${cre_g0}      ${cre_g}
        Set Suite Variable    ${cre_m0}      ${cre_m}
        Set Suite Variable    ${cre_a0}      ${cre_a}
        Set Suite Variable    ${ntrx_g0}     ${ntrx_g}
        Set Suite Variable    ${ntrx_m0}     ${ntrx_m}
        Set Suite Variable    ${ntrx_a0}     ${ntrx_a}
    END

CheckSoglieServizio
    [Documentation]    tipoMovimento viene usato solo per controllare se è storno/stornoAbbonamento o no
    [Arguments]      ${amountC}     ${tipoMovimento}    ${c_ctr}=${c_ctr}
##+
    ${amount}    Evaluate    ${amountC}/100
##+
    DbConnection    CPDB30
    ${flag_sogliaXX}    query    SELECT * FROM KBBA.VKBB01_SOG vs JOIN KBBA.VKBBMP_SER_GRP vsg ON vsg.C_GRP_SER=vs.C_SER WHERE vsg.C_SER ='${c_ser}' AND (C_PRO='${c_pro}' OR C_CTR='${c_ctr}' ) AND vs.C_SER IN ('XX') AND vs.D_FIN_VAL>CURRENT timestamp
    Disconnect From Database
    ${flag_sogliaXX}    Get Length    ${flag_sogliaXX}
    Set Suite Variable    ${flag_sogliaXX}    ${flag_sogliaXX}
    IF    ${flag_sogliaXX}>0
        CheckSogliaXX        ${amount}    ${tipoMovimento}
    ELSE
        CheckSogliaXX        ${0}    ${tipoMovimento}
    END
    IF    '${nomeSoglia}'!='${null}'
        dbconnection    CPDB29
        ${query}    query    select I_CRE,N_TRX from KBBA.TKBB01_CRE_CTR where C_CTR = '${c_ctr}' and c_tip_sog != '00' and D_FIN_VAL > current timestamp and C_SER = '${nomeSoglia}' order by c_tip_sog desc    false    true
        Disconnect From Database

        ${row_count}    Get Length    ${query}

        Set Suite Variable    ${cre_g}      0
        Set Suite Variable    ${cre_m}      0
        Set Suite Variable    ${cre_a}      0
        Set Suite Variable    ${ntrx_g}     0
        Set Suite Variable    ${ntrx_m}     0
        Set Suite Variable    ${ntrx_a}     0
        IF    ${row_count}>0
            ${cre_a}     Get from Dictionary    ${query}[0]    I_CRE
            ${ntrx_a}    Get from Dictionary    ${query}[0]    N_TRX
             IF    "${tipoMovimento}"!="storno" and "${tipoMovimento}"!="stornoAbbonamento"
                ${cre_a}     evaluate    ${cre_a}-${amount}
                ${ntrx_a}    evaluate    ${ntrx_a}-1
            END
            Should Be Equal As Numbers    ${cre_a0}      ${cre_a}
            Should Be Equal As Numbers    ${ntrx_a0}     ${ntrx_a}
            IF    ${row_count}>1
                ${cre_m}     Get from Dictionary    ${query}[1]    I_CRE
                ${ntrx_m}    Get from Dictionary    ${query}[1]    N_TRX
                IF    "${tipoMovimento}"!="storno" and "${tipoMovimento}"!="stornoAbbonamento"
                    ${cre_m}     evaluate    ${cre_m}-${amount}
                    ${ntrx_m}    evaluate    ${ntrx_m}-1
                END
                Should Be Equal As Numbers    ${cre_m0}      ${cre_m}
                Should Be Equal As Numbers    ${ntrx_m0}     ${ntrx_m}
                IF    ${row_count}>2
                    ${cre_g}     Get from Dictionary    ${query}[2]    I_CRE
                    ${ntrx_g}    Get from Dictionary    ${query}[2]    N_TRX
                    IF    "${tipoMovimento}"!="storno" and "${tipoMovimento}"!="stornoAbbonamento"
                        ${cre_g}     evaluate    ${cre_g}-${amount}
                        ${ntrx_g}    evaluate    ${ntrx_g}-1
                    END
                    Should Be Equal As Numbers    ${cre_g0}      ${cre_g}
                    Should Be Equal As Numbers    ${ntrx_g0}     ${ntrx_g}
                END
            END
        END
    END

SetupSogliaXX
    [Arguments]    ${c_ctr}=${c_ctr}
    dbconnection    CPDB29
    ${crediti}  query    select I_CRE,N_TRX from KBBA.TKBB01_CRE_CTR where C_CTR = ${c_ctr} and c_tip_sog != '00' and D_FIN_VAL > current timestamp and C_SER = 'XX' order by c_tip_sog   false   true
    disconnect from database
    ${row_count}    Get Length    ${crediti}
    IF    ${row_count}>0
        ${i_cre0}    Get from Dictionary    ${crediti}[0]    I_CRE
    ELSE
        ${i_cre0}    Set Variable    ${0}
    END
    Set Suite Variable    ${i_cre0}     ${i_cre0}

CheckSogliaXX
    [Arguments]    ${amount}   ${tipoMovimento}    ${c_ctr}=${c_ctr}
    dbconnection    CPDB29
    ${crediti}  query    select I_CRE,N_TRX,C_TIP_SOG from KBBA.TKBB01_CRE_CTR where C_CTR = ${c_ctr} and c_tip_sog != '00' and D_FIN_VAL > current timestamp and C_SER = 'XX' order by c_tip_sog   false   true
    Disconnect From Database
    ${i_cre}    get from dictionary    ${crediti}[0]    I_CRE
        IF  "${tipoMovimento}"!="storno" and "${tipoMovimento}"!="stornoAbbonamento"
            ${i_cre}    evaluate    ${i_cre}-${amount}
        END
    should be equal as numbers    ${i_cre0}     ${i_cre}


SetupSogliaTR
    [Arguments]    ${c_ctr}=${c_ctr}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}
    SetupSogliaXX    ${c_ctr}
    dbconnection    CPDB29
    ${crediti}  query    select I_CRE,N_TRX from KBBA.TKBB01_CRE_CTR where C_CTR = ${c_ctr} and c_tip_sog = '00' and D_FIN_VAL > current timestamp and C_SER = 'TR' order by c_tip_sog   false   true
    disconnect from database
    ${row_count}    Get Length    ${crediti}
    IF    ${row_count}>0
        ${i_creTR}    Get from Dictionary    ${crediti}[0]    I_CRE
    ELSE
        ${i_creTR}    Set Variable    ${0}
    END
    Set Suite Variable    ${i_creTR}     ${i_creTR}

CheckSogliaTR
    [Arguments]    ${amount}    ${c_ctr}=${c_ctr}
    CheckSogliaXX        ${0}    NULL
    dbconnection    CPDB29
    ${crediti}  query    select I_CRE,N_TRX from KBBA.TKBB01_CRE_CTR where C_CTR = ${c_ctr} and c_tip_sog = '00' and D_FIN_VAL > current timestamp and C_SER = 'TR' order by c_tip_sog   false   true
    Disconnect From Database
    ${i_cre}    get from dictionary    ${crediti}[0]    I_CRE
    ${i_cre}    evaluate    ${i_cre}-${amount}
    should be equal as numbers    ${i_creTR}     ${i_cre}


############################# Controlli TPay e TPayX - DB17 #############################

ControlloRiskShield
    [Documentation]    tipoMovimento IN (prenotazione, autorizzazione, conferma, modifica, storno , modificaPrenotazione, confermaMultipla, abbonamento e stornoAbbonamento)
    [Arguments]     ${amount}    ${tipoMovimento}    ${c_ser}=${c_ser}    ${c_sup}=${c_sup}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    dbconnection    CPDB17
    ${flag_RSenabled}    query    SELECT VAL_CH FROM KAAA.VKAA02_PARM_GEN WHERE C_OGG LIKE 'RISKSHIELD_ENABLED'    false    true
    Disconnect From Database
    IF    "${flag_RSenabled}[0]"=="{'VAL_CH': 'TRUE${space*26}'}"
        ${F_INV}    Set Variable    S
    ELSE
        ${F_INV}    Set Variable    N
    END

    dbconnection    CPDB17
    ${flag_ServiceEnabled}    Row Count    SELECT * FROM KAAA.TKAA05_MIN_STR WHERE C_SER=${c_ser} AND F_INV_RS='N'      false
    Disconnect From Database
    IF    ${flag_ServiceEnabled}>0
        dbconnection    CPDB17
        Row Count Is 0       SELECT * FROM KAAA.TKAA08_LOG_RSK_SLD WHERE C_IDE_TRX ='${transactionID}'
        Disconnect From Database
    ELSE IF    ${flag_ServiceEnabled}==0
        dbconnection    CPDB17
        ${query}    query    SELECT C_TIP_MSG, C_RES_KAA, C_CTR, C_SUP, C_ERR, T_MSG, T_ECC, F_INV FROM KAAA.TKAA08_LOG_RSK_SLD WHERE C_IDE_TRX ='${transactionID}' ORDER BY D_INS DESC     false    true
        ${query_amount}    query    SELECT I_TRX FROM KAAA.TKAA08_LOG_RSK_SLD WHERE C_IDE_TRX ='${transactionID}' ORDER BY D_INS DESC     false    true
        Disconnect From Database
        IF    "${tipoMovimento}" in ("autorizzazione","modifica","abbonamento")
            ${c_tip_msg}    set variable    AUTH
        ELSE IF     "${tipoMovimento}" in ("prenotazione","modificaPrenotazione")
            ${c_tip_msg}    set variable    PRE
        ELSE IF     "${tipoMovimento}" in ("conferma","confermaMultipla")
            ${c_tip_msg}    set variable    CONF
        ELSE IF     "${tipoMovimento}" in ("storno","stornoAbbonamento")
            ${c_tip_msg}    set variable    CANC
        END
        &{TKAA08}    Create Dictionary    C_TIP_MSG=${c_tip_msg}		C_RES_KAA=OK		C_CTR=${c_ctr}		C_SUP=${c_sup}		C_ERR=${null}		T_MSG=${null}		T_ECC=${null}		F_INV=${F_INV}    #C_RES=OK     T_HNT=

        ${query}    Create Dictionary    &{query[0]}
        Should Be Equal   ${TKAA08}    ${query}

        ${I_TRX}    Get from Dictionary    ${query_amount}[0]    I_TRX
        Should Be Equal As Numbers    ${amount}    ${I_TRX}
    END

Controllo_TKAA01_TKAA06
    [Documentation]    Input: tipoMovimento IN (prenotazione, autorizzazione, conferma, modifica, storno, modificaPrenotazione, confermaMultipla, abbonamento, stornoAbbonamento)
                ...    Suite variables aggiuntive necessarie:
                ...     -   per modificaPrenotazione -    amount_new, transactionID_new e d_aut_new
                ...     -   per confermaMultipla     -    lista_trx e lista_d_aut e listaImporti  (con le transazioni legate alla principale)
                ...     -   per abbonamento          -    lista_trx e lista_d_aut e listaImporti  (con le transazioni legate alla principale)
    [Arguments]     ${tipoMovimento}    ${amount}=${amount}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}    ${c_sup}=${c_sup}    ${c_ser}=${c_ser}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    IF    "${tipoMovimento}"=="modificaPrenotazione"
        Controllo_TKAA01_TKAA06    storno    ${amount}    ${d_aut}    ${transactionID}
        ControlloTKAA01    prenotazione    ${amount_new}    ${d_aut_new}    ${transactionID_new}
        Run Keyword If    ("${c_pro}" in ("FC","FE","FF"))     ControlloTKAA06    prenotazione    ${d_aut_new}    ${transactionID_new}

    ELSE IF    "${tipoMovimento}"=="confermaMultipla"
        ${amount_new} 	Get Variable Value	${amount_new}	${amount}
        ${d_aut_new} 	Get Variable Value	${d_aut_new}	${d_aut}
        ${transactionID_new} 	Get Variable Value	${transactionID_new}	${transactionID}
        ControlloTKAA01    conferma    ${amount_new}    ${d_aut_new}    ${transactionID_new}
        Run Keyword If    ("${c_pro}" in ("FC","FE","FF"))     ControlloTKAA06    conferma    ${d_aut_new}    ${transactionID_new}
        #controllo D_AUT_PRI e N_RAT
        ${count_rate}    Get Length    ${lista_trx}
        dbConnection        CPDB17
        Row Count Is Equal To X    SELECT * FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut_new}' AND D_AUT_PRI='${d_aut_new}' AND N_RAT='${count_rate}' ORDER BY D_AUT DESC      1
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut}    ${amount}    IN ZIP    ${lista_trx}    ${lista_d_aut}    ${listaImporti}
            ControlloTKAA01    autorizzazione    ${amount}    ${d_aut}    ${transactionID}
            #controllo D_AUT_PRI e N_RAT
            dbConnection        CPDB17
            Row Count Is Equal To X    SELECT * FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' AND N_RAT IS NULL AND D_AUT_PRI='${d_aut_new}' ORDER BY D_AUT DESC      1
            Disconnect From Database
        END

    ELSE IF    "${tipoMovimento}"=="abbonamento"
        Controllo_TKAA01_TKAA06    autorizzazione        ${amount}    ${d_aut}    ${transactionID}
        dbConnection        CPDB17
        Row Count Is Equal To X    SELECT D_AUT_PRI,N_RAT FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' AND D_AUT_PRI='${d_aut}' AND N_RAT='${12}' ORDER BY D_AUT DESC      1
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut_i}    ${amount}    IN ZIP    ${lista_trx}    ${lista_d_aut}    ${listaImporti}
            ControlloTKAA01    autorizzazione    ${amount}    ${d_aut_i}    ${transactionID}
            #controllo D_AUT_PRI e N_RAT
            ${count_rate}    Set Variable If    ${amount}==${27.5}    ='1'    IS NULL
            dbConnection        CPDB17
            Row Count Is Equal To X    SELECT D_AUT_PRI,N_RAT FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut_i}' AND D_AUT_PRI='${d_aut}' AND N_RAT ${count_rate} ORDER BY D_AUT DESC     1
            Disconnect From Database
        END

    ELSE IF    "${tipoMovimento}"=="stornoAbbonamento"
        Set Suite Variable    ${i_pag_aut}      ${amount}    #serve per controllo in TKAA01
        Set Suite Variable    ${i_pag_conf}     ${amount}    #serve per controllo in TKAA01
        ControlloTKAA01    ${tipoMovimento}    ${amount}    ${d_aut}    ${transactionID}
        Run Keyword If    ("${c_pro}" in ("FC","FE","FF"))     ControlloTKAA06    ${tipoMovimento}
        #controllo D_AUT_PRI e N_RAT
        ${count}    Get Length    ${lista_trx}
        dbConnection        CPDB17
        Row Count Is Equal To X    SELECT D_AUT_PRI,N_RAT FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' AND D_AUT_PRI='${d_aut}' AND N_RAT=12 ORDER BY D_AUT DESC     1
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut_i}    ${amount}    IN ZIP    ${lista_trx}    ${lista_d_aut}    ${listaImporti}
            Set Suite Variable    ${i_pag_aut}      ${amount}    #serve per controllo in TKAA01
            Set Suite Variable    ${i_pag_conf}     ${amount}    #serve per controllo in TKAA01
            ControlloTKAA01    stornoAbbonamento    ${amount}    ${d_aut_i}    ${transactionID}
##            #controllo D_AUT_PRI e N_RAT
            ${count_rate}    Set Variable If    ${amount}==${27.5}    ='1'    IS NULL
            dbConnection        CPDB17
            Row Count Is Equal To X    SELECT * FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut_i}' AND D_AUT_PRI='${d_aut}' AND N_RAT ${count_rate} ORDER BY D_AUT DESC      1
            Disconnect From Database
        END

    ELSE
        ControlloTKAA01    ${tipoMovimento}    ${amount}    ${d_aut}    ${transactionID}
        Run Keyword If    ("${c_pro}" in ("FC","FE","FF"))     ControlloTKAA06    ${tipoMovimento}    ${d_aut}    ${transactionID}
    END

ControlloTKAA01
    [Arguments]     ${tipoMovimento}    ${amount}=${amount}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}    ${c_sup}=${c_sup}    ${c_ser}=${c_ser}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    #creo i dizionari di base
    &{TKAA01}           Create Dictionary    C_TIP_AUT=03		C_SER=${c_ser}    D_PAG=${null}		T_INS_ESE=CANNE EST    C_IDE_TXN_ESE=${transactionID}		C_TIT=${c_tit}		C_TIP_TIT=${c_tip_tit}    C_SUP=${c_sup}${space*10}    C_CTR=${c_ctr}		N_PRG_RAG=${null}		F_STA_AUT=B		F_AUT=S		D_CON=${null}		C_ERR=${null}		T_ANO=${null}		F_FAT=S		C_PRO=${c_pro}		#D_PAG=2021-07-16
    #faccio le query a DB
    dbConnection        CPDB17
    ${query_01}             query    SELECT C_TIP_AUT, C_SER, D_PAG, T_INS_ESE, C_IDE_TXN_ESE, C_TIT, C_TIP_TIT, C_SUP, C_CTR, N_PRG_RAG, F_STA_AUT, F_AUT, D_CON, C_ERR,T_ANO,F_FAT,C_PRO FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' ORDER BY D_AUT DESC    false    true
    ${query_01_amount}     query    SELECT I_PAG_AUT, I_PAG_CONF FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' ORDER BY D_AUT DESC   false    true
    ${query_01_date}        query    SELECT D_CONF, D_STR FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' ORDER BY D_AUT DESC    false    true
    Disconnect From Database
    #setup D_CONF e D_STR
    ${d_conf}   get from dictionary    ${query_01_date}[0]     D_CONF
    ${d_str}    get from dictionary    ${query_01_date}[0]     D_STR
    Set Suite Variable    ${d_conf}     ${d_conf}
    Set Suite Variable    ${d_str}      ${d_str}
    #setup dizionario TKAA01 e controllo su D_CONF e D_STR
    IF       "${tipoMovimento}" == "prenotazione"
        Set Suite Variable    ${c_tip_aut}     03
        Set Suite Variable    ${f_sta_aut}     B
        Set Suite Variable    ${i_pag_aut}     ${amount}
        Set Suite Variable    ${i_pag_conf}    ${null}
    ELSE IF     "${tipoMovimento}" == "autorizzazione"
        Set Suite Variable    ${c_tip_aut}     01
        Set Suite Variable    ${f_sta_aut}     I
        Set Suite Variable    ${i_pag_aut}     ${amount}
        Set Suite Variable    ${i_pag_conf}    ${amount}
    ELSE IF     "${tipoMovimento}" == "conferma"
        Set Suite Variable    ${f_sta_aut}     P
        Set Suite Variable    ${i_pag_conf}    ${amount_new}

        Should Not Be Equal As Strings    ${d_conf}     ${null}
    ELSE IF    "${tipoMovimento}"=="storno" or "${tipoMovimento}"=="stornoAbbonamento"
        Set Suite Variable    ${f_sta_aut}     R

        Should Not Be Equal As Strings    ${d_str}     ${null}
    ELSE IF     "${tipoMovimento}" == "modifica"
        Set Suite Variable    ${f_sta_aut}     M
        Set Suite Variable    ${i_pag_aut}     ${amount_new}
        Set Suite Variable    ${i_pag_conf}    ${amount_new}
    END
    ${dataPagamento}      get current date    result_format=%Y-%m-%d
    Set To Dictionary     ${TKAA01}             C_TIP_AUT=${c_tip_aut}    F_STA_AUT=${f_sta_aut}     D_PAG=${dataPagamento}
    #trasformo la data nella query in stringa
    ${query_01}             Create Dictionary    &{query_01[0]}
    ${dataPagamento}    Convert To String    ${query_01['D_PAG']}
    set to dictionary    ${query_01}    D_PAG=${dataPagamento}
    #controllo TKAA01
    Should Be Equal    ${TKAA01}    ${query_01}
    ${query_01_amount}     Create Dictionary    &{query_01_amount[0]}
    ${i_pag_aut_query}     Get From Dictionary    ${query_01_amount}    I_PAG_AUT
    ${i_pag_conf_query}    Get From Dictionary    ${query_01_amount}    I_PAG_CONF
    Should Be Equal As Numbers    ${i_pag_aut}    ${i_pag_aut_query}
    IF    "${i_pag_conf}"!="None"
        Should Be Equal As Numbers    ${i_pag_conf}    ${i_pag_conf_query}
    ELSE
        Should Be Equal As Strings    ${i_pag_conf}    ${i_pag_conf_query}
    END

ControlloTKAA06
    [Arguments]     ${tipoMovimento}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}
    #creo i dizionari di base
    &{TKAA06}           Create Dictionary    C_WS_3PA=${null}    T_ECC=${null}       C_ESI=OK

    #setup C_WS_3PA
    IF       "${tipoMovimento}" == "prenotazione"
        Set Suite Variable    ${c_ws_3pa}      01
    ELSE IF     "${tipoMovimento}" == "autorizzazione"
        IF         ${amount}>0
            Set Suite Variable    ${c_ws_3pa}    04
        ELSE IF    ${amount}<0
            Set Suite Variable    ${c_ws_3pa}    06
        ELSE
            Set Suite Variable    ${c_ws_3pa}    nullo
        END
    ELSE IF     "${tipoMovimento}" == "conferma"
        IF    ${amount_new}==0
            Set Suite Variable    ${c_ws_3pa}    02
        ELSE
            Set Suite Variable    ${c_ws_3pa}    03
        END
    ELSE IF    "${tipoMovimento}"=="storno"
        IF         "${c_ws_3pa}"=="06"
            Set Suite Variable    ${c_ws_3pa}    04
        ELSE IF    "${c_ws_3pa}"=="02" or "${c_ws_3pa}"=="nullo"     #è 02 se ho fatto conferma nulla
            Set Suite Variable    ${c_ws_3pa}    nullo
        ELSE IF    "${c_ws_3pa}"=="03"
            Set Suite Variable    ${c_ws_3pa}    05
        ELSE IF    "${c_ws_3pa}"=="01"
            Set Suite Variable    ${c_ws_3pa}    02
        ELSE
            Set Suite Variable    ${c_ws_3pa}    06
        END
    ELSE IF     "${tipoMovimento}" == "modifica"
        ${var}     Set Variable    ${c_ws_3pa}
        IF    ${amount_new}<${amount}
            ${c_ws_3pa}     Set Variable    06
        ELSE IF    ${amount_new}==${amount}
            ${c_ws_3pa}     Set Variable    nullo
        ELSE
            ${c_ws_3pa}     Set Variable    04
        END
        IF    "${var}"=="nullo" and ${amount_new}!=0
            Set Suite Variable    ${c_ws_3pa}    ${c_ws_3pa}
        ELSE IF    "${var}"=="nullo" and ${amount_new}==0
            Set Suite Variable    ${c_ws_3pa}    nullo
        END
    ELSE IF    "${tipoMovimento}"=="stornoAbbonamento"
        Set Suite Variable    ${c_ws_3pa}    05
    END
    Set To Dictionary    ${TKAA06}    C_WS_3PA=${c_ws_3pa}

    IF      "${c_ws_3pa}"!="nullo"
        dbConnection        CPDB17
        ${queryBNL}    query    SELECT C_WS_3PA, T_ECC, C_ESI FROM KAAA.TKAA06_LOG_AUT_BNL WHERE D_AUT ='${d_aut}' ORDER BY D_INS DESC LIMIT 1    false    true
        Disconnect From Database
        ${queryBNL}    Create Dictionary    &{queryBNL[0]}
        Should Be Equal   ${TKAA06}    ${queryBNL}
        IF    "${tipoMovimento}"=="prenotazione" or "${tipoMovimento}"=="conferma"
            dbConnection        CPDB17
            ${N_IMP}        query    SELECT N_IMP FROM KAAA.TKAA06_LOG_AUT_BNL WHERE D_AUT ='${d_aut}' ORDER BY D_INS DESC LIMIT 1    false    true
            ${c_ide_aut}    query    SELECT C_IDE_AUT FROM KAAA.TKAA01_AUT_PAG WHERE D_AUT ='${d_aut}' ORDER BY D_AUT DESC LIMIT 1    false    true
            Disconnect From Database
            ${N_IMP}        Get from Dictionary    ${N_IMP}[0]    N_IMP
            ${c_ide_aut}    Get from Dictionary    ${c_ide_aut}[0]    C_IDE_AUT
            Should Be Equal As Strings    ${N_IMP}      ${c_ide_aut}
        END
    ELSE
        DbConnection    CPDB17
        Row Count Is 0    SELECT C_WS_3PA, T_ECC, C_ESI FROM KAAA.TKAA06_LOG_AUT_BNL WHERE D_AUT ='${d_aut}' ORDER BY D_INS DESC    false
        Disconnect From Database
    END

ControlloTKAA02
    [Arguments]    ${n_chiamate}=1    ${transactionID}=${transactionID}
    DbConnection    CPDB17
    Row Count Is Equal To X    SELECT * FROM KAAA.TKAA02_LOG_OPE_ETF WHERE C_TRA_ID='${transactionID}' AND T_ECC='' AND C_ESI_OPE='S'    ${n_chiamate}
    Disconnect From Database



############################# Controlli BigBang - DB46 #############################

ControlloRiskShield_BB
    [Documentation]    tipoMovimento IN (prenotazione, autorizzazione, conferma, modifica, storno , modificaPrenotazione, confermaMultipla, abbonamento e stornoAbbonamento)
    [Arguments]     ${amount}    ${tipoMovimento}    ${c_ser}=${c_ser}    ${c_sup}=${c_sup}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    ${c_req_id}    Setup c_req_id    ${tipoMovimento}    ${transactionID}
    ${F_INV}    Set Variable    S

    dbconnection    CPDB46
    ${query}    query    select c_tip_msg, c_res_ae, c_ctr, c_sup, c_sta_htt, f_inv from kaea.tkae05_rsk_sld_log where c_ide_trx ='${transactionID}' and c_res in ('OK','WR') and c_ide_msg ='${c_req_id}' order by d_reg desc     false    true
    ${query_amount}    query    select i_trx from kaea.tkae05_rsk_sld_log where c_ide_trx ='${transactionID}' and c_res in ('OK','WR') and c_ide_msg ='${c_req_id}' order by d_reg desc     false    true
    Disconnect From Database

    IF    "${tipoMovimento}" in ("autorizzazione","modifica","abbonamento")
        ${c_tip_msg}    set variable    AUTH
    ELSE IF     "${tipoMovimento}" in ("prenotazione","modificaPrenotazione")
        ${c_tip_msg}    set variable    PRE
    ELSE IF     "${tipoMovimento}" in ("conferma","confermaMultipla")
        ${c_tip_msg}    set variable    CONF
    ELSE IF     "${tipoMovimento}" in ("storno","stornoAbbonamento")
        ${c_tip_msg}    set variable    CANC
    END
    &{TKAE05}    Create Dictionary    c_tip_msg=${c_tip_msg}		c_res_ae=OK		c_ctr=${c_ctr}		c_sup=${c_sup}		c_sta_htt=200		f_inv=${F_INV}

    ${query}    Create Dictionary    &{query[0]}
    Should Be Equal   ${TKAE05}    ${query}

    ${I_TRX}    Get from Dictionary    ${query_amount}[0]    i_trx
    Should Be Equal As Numbers    ${amount}    ${I_TRX}



ControlloDB46
    [Documentation]    Input: tipoMovimento IN (prenotazione, autorizzazione, conferma, modifica, storno, modificaPrenotazione, confermaMultipla, abbonamento, stornoAbbonamento)
                ...    Suite variables aggiuntive necessarie:
                ...     -   per modificaPrenotazione -    amount_new, transactionID_new e d_aut_new
                ...     -   per confermaMultipla     -    lista_trx e lista_d_aut e listaImporti  (con le transazioni legate alla principale)
                ...     -   per abbonamento          -    lista_trx e lista_d_aut e listaImporti  (con le transazioni legate alla principale)
    [Arguments]     ${tipoMovimento}    ${amount}=${amount}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}    ${c_sup}=${c_sup}    ${c_ser}=${c_ser}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    ${c_req_id}    Setup c_req_id    ${tipoMovimento}    ${transactionID}
    ControlloPega    ${c_req_id}    ${tipoMovimento}
    ControlloBilling    ${c_req_id}

    IF    "${tipoMovimento}"=="modificaPrenotazione"
        ControlloTKAE02    storno    ${amount}    ${d_aut}    ${transactionID}
        ControlloTKAE02    prenotazione    ${amount_new}    ${d_aut_new}    ${transactionID_new}

    ELSE IF    "${tipoMovimento}"=="confermaMultipla"
        ${amount_new} 	Get Variable Value	${amount_new}	${amount}
        ${d_aut_new} 	Get Variable Value	${d_aut_new}	${d_aut}
        ${transactionID_new} 	Get Variable Value	${transactionID_new}	${transactionID}
        ControlloTKAE02    conferma    ${amount_new}    ${d_aut_new}    ${transactionID_new}
        #controllo D_AUT_PRI e N_RAT
        ${count_rate}    Get Length    ${lista_trx}
        dbConnection      CPDB46
        Row Count Is Equal To X    select * from kaea.tkae02_aut WHERE c_ide_aut ='${d_aut_new}' AND c_ide_aut_pri ='${d_aut_new}' AND n_rat ='${count_rate}' order by c_ide_aut desc      1
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut}    ${amount}    IN ZIP    ${lista_trx}    ${lista_d_aut}    ${listaImporti}
            ControlloTKAE02    autorizzazione    ${amount}    ${d_aut}    ${transactionID}
            #controllo D_AUT_PRI e N_RAT
            dbConnection     CPDB46
            Row Count Is Equal To X    select * from kaea.tkae02_aut where c_ide_aut ='${d_aut}' AND n_rat IS NULL AND c_ide_aut_pri ='${d_aut_new}' order by c_ide_aut desc      1
            Disconnect From Database
        END

    ELSE IF    "${tipoMovimento}"=="abbonamento"
        ControlloTKAE02    autorizzazione        ${amount}    ${d_aut}    ${transactionID}
        dbConnection        CPDB46
        Row Count Is Equal To X    select c_ide_aut_pri, n_rat from kaea.tkae02_aut where c_ide_aut ='${d_aut}' AND c_ide_aut_pri ='${d_aut}' AND n_rat ='${12}' order by c_ide_aut desc      1
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut_i}    ${amount}    IN ZIP    ${lista_trx}    ${lista_d_aut}    ${listaImporti}
            ControlloTKAE02    autorizzazione    ${amount}    ${d_aut_i}    ${transactionID}
            #controllo D_AUT_PRI e N_RAT
            ${count_rate}    Set Variable If    ${amount}==${27.5}    ='1'    IS NULL
            dbConnection        CPDB46
            Row Count Is Equal To X    select c_ide_aut_pri, n_rat from kaea.tkae02_aut where c_ide_aut ='${d_aut_i}' AND c_ide_aut_pri ='${d_aut}' AND n_rat ${count_rate} order by c_ide_aut desc      1
            Disconnect From Database
        END

    ELSE IF    "${tipoMovimento}"=="stornoAbbonamento"
        Set Suite Variable    ${i_pag_aut}      ${amount}    #serve per controllo in TKAA01
        Set Suite Variable    ${i_pag_conf}     ${amount}    #serve per controllo in TKAA01
        ControlloTKAE02    ${tipoMovimento}    ${amount}    ${d_aut}    ${transactionID}
        #controllo D_AUT_PRI e N_RAT
        ${count}    Get Length    ${lista_trx}
        dbConnection      CPDB46
        Row Count Is Equal To X    select c_ide_aut_pri, n_rat from kaea.tkae02_aut where c_ide_aut ='${d_aut}' AND c_ide_aut_pri ='${d_aut}' AND n_rat=12 order by c_ide_aut desc      1
        Disconnect From Database
        ###estrarre le trx dalla lista e richiamare il controllo con tipo autorizzazione
        FOR    ${transactionID}    ${d_aut_i}    ${amount}    IN ZIP    ${lista_trx}    ${lista_d_aut}    ${listaImporti}
            Set Suite Variable    ${i_pag_aut}      ${amount}    #serve per controllo in TKAE02
            Set Suite Variable    ${i_pag_conf}     ${amount}    #serve per controllo in TKAE02
            ControlloTKAE02    stornoAbbonamento    ${amount}    ${d_aut_i}    ${transactionID}
##            #controllo D_AUT_PRI e N_RAT
            ${count_rate}    Set Variable If    ${amount}==${27.5}    ='1'    IS NULL
            dbConnection      CPDB46
            Row Count Is Equal To X    select * from kaea.tkae02_aut where c_ide_aut ='${d_aut_i}' AND c_ide_aut_pri ='${d_aut}' AND n_rat ${count_rate} order by c_ide_aut desc      1
            Disconnect From Database
        END
    ELSE
        ControlloTKAE02    ${tipoMovimento}    ${amount}    ${d_aut}    ${transactionID}
    END

ControlloTKAE02
    [Arguments]     ${tipoMovimento}    ${amount}=${amount}    ${d_aut}=${d_aut}    ${transactionID}=${transactionID}    ${c_sup}=${c_sup}    ${c_ser}=${c_ser}
    # imposto c_ctr, c_pro e c_tit
    ${c_pro}    Get Variable Value    ${c_pro}
    Run Keyword If    """${c_pro}""" == 'None'    ContrattoETitolo    ${c_sup}

    #creo i dizionari di base
    &{TKAE02}     Create Dictionary    c_tip_aut=03		c_ser=${c_ser}    t_ins_ese=STAZIONE Q8 CENTRO DEI BORGHI    c_ide_trx=${transactionID}		c_tit=${c_tit}		c_tip_tit=${c_tip_tit}    c_sup=${c_sup}    c_ctr=${c_ctr}		n_prg_rag=${null}		f_sta_aut=B		f_aut=S    d_con=${null}		c_err=${null}		t_ano=${null}		f_fat=${null}		c_pro=${c_pro}
    #faccio le query a DB
    dbConnection    CPDB46
    ${query}           query    select c_tip_aut, c_ser, t_ins_ese, c_ide_trx, c_tit, c_tip_tit, c_sup, c_ctr, n_prg_rag, f_sta_aut, f_aut, d_con, c_err, t_ano, f_fat, c_pro from kaea.tkae02_aut where c_ide_aut ='${d_aut}' order by c_ide_aut desc    false    true
    ${query_amount}    query    select i_pag_aut, i_pag_conf from kaea.tkae02_aut where c_ide_aut ='${d_aut}' order by c_ide_aut desc    false    true
    ${query_date}      query    select d_conf, d_str from kaea.tkae02_aut where c_ide_aut ='${d_aut}' order by c_ide_aut desc    false    true
    Disconnect From Database
    ${query}    Set Variable    ${query[0]}

    #setup D_CONF e D_STR
    ${d_conf}   get from dictionary    ${query_date}[0]     d_conf
    ${d_str}    get from dictionary    ${query_date}[0]     d_str
    Set Suite Variable    ${d_conf}     ${d_conf}
    Set Suite Variable    ${d_str}      ${d_str}
    #setup dizionario TKAA01 e controllo su D_CONF e D_STR
    IF       "${tipoMovimento}" == "prenotazione"
        Set Suite Variable    ${c_tip_aut}     03
        Set Suite Variable    ${f_sta_aut}     B
        Set Suite Variable    ${i_pag_aut}     ${amount}
        Set Suite Variable    ${i_pag_conf}    ${null}
    ELSE IF     "${tipoMovimento}" == "autorizzazione"
        Set Suite Variable    ${c_tip_aut}     01
        Set Suite Variable    ${f_sta_aut}     I
        Set Suite Variable    ${i_pag_aut}     ${amount}
        Set Suite Variable    ${i_pag_conf}    ${amount}
    ELSE IF     "${tipoMovimento}" == "conferma"
        Set Suite Variable    ${f_sta_aut}     P
        Set Suite Variable    ${i_pag_conf}    ${amount_new}

        Should Not Be Equal As Strings    ${d_conf}     ${null}
    ELSE IF    "${tipoMovimento}"=="storno" or "${tipoMovimento}"=="stornoAbbonamento"
        Set Suite Variable    ${f_sta_aut}     R

        Should Not Be Equal As Strings    ${d_str}     ${null}
    ELSE IF     "${tipoMovimento}" == "modifica"
        Set Suite Variable    ${f_sta_aut}     M
        Set Suite Variable    ${i_pag_aut}     ${amount_new}
        Set Suite Variable    ${i_pag_conf}    ${amount_new}
    END
    Set To Dictionary     ${TKAE02}     c_tip_aut=${c_tip_aut}    f_sta_aut=${f_sta_aut}
    #controllo TKAA01
    Should Be Equal    ${TKAE02}    ${query}
    ${query_amount}     Create Dictionary    &{query_amount[0]}
    ${i_pag_aut_query}     Get From Dictionary    ${query_amount}    i_pag_aut
    ${i_pag_conf_query}    Get From Dictionary    ${query_amount}    i_pag_conf
    Should Be Equal As Numbers    ${i_pag_aut}    ${i_pag_aut_query}
    IF    "${i_pag_conf}"!="None"
        Should Be Equal As Numbers    ${i_pag_conf}    ${i_pag_conf_query}
    ELSE
        Should Be Equal As Strings    ${i_pag_conf}    ${i_pag_conf_query}
    END

Setup c_req_id
    [Arguments]    ${tipoMovimento}    ${transactionID}=${transactionID}
    IF    "${tipoMovimento}" in ("autorizzazione","prenotazione","abbonamento")
        ${t_flu}    Set Variable    authorize
    ELSE IF    "${tipoMovimento}" in ("modificaPrenotazione","modifica")
        ${t_flu}    Set Variable    modify
    ELSE IF    "${tipoMovimento}" == "conferma"
        ${t_flu}    Set Variable    confirm
    ELSE IF    "${tipoMovimento}" == "confermaMultipla"
        ${t_flu}    Set Variable    multipleConfirm
    ELSE IF    "${tipoMovimento}" in ("storno","stornoAbbonamento")
         ${t_flu}    Set Variable    refund
    ELSE
        Log    tipoMovimento non valido
    END
    DbConnection    CPDB46
    Row Count Is Equal To X   select * from kaea.tkae01_ope_log where c_ide_trx='${transactionID}' AND t_flu='${t_flu}' AND c_err is null AND c_esi_ope='200'     1
    ${query}    Query    select c_req_id from kaea.tkae01_ope_log where c_ide_trx='${transactionID}' AND t_flu='${t_flu}' AND c_err is null AND c_esi_ope='200'     false    true
    Disconnect From Database
    ${c_req_id}    get from dictionary    ${query}[0]     c_req_id
    [Return]    ${c_req_id}

ControlloPega
    [Documentation]    Input: tipoMovimento IN (prenotazione, autorizzazione, conferma, modifica, storno, modificaPrenotazione, confermaMultipla, abbonamento, stornoAbbonamento)
    [Arguments]    ${c_req_id}    ${tipoMovimento}
    IF    "${tipoMovimento}" in ("autorizzazione","prenotazione","abbonamento","modificaPrenotazione","modifica")
        DbConnection    CPDB46
        Row Count Is Equal To X    select * from kaea.tkae03_peg_log where c_req_id ='${c_req_id}' and c_esi_ope='200' and c_err is null and t_err is null    1
        Disconnect From Database
    ELSE IF    "${tipoMovimento}" in ("conferma","confermaMultipla","storno","stornoAbbonamento")
        DbConnection    CPDB46
        Row Count Is 0    select * from kaea.tkae03_peg_log where c_req_id ='${c_req_id}' and c_esi_ope='200' and c_err is null and t_err is null
        Disconnect From Database
    END

ControlloBilling
    [Documentation]    Input: tipoMovimento IN (prenotazione, autorizzazione, conferma, modifica, storno, modificaPrenotazione, confermaMultipla, abbonamento, stornoAbbonamento)
    [Arguments]    ${c_req_id}
        DbConnection    CPDB46
        Row Count Is Equal To X    select * from kaea.tkae04_bil_log where c_req_id ='${c_req_id}' and c_esi_ope='200' and c_err is null and t_err is null    1
        Disconnect From Database






