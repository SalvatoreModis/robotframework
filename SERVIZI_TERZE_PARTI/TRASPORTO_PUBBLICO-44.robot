*** Settings ***
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/Body.txt
Resource          schemaJson.txt
Resource          ../KEYWORDS/checkDB.robot

*** Variables ***
${endpoint}       https://ws-test.telepass.com
${access_token}    ${EMPTY}
#${obuID}         1000119691
${transactionID}    ${EMPTY}
${update_response}    ${EMPTY}
@{dettagli}
${outputUpdatebody}    ${EMPTY}
${C_SER}          44

*** Test Cases ***
CHECKUSER
    Set Suite Variable    ${obuID}    1000125623
    Set Suite Variable    ${PROD}    TPAY
    CheckUSER    ${obuID}

LOGIN
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

PAGAMENTO_INSTALMENTS
    ${abbonamentoATM_json}    evaluate    json.loads('''${body_abbonamentoATM}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    serviceCode=${C_SER}
    ${time}    Evaluate    int(round(time.time() * 1000))
    ${time1}    Evaluate    ${time}+ 432000000
    ${uuid}=    Evaluate    uuid.uuid4()    modules=uuid
    Set to Dictionary    ${abbonamentoATM_json}    pointOfService=${POS}
    Set to Dictionary    ${abbonamentoATM_json}    pyngCode=${obuID}
    Set to Dictionary    ${abbonamentoATM_json}    externalId='${uuid}'
    Set to Dictionary    ${abbonamentoATM_json}    validityEndDate=${time1}
    Set to Dictionary    ${abbonamentoATM_json}    validityStartDate=${time}
    ${output}    PostRequest3part    ${endpoint}    ${basepath_instalments}    ${abbonamentoATM_json}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    instalmentsATM    ${output.json()}
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX_PRI='${transactionID}' AND F_STA_TRX='30'    4
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA25_RAT_SPE WHERE C_IDE_TRX_PRI='${transactionID}' AND C_STA='E' UNION SELECT * FROM KDAA.TKDA25_RAT_SPE WHERE C_IDE_TRX_PRI='${transactionID}' AND C_STA='D'    14
    Disconnect from Database
