*** Settings ***
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/Body.txt
Resource          schemaJson.txt
Resource          ../KEYWORDS/checkDB.robot

*** Variables ***
${access_token}    ${EMPTY}
${transactionID}    ${EMPTY}
${update_response}    ${EMPTY}
@{dettagli}
${outputUpdatebody}    ${EMPTY}
${C_SER}          63
${DAUT_str}       ${EMPTY}
${amount}         ${222}

*** Test Cases ***
CHECKUSER
    Set Suite Variable    ${obuID}    1000125623
    Set Suite Variable    ${PROD}    TPAY
    CheckUSER    ${obuID}

LOGIN
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}
    #SchemaValidator    token    ${output.json()}    fallisce manca agreement code sul token

PRENOTAZIONE3PART
    ##set soglie in base al servizio
    SetupSoglieServizio    ${C_SER}    ${obuID}
    #preparazione json prenotazione
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    #seleziona il punto di erogazione in base alle ultime transazioni sulla tkda05
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Roma(RM)
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    amount    ${amount}
    #richiesta prenotazione
    ${output}    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    prenotazione    ${output.json()}
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}
    #####    richiamo controllo su contatori crediti
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    ######### aggiornamento dati transazione
    ${output}    Set to Dictionary    ${output.json()}    terminalId=12455    merchantOrderId=AAAA-BBBB.
    ${outputUpdate}    PutRequest3part    ${endpoint}    ${basepath_aggiornamento3part}${transactionID}    ${output}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    aggiornamento    ${outputUpdate.json()}
    ${details}    CreateDictionaryList    ${C_SER}
    ${outputUpdatebody}    Set to Dictionary    ${outputUpdate.json()}    details=${details}
    Set Suite Variable    ${outputUpdatebody}    ${outputUpdatebody}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='10'    1
    Disconnect from Database

CONFERMA-DELETE3PART
    ${output}    PostRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}/authorize    ${outputUpdatebody}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    conferma    ${output.json()}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    conferma
    ####additional data
    ${additionaldata_json}    evaluate    json.loads('''${body_additionaldata63}''')    json
    ${outputAdditional}    PostRequest3part    ${endpoint}    ${basepath_additionaldata}${transactionID}/additionalData    ${additionaldata_json}    ${access_token}    ${obuID}    ${PROD}    nopaymenttype
    dbConnection    CPDB32
    Row Count Is Less Than X    SELECT * FROM KDBA.TKDB03_ADD_DAT_TRX WHERE C_IDE_TRX='${transactionID}'    14
    Disconnect from Database
    ####delete request
    ${outputDel}    DeleteRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}    ${output.json()}    ${access_token}    ${obuID}    ${PROD}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97'    1
    Disconnect from Database
    ControlloVKBB01    storno    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    storno

CHECKUSERX
    Set Suite Variable    ${obuID}    1000116366
    Set Suite Variable    ${PROD}    TPAYX
    CheckUSER    ${obuID}

LOGINX
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}
    #SchemaValidator    token    ${output.json()}    fallisce manca agreement code sul token

PRENOTAZIONE3PARTX
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Roma(RM)
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    amount    ${amount}
    ${output}    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    prenotazione    ${output.json()}
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    ${output}    Set to Dictionary    ${output.json()}    terminalId=12455    merchantOrderId=AAAA-BBBB.
    ${outputUpdate}    PutRequest3part    ${endpoint}    ${basepath_aggiornamento3part}${transactionID}    ${output}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    aggiornamento    ${outputUpdate.json()}
    ${details}    CreateDictionaryList    ${C_SER}
    ${outputUpdatebody}    Set to Dictionary    ${outputUpdate.json()}    details=${details}
    Set Suite Variable    ${outputUpdatebody}    ${outputUpdatebody}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='10'    1
    Disconnect from Database

CONFERMA-DELETE3PARTX
    ${output}    PostRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}/authorize    ${outputUpdatebody}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    conferma    ${output.json()}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    conferma
    ####additional data
    ${additionaldata_json}    evaluate    json.loads('''${body_additionaldata63}''')    json
    ${outputAdditional}    PostRequest3part    ${endpoint}    ${basepath_additionaldata}${transactionID}/additionalData    ${additionaldata_json}    ${access_token}    ${obuID}    ${PROD}    nopaymenttype
    dbConnection    CPDB32
    Row Count Is Less Than X    SELECT * FROM KDBA.TKDB03_ADD_DAT_TRX WHERE C_IDE_TRX='${transactionID}'    14
    Disconnect from Database
    Sleep    10
    ####delete request
    ${outputDel}    DeleteRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}    ${output.json()}    ${access_token}    ${obuID}    ${PROD}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97'    1
    Disconnect from Database
    ControlloVKBB01    storno    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    storno
