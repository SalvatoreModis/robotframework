*** Settings ***
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/Body.txt
Resource          schemaJson.txt
Resource          ../KEYWORDS/checkDB.robot

*** Variables ***
${access_token}    ${EMPTY}
${transactionID}    ${EMPTY}
${update_response}    ${EMPTY}
@{dettagli}
${outputUpdatebody}    ${EMPTY}
${C_SER}          37

*** Test Cases ***
CHECKUSER
    Set Suite Variable    ${obuID}    1000125623
    Set Suite Variable    ${PROD}    TPAY
    CheckUSER    ${obuID}

LOGIN
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

PAGAMENTO_INSTALMENTS
    ${traghetti_json}    evaluate    json.loads('''${body_traghetti}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    serviceCode=${C_SER}
    Set to Dictionary    ${traghetti_json}    pointOfService    ${POS}
    ${time}    Evaluate    int(round(time.time() * 1000))
    ${time1}    Evaluate    ${time}+ 432000000
    ${random} =    Evaluate    random.randint(0, 99999999)
    Set to Dictionary    ${traghetti_json}    externalId=WS${random}
    Set to Dictionary    ${traghetti_json}    validityEndDate=${time1}
    Set to Dictionary    ${traghetti_json}    validityStartDate=${time}
    ${output}    PostRequest3part    ${endpoint}    ${basepath_instalments}    ${traghetti_json}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    instalments    ${output.json()}
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX_PRI='${transactionID}' AND F_STA_TRX='30'    3
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX a JOIN KDAA.TKDA03_OPE_REQ_PRD b ON a.D_INS=b.D_INS WHERE a.C_IDE_TRX_PRI='${transactionID}' AND C_TIP_TRX='S'    4
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA25_RAT_SPE WHERE C_IDE_TRX_PRI='${transactionID}' AND C_STA='E'    2
    Disconnect from Database
