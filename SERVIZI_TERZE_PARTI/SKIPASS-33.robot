*** Settings ***
Resource          ../DATA/BasePathMulesoftRT.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/Body.txt
Resource          schemaJson.txt
Resource          ../KEYWORDS/checkDB.robot

*** Variables ***
${access_token}    ${EMPTY}
${transactionID}    ${EMPTY}
${update_response}    ${EMPTY}
@{dettagli}
${outputUpdatebody}    ${EMPTY}
${C_SER}          33
${DAUT_str}       ${EMPTY}
${amount}         ${222}

*** Test Cases ***
CHECKUSER
    Set Suite Variable    ${obuID}    1000125623
    Set Suite Variable    ${PROD}    TPAY
    CheckUSER    ${obuID}

LOGIN
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

PRENOTAZIONE3PART
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    merchantOrderId=1643044375064
    Set to Dictionary    ${checkin_json}    amount    ${amount}
    ${output}    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    prenotazioneSkipass    ${output.json()}
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='10'    1
    Disconnect from Database

MODIFICAPRENOTAZIONE3PART
    ${modifica_json}    evaluate    json.loads('''${body_modificaSkipass}''')    json
    ${output}    PatchRequest3part    ${endpoint}    ${basepath_modificaSkipass}${transactionID}/amount    ${modifica_json}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    prenotazioneSkipass    ${output.json()}
    #CONFERMA3PART
    #    ${output}    PostRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}/authorize    ${outputUpdatebody}    ${access_token}    ${obuID}    ${PROD}
    #    SchemaValidator    conferma    ${output.json()}
    #    dbConnection    CPDB32
    #    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    #    Disconnect from Database

CONFERMAMULTI3PART
    ${confermaMulti_json}    evaluate    json.loads('''${body_confermaMultiSkipass}''')    json
    ${OPTIONAL}    Set Variable    NOPAYTYPE
    ${output}    PostRequest3part    ${endpoint}    ${basepath_confermaMultiSkipass}${transactionID}/confirm    ${confermaMulti_json}    ${access_token}    ${obuID}    ${PROD}    ${OPTIONAL}
    SchemaValidator    confermaMultiSkipass    ${output.json()}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX_PRI='${transactionID}' AND F_STA_TRX='30'    2
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX_PRI='${transactionID}' AND F_STA_TRX='91'    1
    Disconnect from Database

CHECKUSERX
    Set Suite Variable    ${obuID}    1000116366
    Set Suite Variable    ${PROD}    TPAYX
    CheckUSER    ${obuID}

LOGINX
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

PRENOTAZIONE3PARTX
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    merchantOrderId=1643044375064
    Set to Dictionary    ${checkin_json}    amount    ${amount}
    ${output}    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    prenotazioneSkipass    ${output.json()}
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='10'    1
    Disconnect from Database

MODIFICAPRENOTAZIONE3PARTX
    ${modifica_json}    evaluate    json.loads('''${body_modificaSkipass}''')    json
    ${output}    PatchRequest3part    ${endpoint}    ${basepath_modificaSkipass}${transactionID}/amount    ${modifica_json}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    prenotazioneSkipass    ${output.json()}
    #CONFERMA3PART
    #    ${output}    PostRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}/authorize    ${outputUpdatebody}    ${access_token}    ${obuID}    ${PROD}
    #    SchemaValidator    conferma    ${output.json()}
    #    dbConnection    CPDB32
    #    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    #    Disconnect from Database

CONFERMAMULTI3PARTX
    ${confermaMulti_json}    evaluate    json.loads('''${body_confermaMultiSkipass}''')    json
    ${OPTIONAL}    Set Variable    NOPAYTYPE
    ${output}    PostRequest3part    ${endpoint}    ${basepath_confermaMultiSkipass}${transactionID}/confirm    ${confermaMulti_json}    ${access_token}    ${obuID}    ${PROD}    ${OPTIONAL}
    SchemaValidator    confermaMultiSkipass    ${output.json()}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX_PRI='${transactionID}' AND F_STA_TRX='30'    2
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX_PRI='${transactionID}' AND F_STA_TRX='91'    1
    Disconnect from Database
