*** Settings ***
Resource          ../DATA/BasePathMulesoftRT.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/Body.txt
Resource          schemaJson.txt
Resource          ../KEYWORDS/checkDB.robot

*** Variables ***
${access_token}    ${EMPTY}
${transactionID}    ${EMPTY}
${update_response}    ${EMPTY}
@{dettagli}
${outputUpdatebody}    ${EMPTY}
${C_SER}          23
${DAUT_str}       ${EMPTY}
${amount}         ${222}

*** Test Cases ***
CHECKUSER
    Set Suite Variable    ${obuID}    1000125623
    Set Suite Variable    ${PROD}    TPAY
    CheckUSER    ${obuID}

LOGIN
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}
    SchemaValidator    token    ${output.json()}

PRENOTAZIONE3PART
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Corso Italia n333,20123 Milano MI,Italia
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    amount    ${amount}
    ${output}    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    ###    SchemaValidator    prenotazione    ${output.json()}
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    #########
    ${output}    Set to Dictionary    ${output.json()}    terminalId=12455    merchantOrderId=AAAA-BBBB.
    ${outputUpdate}    PutRequest3part    ${endpoint}    ${basepath_aggiornamento3part}${transactionID}    ${output}    ${access_token}    ${obuID}    ${PROD}
    ###    SchemaValidator    aggiornamento    ${outputUpdate.json()}
    ${details}    CreateDictionaryList    ${C_SER}
    ${outputUpdatebody}    Set to Dictionary    ${outputUpdate.json()}    details=${details}
    Set Suite Variable    ${outputUpdatebody}    ${outputUpdatebody}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='10' and C_SUP='${obuID}'    1
    Disconnect from Database

CONFERMA-DELETE3PART
    ${output}    PostRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}/authorize    ${outputUpdatebody}    ${access_token}    ${obuID}    ${PROD}
    ###    SchemaValidator    conferma    ${output.json()}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30' and C_SUP='${obuID}'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    conferma
    ${outputDel}    DeleteRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}    ${output.json()}    ${access_token}    ${obuID}    ${PROD}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97'    1
    Disconnect from Database
    ControlloVKBB01    storno    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    storno

CHECKUSERX
    Set Suite Variable    ${obuID}    1000116366
    Set Suite Variable    ${PROD}    TPAYX
    CheckUSER    ${obuID}

LOGINX
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${C_SER}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}
    SchemaValidator    token    ${output.json()}

PRENOTAZIONE3PARTX
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${checkin_json}    evaluate    json.loads('''${body_prenotazione3part}''')    json
    ${C_PUN_ERO_SER}    GetPOSCode    ${C_SER}
    &{POS}    Create Dictionary    code=${C_PUN_ERO_SER}    addressOverride=Corso Italia n333,20123 Milano MI,Italia
    Set to Dictionary    ${checkin_json}    pointOfService    ${POS}
    Set to Dictionary    ${checkin_json}    serviceCode    ${C_SER}
    Set to Dictionary    ${checkin_json}    amount    ${amount}
    ${output}    PostRequest3part    ${endpoint}    ${basepath_prenotazione3part}    ${checkin_json}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    prenotazione    ${output.json()}
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    #########
    ${output}    Set to Dictionary    ${output.json()}    terminalId=12455    merchantOrderId=AAAA-BBBB.
    ${outputUpdate}    PutRequest3part    ${endpoint}    ${basepath_aggiornamento3part}${transactionID}    ${output}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    aggiornamento    ${outputUpdate.json()}
    ${details}    CreateDictionaryList    ${C_SER}
    ${outputUpdatebody}    Set to Dictionary    ${outputUpdate.json()}    details=${details}
    Set Suite Variable    ${outputUpdatebody}    ${outputUpdatebody}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='10' and C_SUP='${obuID}'    1
    Disconnect from Database

CONFERMA-DELETE3PARTX
    ${output}    PostRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}/authorize    ${outputUpdatebody}    ${access_token}    ${obuID}    ${PROD}
    SchemaValidator    conferma    ${output.json()}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30' and C_SUP='${obuID}'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    conferma
    ${outputDel}    DeleteRequest3part    ${endpoint}    ${basepath_conferma3part}${transactionID}    ${output.json()}    ${access_token}    ${obuID}    ${PROD}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97'    1
    Disconnect from Database
    ControlloVKBB01    storno    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    storno
