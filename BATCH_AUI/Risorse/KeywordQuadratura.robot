*** Keywords ***
Parametri batch quadratura
    [Arguments]  ${nomebatch}
    ${query} =  Catenate  SEPARATOR=  SELECT VAL_DE FROM KCAA.VKCA02_PARM_GEN vpg WHERE C_OGG = '  ${nomebatch}  ' AND N_PRG_OGG = 3;
    ${prog}  QUERY  ${query}
    [return]  ${prog}[0][0]

Trova file quadratura
    [Arguments]  ${proginiziale}  ${mod}
    ${nomefile}  ${listagiorni}  AUIutils.trovafilequadratura  ${proginiziale}  ${mod}
    [return]  ${nomefile}  ${listagiorni}

Scaricamento file quadratura
    [Arguments]  ${progressivo}  ${mod}
    ${file}  ${listagiorni}  Trova file quadratura  ${progressivo}  ${mod}
    Log to console  \n
    Log to console   scaricamento file ${file}
    DownloadFileAUI.fileAUIdownload  ${file}
    ${tottrx}  ${esito}  AUIutils.controllorighe  C:/Users/Modis/PycharmProjects/KCABatch/File/${file}  Q
    ${qquad}  querygen.qcontaquadratura  ${listagiorni}[0]  ${listagiorni}[-1]  ${mod}
    ${trxdb}  QUERY  ${qquad}
    Log to console  Numero totale transazioni recuperate: ${trxdb}[0][0]
    IF  ${tottrx} != ${trxdb}[0][0]
             Log to Console  Errore numero righe del file ${file}: righe file (${tottrx}) diverso da numero di transazioni in db (${trxdb}[0][0])
             ${esito}  SET VARIABLE  2
    END
    ${costanti}  verificaquadratura.costantiquad
    ${esitocontrolli}  verificaquadratura.controlloquadratura  C:/Users/Modis/PycharmProjects/KCABatch/File/${file}  ${mod}  ${listagiorni}[0]  ${listagiorni}[-1]  ${trxdb}[0][0]  ${costanti}
    IF  ${esitocontrolli} == ${FALSE}
               ${esito}  SET VARIABLE  3
    END
    SET TEST VARIABLE  ${esito}


