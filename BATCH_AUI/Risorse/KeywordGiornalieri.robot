*** Keywords ***
Parametri batch giornaliero
    [Arguments]  ${nomebatch}
    ${query1} =  Catenate  SEPARATOR=  SELECT VAL_TS FROM KCAA.VKCA02_PARM_GEN vpg WHERE C_OGG = '  ${nomebatch}  ' AND N_PRG_OGG = 2;
    ${query2} =  Catenate  SEPARATOR=  SELECT VAL_DE FROM KCAA.VKCA02_PARM_GEN vpg WHERE C_OGG = '  ${nomebatch}  ' AND N_PRG_OGG = 3;
    ${TS}  QUERY  ${query1}
    ${prog}  QUERY  ${query2}
    [return]  ${TS}[0][0]  ${prog}[0][0]

Trova file giornalieri
    [Arguments]     ${datapartenza}  ${proginiziale}  ${mod}
    ${listafile}  ${listagiorni}  AUIutils.trovafilegiornaliero  ${datapartenza}  ${proginiziale}  ${mod}
    [return]  ${listafile}

Scaricamento file giornalieri
    [Arguments]     ${TSinizio}  ${progressivo}  ${mod}
    ${lista}  Trova file giornalieri  ${TSinizio}  ${progressivo}  ${mod}
    ${numfile}  GET LENGTH  ${lista}
    ${esito}  SET VARIABLE  0
    Log to console  \n
    FOR    ${i}    IN RANGE   ${numfile}
           ${TS2}  ADD TIME TO DATE  ${TSinizio}  ${i+1} day
           ${TS3}  CONVERT DATE  ${TS2}  result_format=%Y-%m-%d
#           ${qday}  Conta trx giornalieri  ${TS3}  ${mod}
           ${qday}  querygen.qcontagiornaliero  ${TS3}  ${mod}
           ${trxdb}  QUERY  ${qday}
           ${file_i}  Set Variable  ${lista}[${i}]
           Log to console   scaricamento file ${file_i} per il giorno ${TS3}
           DownloadFileAUI.fileAUIdownload  ${file_i}
           ${tottrx}  ${flag}  AUIutils.controllorighe  C:/Users/Modis/PycharmProjects/KCABatch/File/${file_i}   G
           Log to console  Numero totale transazioni recuperate: ${trxdb}[0][0]
           IF  ${flag}
               Log to Console  Errore nella lunghezza delle righe file ${file_i}
               ${esito}  SET VARIABLE  ${flag}
           END
           IF  ${tottrx} != ${trxdb}[0][0]
               Log to Console  Errore numero righe del file ${file_i}: righe file (${tottrx}) diverso da numero di transazioni in db (${trxdb}[0][0])
               ${esito}  SET VARIABLE  2
           END
           ${fileind} =  Catenate  SEPARATOR=  File\\  ${file_i}
           ${costanti}  verificagiornaliero.costanti
           ${esitocontrolli}  verificagiornaliero.controllogiornaliero  ${fileind}  ${mod}  ${TS3}  ${trxdb}[0][0]  ${costanti}
           Log to console  ${esitocontrolli}
           IF  ${esitocontrolli} == ${FALSE}
               ${esito}  SET VARIABLE  3
           END
           Log to console  ${esito}
    END
    SET TEST VARIABLE  ${esito}


