from paramiko import SSHClient, AutoAddPolicy


def batchrun(x, y):
    client = SSHClient()
    client.set_missing_host_key_policy(AutoAddPolicy())
    client.load_system_host_keys()
    client.connect("10.153.232.8", port=22, username="usr_87002789_gruppo_autostrade_i",
                   password=None, key_filename="C:/Users/Modis/PycharmProjects/.ssh/id_rsa",
                   pkey=None, timeout=None, allow_agent=True,
                   look_for_keys=True, compress=False, sock=None, gss_auth=False, gss_kex=False, gss_deleg_creds=True,
                   gss_host=None,
                   banner_timeout=None, auth_timeout=None, gss_trust_dns=True, passphrase=None,
                   disabled_algorithms=None)
    cmdline = '../../prd/k/'+x.lower()+'/script/job/./' + y
    stdin, stdout, stderr = client.exec_command(cmdline, get_pty=True)
    res=[]
    for line in iter(stdout.readline, ""):
        print(line, end="")
        res.append(line)
    client.close()
    return(res[-1].strip())



##batchrun('kddc1j01')

