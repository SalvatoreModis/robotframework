import pandas as pd
import sys

sys.path.insert(1, r'C:\Users\Modis\PycharmProjects\KCAbatch\KCAutils')
import controlliStatici
import databasconnect
import datetime
import ibm_db
import querygen
import numpy as np


def costantiquad():
    lungcampi = [11, 2, 6, 1, 25, 10, 8, 25, 1, 25, 70, 5, 16, 3, 3, 3, 8, 1, 16, 2, 20, 3, 1, 15, 15]

    nomicampi = ['a01bCodiceIntermediario', 'a01aTipoIntermediario', 'a11Filiale', 'a02TipoInformazione',
                 'rifOperazione', 'codSezionale', 'A21DataOperazione', 'mastroContabile', 'a42TipoRapporto',
                 'a41Rapporto', 'd11_ragione_sociale', 'Product', 'c11Ndg', 'd21Sae', 'd22Rae', 'd23SettSint',
                 'a21DataOperazioneChiusura', 'a43TipoLegame', 'c11NdgSecondario', 'a52TipoRegistrazione',
                 'causaleOperativa','b12Divisa', 'b13Segno', 'b14ImportoTotale', 'b15ImportoContante']

    query_cfis = "SELECT VAL_DA , VAL_CH FROM KCAA.VKCA02_PARM_GEN vpg WHERE C_OGG IN ('C_FIS_TLP','C_FIS_TPAY');"
    conn28 = databasconnect.connessionedb('cpdb28')
    T1 = databasconnect.queryexec(query_cfis, conn28, n=2)
    T1b = T1[T1['VAL_DA'] <= datetime.date.today()]
    cfistlp = T1b.VAL_CH[T1b.VAL_DA == T1b.VAL_DA.max()].values[0].strip()
    ibm_db.close(conn28)
    valori = {"lungcampi": lungcampi, "nomicampi": nomicampi, "cfistlp": cfistlp}
    return valori


def controlloquadratura(filein, mod, datain, datafin, ntrx, cost):
    if ntrx == 0:
        esito = True
    else:
        lungcampi = cost["lungcampi"]
        nomicampi = cost["nomicampi"]
        cfistlp = cost["cfistlp"]
        file = pd.read_fwf(filein, widths=lungcampi, names=nomicampi, dtype=str, delimiter="\n\t")
        checklist = controlliStatici.valorifissiquadratura(file, mod)
        checklist['a01bCodiceIntermediario'] = all(file.a01bCodiceIntermediario == cfistlp)
        conn28 = databasconnect.connessionedb('cpdb28')
        querytrx = querygen.qtrxquadratura(datain, datafin, mod)
        T = databasconnect.queryexec(query=querytrx, conn=conn28, n=ntrx)
        tabella = pd.DataFrame(T.values)
        tabella.columns = ['D_CON', 'D_MOV_ENT', 'O_MOV_ENT', 'D_PAG', 'O_PAG', 'C_CON',
                           'C_PUN_ERO_SER', 'C_IDE_TRX', 'C_CTR', 'C_SUP', 'C_SER', 'I_TOT', 'C_TIP_TIT']
        checkrow = {'A21DataOperazione': [], 'b13Segno': [], 'b14ImportoTotale': [], 'c11Ndg': [], 'a41Rapporto': []}
        if mod == 'BB':
            for i in range(ntrx):
                codcli = c11ndgbb(tabella['C_SUP'][i], conn28)
                checkrow['c11Ndg'].append(codcli == file.c11Ndg[i].strip())
        ibm_db.close(conn28)
        tabella['I_TOT'] = tabella['I_TOT'].str.replace(",", ".").apply(lambda x: float(x))
        itotstr = (abs(tabella['I_TOT']) * 100).apply(lambda x: int(round(x))).apply(lambda x: str(x)).apply(
            lambda x: x.zfill(15))
        tspag = tabella['D_PAG'].apply(lambda x: str(x)) + '-' + tabella['O_PAG'].apply(lambda x: str(x)).str.replace(
            ":",
            ".") + '.000000'
        checkrow['A21DataOperazione'] = tabella['D_PAG'].apply(lambda x: x.strftime("%Y%m%d")) == file.A21DataOperazione
        checkrow['b13Segno'] = file.b13Segno == np.where(tabella['I_TOT'] >= 0, 'D', 'A')
        checkrow['b14ImportoTotale'] = file.b14ImportoTotale == itotstr
        if mod == 'BB':
            checkrow['a41Rapporto'] = ('C' + tabella['C_CTR']).apply(lambda x: x.zfill(25)) == file.a41Rapporto
        conn30 = databasconnect.connessionedb('cpdb30')
        if mod != 'BB':
            for i in range(ntrx):
                ctr = tabella['C_CTR'][i]
                cou = coldute(ctr, conn30)
                cou = ('C' + cou).zfill(25)
                checkrow['a41Rapporto'].append(cou == file.a41Rapporto[i])
                int1 = intestatario1(ctr, conn30, mod)
                checkrow['c11Ndg'].append(int1 == file.c11Ndg[i])
        ibm_db.close(conn30)
        checkrowfinal = {k: all(v) for k, v in checkrow.items()} and {k: len(v) > 0 for k, v in checkrow.items()}
        checklistfinal = checklist | checkrowfinal
        {k: v for k, v in checklistfinal.items() if v is not True}
        esito = all(value == True for value in checklistfinal.values())
        print(checklistfinal)
    return esito


# xxx=controllogiornaliero('File/BTPRD.ETS.AUIBB.OPE.N01416', 'BB', costs)
# filein = 'FILE/BTPRD.ETS.QRP2C.OPE.N00296'
# mod = 'CON'
# cost = costantiquad()
# datain = '2022-03-01'
# datafin = '2022-03-16'
# ntrx = 536


def coldute(ctr, conn):
    query = 'SELECT C_OLD_UTE FROM KCAA.VKCA27_CTR vc WHERE C_CTR =' + ctr + ';'
    cod = databasconnect.queryexec(query, conn)
    return cod['C_OLD_UTE'][0]


def intestatario1(ctr, conn, mod):
    ctpcli = databasconnect.queryexec(querycli(ctr), conn)['C_CTP_CLI'][0]
    intest1 = intestatario1base(str(ctpcli), conn, mod)
    if intest1 is None:
        z = alttetsav(ctr, conn)
        for cli in z['C_CTP_CLI']:
            #            queryc = 'SELECT C_FIS FROM KCAA.VKCA15_CLI WHERE C_CTP_CLI =' + str(cli) + ';'
            clien = databasconnect.queryexec(querycfis(cli), conn)
            cli2 = clien.values[0][0]
            if cli2 is not None:
                intest1 = intestatario1base(cli2, conn, mod)
    return intest1


def intestatario1base(ctpcli, conn, mod):
    if mod == 'CON':
        intest1 = databasconnect.queryexec(querycfis(ctpcli), conn)
    elif mod == 'BUS':
        query = 'SELECT COALESCE(CAST(C_PAR_IVA AS VARCHAR), C_FIS ) FROM KCAA.VKCA15_CLI vc2 WHERE C_CTP_CLI =' + ctpcli + ';'
        intest1 = databasconnect.queryexec(query, conn)
    return intest1.values[0][0]


def c11ndgbb(supp, conn):
    query = 'SELECT C_INT FROM KCAA.VKCA01_BB_CLI WHERE C_SUP_COD = \'' + supp \
             + '\' AND CURRENT_TIMESTAMP BETWEEN D_INI_VAL AND D_FIN_VAL;'
    codcli = databasconnect.queryexec(query, conn)
    return codcli.values[0][0]


def querytit(supp, tiptit):
    if tiptit == 'AP':
        query = 'SELECT C_TIT FROM KCAA.VKCAO9_APP WHERE C_APP = \'' + supp + '\';'
    else:
        query = 'SELECT C_TIT FROM KCAA.VKCAI7_APP_VIR WHERE C_OBU_ID = \'' + supp + '\';'
    return query


def querycliutipin(titolo, ctr):
    query = 'SELECT C_CTP_UTI FROM KCAA.VKCA01_CLI_UTI_PYN WHERE C_CTR=\'' + ctr + '\' AND C_TIT= \'' + \
            titolo + '\'' + \
            ' AND CURRENT TIMESTAMP BETWEEN D_INI_VAL AND D_FIN_VAL;'
    return query


def querycfis(ctpcli):
    query = 'SELECT C_FIS FROM KCAA.VKCA15_CLI vc2 WHERE C_CTP_CLI =' + ctpcli + ';'
    return query


def queryriccli(ctpcli):
    query = 'SELECT C_RIC_CLI FROM KCAA.VKCAAV_COD_TLP_IT WHERE C_CTP_CLI=' + ctpcli + ';'
    return query


def querycli(ctr):
    query = 'SELECT C_CTP_CLI FROM KCAA.VKCA27_CTR vc WHERE C_CTR = ' + str(ctr) + ';'
    return query


def alttetsav(ctr, conn):
    x = databasconnect.queryexec(querycli(ctr), conn)
    y = databasconnect.queryexec(queryriccli(x.values[0][0]), conn)
    queryz = 'SELECT C_CTP_CLI FROM KCAA.VKCAAV_COD_TLP_IT WHERE C_RIC_CLI =\'' + str(
        y.values[0][0]) + '\' AND C_CTP_CLI!=' + str(x.values[0][0]) + ';'
    queryn = 'SELECT COUNT(*) FROM KCAA.VKCAAV_COD_TLP_IT WHERE C_RIC_CLI = \'' + str(
        y.values[0][0]) + '\' AND C_CTP_CLI!=' + str(x.values[0][0]) + ';'
    nz = databasconnect.queryexec(queryn, conn)
    z = databasconnect.queryexec(queryz, conn, n=nz.values[0][0])
    return z
