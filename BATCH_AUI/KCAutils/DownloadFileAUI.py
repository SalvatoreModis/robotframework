from paramiko import SSHClient, AutoAddPolicy
from scp import SCPClient


def fileAUIdownload(x):
    client = SSHClient()
    client.set_missing_host_key_policy(AutoAddPolicy())
    client.load_system_host_keys()
    client.connect("10.153.232.8",
                   port=22,
                   username="usr_048114581_gruppo_autostrade_",
                   password=None,
                   key_filename="C:/Users/Modis/PycharmProjects/.ssh/id_rsa",
                   pkey=None, timeout=None, allow_agent=True,
                   look_for_keys=True, compress=False, sock=None, gss_auth=False, gss_kex=False, gss_deleg_creds=True,
                   gss_host=None,
                   banner_timeout=None, auth_timeout=None, gss_trust_dns=True, passphrase=None,
                   disabled_algorithms=None)
    scp = SCPClient(client.get_transport())
    indfile = '../../prd/k/kca/dat/'+x
    print(indfile)
    scp.get(indfile, 'C:/Users/Modis/PycharmProjects/KCABatch/File')
    scp.close()
    client.close()


