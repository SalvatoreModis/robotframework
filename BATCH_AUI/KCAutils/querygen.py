def qcontagiornaliero(data, mod):
    querypt1 = 'SELECT COUNT(*) FROM KCAA.VKCA05_TRX A INNER JOIN KCAA.VKCA07_COD_STA_TRX CT ' \
               'ON A.F_STA_TRX=CT.C_STA_TRX LEFT JOIN KCAA.VKCA02_CON C ON A.C_CON=C.C_CON ' \
               'AND C.C_FIS NOT IN (\'09771701001\',\'14070851002\') ' \
               'INNER JOIN KCAA.VKCA17_SER_AMM S ON A.C_SER=S.C_SER LEFT JOIN KCAA.VKCA22_TIP_TRX TT ' \
               'ON A.C_SER=TT.C_SER AND A.C_TIP_TRX=TT.C_TIP_TRX WHERE DATE(A.D_CON) = \''
    querypt2 = '\' AND S.C_SOC=53 AND CT.T_DES_KCB IS NOT NULL AND C_PRO IN ' \
               '(SELECT VAL_CH FROM KCAA.VKCA02_PARM_GEN vpg2 WHERE C_OGG = \'C_PRO_'
    querypt2bb = '\' AND S.C_SOC=53 AND CT.T_DES_KCB IS NOT NULL AND C_PRO LIKE (\'B%'
    querypt3 = '\') AND (TT.C_TIP_TRX IS NULL OR (TT.C_TIP_TRX IS NOT NULL AND TT.C_TIP_CNT IN (\'CM\',\'CO\')));'
    datacon = str(data)
    if mod == 'BB':
        query = querypt1 + datacon + querypt2bb + querypt3
    else:
        query = querypt1 + datacon + querypt2 + mod + querypt3
    return query


def qtrxgiornaliero(data, mod):
    querypt1 = 'SELECT A.D_CON, A.D_MOV_ENT,A.O_MOV_ENT,A.D_PAG, A.O_PAG, A.C_CON,' \
               'A.C_PUN_ERO_SER, A.C_IDE_TRX, A.C_CTR, A.C_SUP, A.C_SER, A.I_TOT, A.C_TIP_TIT ' \
               'FROM KCAA.VKCA05_TRX A INNER JOIN KCAA.VKCA07_COD_STA_TRX CT ' \
               'ON A.F_STA_TRX=CT.C_STA_TRX LEFT JOIN KCAA.VKCA02_CON C ON A.C_CON=C.C_CON ' \
               'AND C.C_FIS NOT IN (\'09771701001\',\'14070851002\') ' \
               'INNER JOIN KCAA.VKCA17_SER_AMM S ON A.C_SER=S.C_SER LEFT JOIN KCAA.VKCA22_TIP_TRX TT ' \
               'ON A.C_SER=TT.C_SER AND A.C_TIP_TRX=TT.C_TIP_TRX WHERE DATE(A.D_CON) = \''
    querypt2 = '\' AND S.C_SOC=53 AND CT.T_DES_KCB IS NOT NULL AND C_PRO IN ' \
               '(SELECT VAL_CH FROM KCAA.VKCA02_PARM_GEN vpg2 WHERE C_OGG = \'C_PRO_'
    querypt2bb = '\' AND S.C_SOC=53 AND CT.T_DES_KCB IS NOT NULL AND C_PRO LIKE (\'B%'
    querypt3 = '\') AND (TT.C_TIP_TRX IS NULL OR (TT.C_TIP_TRX IS NOT NULL AND TT.C_TIP_CNT IN (\'CM\',\'CO\')))' \
               'ORDER BY D_CON ,D_MOV_ENT,O_MOV_ENT;'
    datacon = str(data)
    if mod == 'BB':
        query = querypt1 + datacon + querypt2bb + querypt3
    else:
        query = querypt1 + datacon + querypt2 + mod + querypt3
    return query


def qcontaquadratura(datain, datafin, mod):
    querypt1 = 'SELECT COUNT(*) FROM KCAA.VKCA05_TRX A INNER JOIN KCAA.VKCA07_COD_STA_TRX CT ' \
               'ON A.F_STA_TRX=CT.C_STA_TRX LEFT JOIN KCAA.VKCA02_CON C ON A.C_CON=C.C_CON ' \
               'AND C.C_FIS NOT IN (\'09771701001\',\'14070851002\') ' \
               'INNER JOIN KCAA.VKCA17_SER_AMM S ON A.C_SER=S.C_SER LEFT JOIN KCAA.VKCA22_TIP_TRX TT ' \
               'ON A.C_SER=TT.C_SER AND A.C_TIP_TRX=TT.C_TIP_TRX WHERE DATE(A.D_CON) BETWEEN \''
    querypt2 = '\' AND S.C_SOC=53 AND CT.T_DES_KCB IS NOT NULL AND C_PRO IN ' \
               '(SELECT VAL_CH FROM KCAA.VKCA02_PARM_GEN vpg2 WHERE C_OGG = \'C_PRO_'
    querypt2bb = '\' AND S.C_SOC=53 AND CT.T_DES_KCB IS NOT NULL AND C_PRO LIKE (\'B%'
    querypt3 = '\') AND (TT.C_TIP_TRX IS NULL OR (TT.C_TIP_TRX IS NOT NULL AND TT.C_TIP_CNT IN (\'CM\',\'CO\')));'
    dataincon = str(datain)
    datafincon = str(datafin)
    if mod == "BB":
        query = querypt1 + dataincon + '\' AND \'' + datafincon + querypt2bb + querypt3
    else:
        query = querypt1 + dataincon + '\' AND \'' + datafincon + querypt2 + mod + querypt3
    return query


def qtrxquadratura(datain, datafin, mod):
    querypt1 = 'SELECT A.D_CON, A.D_MOV_ENT,A.O_MOV_ENT,A.D_PAG, A.O_PAG, A.C_CON,' \
               'A.C_PUN_ERO_SER, A.C_IDE_TRX, A.C_CTR, A.C_SUP, A.C_SER, A.I_TOT, A.C_TIP_TIT' \
               ' FROM KCAA.VKCA05_TRX A INNER JOIN KCAA.VKCA07_COD_STA_TRX CT ' \
               'ON A.F_STA_TRX=CT.C_STA_TRX LEFT JOIN KCAA.VKCA02_CON C ON A.C_CON=C.C_CON ' \
               'AND C.C_FIS NOT IN (\'09771701001\',\'14070851002\') ' \
               'INNER JOIN KCAA.VKCA17_SER_AMM S ON A.C_SER=S.C_SER LEFT JOIN KCAA.VKCA22_TIP_TRX TT ' \
               'ON A.C_SER=TT.C_SER AND A.C_TIP_TRX=TT.C_TIP_TRX WHERE DATE(A.D_CON) BETWEEN \''
    querypt2 = '\' AND S.C_SOC=53 AND CT.T_DES_KCB IS NOT NULL AND C_PRO IN ' \
               '(SELECT VAL_CH FROM KCAA.VKCA02_PARM_GEN vpg2 WHERE C_OGG = \'C_PRO_'
    querypt2bb = '\' AND S.C_SOC=53 AND CT.T_DES_KCB IS NOT NULL AND C_PRO LIKE (\'B%'
    querypt3 = '\') AND (TT.C_TIP_TRX IS NULL OR (TT.C_TIP_TRX IS NOT NULL AND TT.C_TIP_CNT IN (\'CM\',\'CO\')))' \
               'ORDER BY D_CON ,D_MOV_ENT,O_MOV_ENT;'
    dataincon = str(datain)
    datafincon = str(datafin)
    if mod == "BB":
        query = querypt1 + dataincon + '\' AND \'' + datafincon + querypt2bb + querypt3
    else:
        query = querypt1 + dataincon + '\' AND \'' + datafincon + querypt2 + mod + querypt3
    return query
