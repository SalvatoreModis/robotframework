import pandas as pd
import sys

sys.path.insert(1, r'C:\Users\Modis\PycharmProjects\KCAbatch\KCAutils')
import controlliStatici
import databasconnect
import datetime
import ibm_db
import querygen
import numpy as np


def costanti():
    lungcampi = [2, 11, 1, 8, 20, 6, 6, 30, 2, 1, 4, 5, 2, 11, 3, 6, 30, 2, 50, 25, 1, 1, 2, 12, 3, 1, 15, 15, 16, 16,
                 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 70, 3, 6, 30, 2, 35, 5, 30, 69, 20, 5, 5, 5, 2, 26, 4, 3,
                 10, 30, 156, 1]

    nomicampi = ['a01aTipoIntermediario', 'a01bCodiceIntermediario', 'a02TipoInformazione', 'A21DataOperazione',
                 'IdOperazione', 'a11Filiale', 'a12aCabComune', 'a12bComune', 'a12cProvincia', 'a22FlagFrazionata',
                 'a24CausaleAnalitica', 'CausaleOperativa', 'a31aTipoIntermediariCtp', 'a31bCodiceIntermediarioCtp',
                 'a32PaeseCtp', 'a33aCabComuneCtp', 'a33bComuneCtp', 'a33cProvinciaCtp', 'a34IntermediarioCtp',
                 'a41Rapporto', 'a42TipoRapporto', 'a43TipoLegame', 'a52TipoRegistrazione', 'a53Connessione',
                 'b12Divisa', 'b13Segno', 'b14Importo', 'b15Contanti', 'Intestatario1', 'Intestatario2',
                 'Intestatario3', 'Intestatario4', 'Esecutore1', 'Esecutore2', 'Esecutore3', 'Esecutore4', 'Titolare1',
                 'Titolare2', 'Titolare3', 'Titolare4', 'OrdinanteBeneficiario', 'F11', 'F13', 'F14A', 'F14B', 'F14C',
                 'F15', 'F16', 'F31', 'z15', 'IDMovimento', 'TipoMovimento', 'ModalitaPagamento', 'MotivoChiusura',
                 'tipoRapportoAdE', 'timeStampTransazione', 'categoriaMercante', 'paeseMercante',
                 'codiceZipPaeseMercante', 'riferimentoOperazione', 'Filler', 'Punto']
    query_cfis = "SELECT VAL_DA , VAL_CH FROM KCAA.VKCA02_PARM_GEN vpg WHERE C_OGG IN ('C_FIS_TLP','C_FIS_TPAY');"
    conn28 = databasconnect.connessionedb('cpdb28')
    T1 = databasconnect.queryexec(query_cfis, conn28, n=2)
    T1b = T1[T1['VAL_DA'] <= datetime.date.today()]
    cfistlp = T1b.VAL_CH[T1b.VAL_DA == T1b.VAL_DA.max()].values[0].strip()
    ibm_db.close(conn28)
    valori = {"lungcampi": lungcampi, "nomicampi": nomicampi, "cfistlp": cfistlp}
    return valori


def controllogiornaliero(filein, mod, data, ntrx, cost):
    if ntrx == 0:
        esito = True
    else:
        lungcampi = cost["lungcampi"]
        nomicampi = cost["nomicampi"]
        cfistlp = cost["cfistlp"]
        file = pd.read_fwf(filein, widths=lungcampi, names=nomicampi, dtype=str, delimiter="\n\t")
        checklist = controlliStatici.valorifissigiornaliero(file, mod)
        checklist['a01bCodiceIntermediario'] = all(file.a01bCodiceIntermediario == cfistlp)
        conn28 = databasconnect.connessionedb('cpdb28')
        querytrx = querygen.qtrxgiornaliero(data, mod)
        T = databasconnect.queryexec(query=querytrx, conn=conn28, n=ntrx)
        tabella = pd.DataFrame(T.values)
        tabella.columns = ['D_CON', 'D_MOV_ENT', 'O_MOV_ENT', 'D_PAG', 'O_PAG', 'C_CON',
                           'C_PUN_ERO_SER', 'C_IDE_TRX', 'C_CTR', 'C_SUP', 'C_SER', 'I_TOT', 'C_TIP_TIT']
        checkrow = {'A21DataOperazione': [], 'IdOperazione': [], 'riferimentoOperazione': [], 'a24CausaleAnalitica': [],
                    'b13Segno': [], 'b14Importo': [], 'timeStampTransazione': [], 'a41Rapporto': [],
                    'Intestatario1': [], 'Esecutore1': []}
        if mod == 'BB':
            for i in range(ntrx):
                int1, ese1 = intesebb(tabella['C_SUP'][i], conn28)
                checkrow['Intestatario1'].append(int1 == file.Intestatario1[i].strip())
                checkrow['Esecutore1'].append(ese1 == file.Esecutore1[i].strip())
        ibm_db.close(conn28)
        tabella['I_TOT'] = tabella['I_TOT'].str.replace(",", ".").apply(lambda x: float(x))
        itotstr = (abs(tabella['I_TOT']) * 100).apply(lambda x: int(round(x))).apply(lambda x: str(x)).apply(
            lambda x: x.zfill(15))
        tspag = tabella['D_PAG'].apply(lambda x: str(x)) + '-' + tabella['O_PAG'].apply(lambda x: str(x)).str.replace(
            ":",
            ".") + '.000000'
        checkrow['A21DataOperazione'] = tabella['D_PAG'].apply(lambda x: x.strftime("%Y%m%d")) == file.A21DataOperazione
        checkrow['timeStampTransazione'] = tspag == file.timeStampTransazione
        checkrow['IdOperazione'] = tabella['C_IDE_TRX'].apply(lambda x: x[:20]) == file.IdOperazione
        checkrow['riferimentoOperazione'] = tabella['C_IDE_TRX'].apply(lambda x: x[20:]).apply(
            lambda x: x.ljust(30)) == file.riferimentoOperazione
        vecljust = np.vectorize(lambda x, n: x.ljust(n))
        causan = vecljust(np.where(tabella['I_TOT'] >= 0, 'I6', 'I5'), 4)
        checkrow['a24CausaleAnalitica'] = file.a24CausaleAnalitica == causan
        checkrow['b13Segno'] = file.b13Segno == np.where(tabella['I_TOT'] >= 0, 'D', 'A')
        checkrow['b14Importo'] = file.b14Importo == itotstr
        if mod == 'BB':
            checkrow['a41Rapporto'] = ('C' + tabella['C_CTR']).apply(lambda x: x.zfill(25)) == file.a41Rapporto
        conn30 = databasconnect.connessionedb('cpdb30')
        if mod != 'BB':
            for i in range(ntrx):
                ctr = tabella['C_CTR'][i]
                cou = coldute(ctr, conn30)
                cou = ('C' + cou).zfill(25)
                checkrow['a41Rapporto'].append(cou == file.a41Rapporto[i])
                int1 = intestatario1(ctr, conn30, mod)
                checkrow['Intestatario1'].append(int1 == file.Intestatario1[i])
                if mod == 'CON':
                    checkrow['Esecutore1'].append(''.rjust(16)==file.Esecutore1[i])
                elif mod == 'BUS':
                    supp = tabella['C_SUP'][i]
                    tiptit = tabella['C_TIP_TIT'][i]
                    es1 = esecutore1(ctr,supp, tiptit, conn30)
                    checkrow['Esecutore1'].append(es1 == file.Esecutore1[i])
        ibm_db.close(conn30)
        checkrowfinal = {k: all(v) for k, v in checkrow.items()} and {k: len(v) > 0 for k, v in checkrow.items()}
        checklistfinal = checklist | checkrowfinal
        {k: v for k, v in checklistfinal.items() if v is not True}
        esito = all(value == True for value in checklistfinal.values())
        print(checklistfinal)
    return esito


# xxx=controllogiornaliero('File/BTPRD.ETS.AUIBB.OPE.N01416', 'BB', costs)
# filein = 'File/BTPRD.ETS.AUITP.OPE.N01290'
# mod = 'CON'
# costanti = costanti()
# data = '2022-03-03'
# ntrx = 113

def coldute(CTR, conn):
    query = 'SELECT C_OLD_UTE FROM KCAA.VKCA27_CTR vc WHERE C_CTR =' + CTR + ';'
    cod = databasconnect.queryexec(query, conn)
    return cod['C_OLD_UTE'][0]


def intestatario1(ctr, conn, mod):
    ctpcli = databasconnect.queryexec(querycli(ctr), conn)['C_CTP_CLI'][0]
    intest1 = intestatario1base(str(ctpcli), conn, mod)
    if intest1 is None:
        z = alttetsav(ctr, conn)
        # x = databasconnect.queryexec(querycli(ctr), conn)
        # y = databasconnect.queryexec(queryriccli(x.values[0][0]), conn)
        # queryz = 'SELECT C_CTP_CLI FROM KCAA.VKCAAV_COD_TLP_IT WHERE C_RIC_CLI =\'' + str(
        #     y.values[0][0]) + '\' AND C_CTP_CLI!=' + str(x.values[0][0]) + ';'
        # queryn = 'SELECT COUNT(*) FROM KCAA.VKCAAV_COD_TLP_IT WHERE C_RIC_CLI = \'' + str(
        #     y.values[0][0]) + '\' AND C_CTP_CLI!=' + str(x.values[0][0]) + ';'
        # nz = databasconnect.queryexec(queryn, conn)
        # z = databasconnect.queryexec(queryz, conn, n=nz.values[0][0])
        for cli in z['C_CTP_CLI']:
            #            queryc = 'SELECT C_FIS FROM KCAA.VKCA15_CLI WHERE C_CTP_CLI =' + str(cli) + ';'
            clien = databasconnect.queryexec(querycfis(cli), conn)
            cli2 = clien.values[0][0]
            if cli2 is not None:
                intest1 = intestatario1base(cli2, conn, mod)
    return intest1


def intestatario1base(ctpcli, conn, mod):
    if mod == 'CON':
        intest1 = databasconnect.queryexec(querycfis(ctpcli), conn)
    elif mod == 'BUS':
        query = 'SELECT COALESCE(CAST(C_PAR_IVA AS VARCHAR), C_FIS ) FROM KCAA.VKCA15_CLI vc2 WHERE C_CTP_CLI =' + ctpcli + ';'
        intest1 = databasconnect.queryexec(query, conn)
    return intest1.values[0][0]


def intesebb(supp, conn):
    queryi = 'SELECT C_INT FROM KCAA.VKCA01_BB_CLI WHERE C_SUP_COD = \'' + supp \
             + '\' AND CURRENT_TIMESTAMP BETWEEN D_INI_VAL AND D_FIN_VAL;'
    querye = 'SELECT C_ESE FROM KCAA.VKCA01_BB_CLI WHERE C_SUP_COD = \'' + supp \
             + '\' AND CURRENT_TIMESTAMP BETWEEN D_INI_VAL AND D_FIN_VAL;'
    intest1 = databasconnect.queryexec(queryi, conn)
    esec1 = databasconnect.queryexec(querye, conn)
    return intest1.values[0][0], esec1.values[0][0]


# xxx=controllogiornaliero('File/BTPRD.ETS.AUIBB.OPE.N01416', 'BB', costs)
# filein = 'File/BTPRD.ETS.AUIBB.OPE.N01416'
# mod = 'BB'
# cost = costanti()
# data = '2022-03-03'
# ntrx = 15


def esecutore1(ctr, supp, tiptit, conn):
    titolo = databasconnect.queryexec(querytit(supp, tiptit), conn)['C_TIT'][0]
    cliutipin = databasconnect.queryexec(querycliutipin(titolo, ctr), conn)['C_CTP_UTI'][0]
    esec1 = databasconnect.queryexec(querycfis(cliutipin), conn)['C_FIS'][0]
    if esec1 is None:
        z = alttetsav(ctr, conn)
        if len(z) > 0:
            for cli in z['C_CTP_CLI']:
                #            queryc = 'SELECT C_FIS FROM KCAA.VKCA15_CLI WHERE C_CTP_CLI =' + str(cli) + ';'
                clien = databasconnect.queryexec(querycfis(cli), conn)
                cli2 = clien.values[0][0]
                if cli2 is not None:
                    esec1 = databasconnect.queryexec(querycfis(cli2), conn)['C_FIS'][0]
        else:
            queryorg = 'SELECT C_FIS_ORG FROM KCAA.VKCA15_CLI WHERE C_CTP_CLI = \'' + cliutipin + '\';'
            esec1 = databasconnect.queryexec(queryorg, conn)['C_FIS_ORG'][0]
    return esec1


def querytit(supp, tiptit):
    if tiptit == 'AP':
        query = 'SELECT C_TIT FROM KCAA.VKCAO9_APP WHERE C_APP = \'' + supp + '\';'
    else:
        query = 'SELECT C_TIT FROM KCAA.VKCAI7_APP_VIR WHERE C_OBU_ID = \'' + supp + '\';'
    return query


def querycliutipin(titolo, ctr):
    query = 'SELECT C_CTP_UTI FROM KCAA.VKCA01_CLI_UTI_PYN WHERE C_CTR=\'' + ctr + '\' AND C_TIT= \'' + \
            titolo + '\'' + \
            ' AND CURRENT TIMESTAMP BETWEEN D_INI_VAL AND D_FIN_VAL;'
    return query


def querycfis(ctpcli):
    query = 'SELECT C_FIS FROM KCAA.VKCA15_CLI vc2 WHERE C_CTP_CLI =' + ctpcli + ';'
    return query


def queryriccli(ctpcli):
    query = 'SELECT C_RIC_CLI FROM KCAA.VKCAAV_COD_TLP_IT WHERE C_CTP_CLI=' + ctpcli + ';'
    return query


def querycli(ctr):
    query = 'SELECT C_CTP_CLI FROM KCAA.VKCA27_CTR vc WHERE C_CTR = ' + str(ctr) + ';'
    return query


def alttetsav(ctr, conn):
    x = databasconnect.queryexec(querycli(ctr), conn)
    y = databasconnect.queryexec(queryriccli(x.values[0][0]), conn)
    queryz = 'SELECT C_CTP_CLI FROM KCAA.VKCAAV_COD_TLP_IT WHERE C_RIC_CLI =\'' + str(
        y.values[0][0]) + '\' AND C_CTP_CLI!=' + str(x.values[0][0]) + ';'
    queryn = 'SELECT COUNT(*) FROM KCAA.VKCAAV_COD_TLP_IT WHERE C_RIC_CLI = \'' + str(
        y.values[0][0]) + '\' AND C_CTP_CLI!=' + str(x.values[0][0]) + ';'
    nz = databasconnect.queryexec(queryn, conn)
    z = databasconnect.queryexec(queryz, conn, n=nz.values[0][0])
    return z

# filein = 'File/BTPRD.ETS.AUIB2B.OPE.N00164'
# mod = 'BUS'
# data = '2020-07-29'
# ntrx = 7
