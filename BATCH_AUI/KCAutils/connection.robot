*** Settings ***
Documentation    Esempi di snippet per connettersi ad un host remoto tramite
...    protocollo SSH ed effettuare trasferimenti di file con protocollo SFTP.
Library    SSHLibrary
Library    OperatingSystem
Test Setup    Apertura Connessione SSH Ad Ambiente Remoto    ${REMOTE_HOST}    ${SSH_USER}    ${SSH_PASSWORD}
Test Teardown    Chiusura Connessioni SSH

*** Variables ***
${REMOTE_HOST}    127.0.0.1
${SSH_USER}    sshuser-2
${SSH_PASSWORD}    Password2022
${nomeFile}    report.txt

*** Test Cases ***
Test Di Connessione SSH E Trasferimento File
    Importazione File Da Ambiente Remoto    ${nomeFile}

*** Keywords ***
Apertura Connessione SSH Ad Ambiente Remoto
    [Documentation]    Snippet per una connessione SSH standard. Qualora la porta
    ...    non fosse specificata in maniera esplicita, usare la porta di default
    ...    che riporta valore 22. La keyword Login si utilizza solo nei casi in cui
    ...    è richiesta un'autenticazione.
    [Arguments]    ${REMOTE_HOST}    ${SSH_USER}    ${SSH_PASSWORD}
    SSHLibrary.Open Connection    ${REMOTE_HOST}    port=22
    SSHLibrary.Login     ${SSH_USER}    ${SSH_PASSWORD}    delay=1

Chiusura Connessioni SSH
    [Documentation]    Chiusura di ogni connessioni aperta verso un ambiente remoto
    ...    eseguita tramite protocollo SSH. Normalmente è una best practice chiudere
    ...    le connessioni aperte per motivi di sicurezza.
    SSHLibrary.Close All Connections

Importazione File Da Ambiente Remoto
    [Documentation]    Snippet per un trasferimento standard con protocollo SFTP
    ...    che importerà un file denominato report.txt all'interno di un folder
    ...    interna al progetto denominata a sua volta tmp. Si danno quindi due parametri
    ...    keyword, rispettivamente il sorgente da cui attingere e il path di destinazione.
    ...    NOTA: Per fare un controllo sull'esistenza dei file si usano due librerie differenti:
    ...    SSHLibrary per verificare il file sull'ambiente remoto e OperatingSystem per verificare
    ...    l'esistenza del file sull'ambiente locale. Ogni keyword ha una funzionalità specifica
    ...    e un dominio di competenza.
    [Arguments]    ${nomeFile}
    SSHLibrary.File Should Exist    /test_automation/testing/reports/${nomeFile}
    SSHLibrary.Get File    /test_automation/testing/reports/${nomeFile}    /tmp
    OperatingSystem.File Should Exist    /tmp/${nomeFile}

