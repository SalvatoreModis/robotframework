from datetime import date, timedelta


def trovafilegiornaliero(datapartenza, progressivoiniziale, mod):
    partenza = datapartenza.date()
    oggi = date.today()
    numfile = (oggi - partenza).days
    progressivi = range(1, numfile)
    prefisso = prefile('G', mod)
    elencofile = []
    datefile = []
    for prg in progressivi:
        nomefile = prefisso + str(int(progressivoiniziale + prg)).zfill(5)
        elencofile.append(nomefile)
        datefile.append(partenza + timedelta(days=prg))
    return elencofile, datefile


def trovafilequadratura(progressivoiniziale, mod):
    oggi = date.today()
    progressivo = int(progressivoiniziale)
    prefisso = prefile('Q', mod)
    nomefile = prefisso + str(progressivo).zfill(5)
    datefile = []
    if oggi.day >= 16:
        for g in range(15):
            gx = date(oggi.year, oggi.month, g + 1)
            datefile.append(gx)
    else:
        finemese = date(oggi.year, oggi.month, 1) - timedelta(days=1)
        for g in range(15, finemese.day):
            gx = date(oggi.year, oggi.month-1, g + 1)
            datefile.append(gx)
    return nomefile, datefile


def prefile(tipo, mod):
    match tipo:
        case 'G':
            match mod:
                case 'CON':
                    return 'BTPRD.ETS.AUITP.OPE.N'
                case 'BUS':
                    return 'BTPRD.ETS.AUIB2B.OPE.N'
                case 'BB':
                    return 'BTPRD.ETS.AUIBB.OPE.N'
        case 'Q':
            match mod:
                case 'CON':
                    return 'BTPRD.ETS.QRP2C.OPE.N'
                case 'BUS':
                    return 'BTPRD.ETS.QRPB2B.OPE.N'
                case 'BB':
                    return 'BTPRD.ETS.QRPBB.OPE.N'


def controllorighe(indfile, tipo):
    file = open(indfile)
    if file.read() == '\n':
        return 0, str(0)
    else:
        line_count = 0
        flag = 0
        lriga = 0
        if tipo == 'G':
            lriga = 1001
        elif tipo == 'Q':
            lriga = 296
        file = open(indfile)
        for line in file:
            line_count += 1
            if len(line) != lriga:
                print('Lunghezza riga ' + str(line_count) + ' non conforme')
                flag = 1
        return line_count, str(flag)

