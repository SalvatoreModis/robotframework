*** Settings ***
Library  SSHLibrary
Library  KCAutils/runbatch.py
Library  KCAutils/DownloadFileAUI.py
Library  KCAutils/AUIutils.py
Library  KCAUtils/verificagiornaliero.py
Library  KCAUtils/verificaquadratura.py
Library  KCAUtils/querygen.py
Resource  Risorse/LibraryDBParamRestReq.robot
Resource  Risorse/KeywordGiornalieri.robot
Resource  Risorse/KeywordQuadratura.robot
Test Setup       dbConnection    ${db1}
Test Teardown        disconnect from database


*** Variables ***
${app}       KCA
#${porta}     22
${username}  usr_87002789_gruppo_autostrade_i
${keyadd}    ../../../.ssh/id_rsa
${db1}       CPDB28
${db2}       CPDB30

*** Test Cases ***
VerifyDB
# Setup iniziale del test, imposta il valore della macchina dei batch con l'IP corretto utilizzando la funzione python
# runbatch.macchina
    [Tags]    KCA  KCC
    ${host}  runbatch.macchina  ${app}
    SET GLOBAL VARIABLE     ${host}

KCAJ14
# Batch AUI giornaliero per contratti di tipo consumer
    [Tags]     AUI  GIORNALIERO  CONSUMER
    ${TSkcaj14}  ${progkcaj14}  Parametri batch giornaliero  KCAJ14
    # keyword per ricavare da db i valori del timestamp iniziale e del progressivo iniziale
    Esecuzione batch  ${app}  kcag2j14
    # keyword per l'esecuzione del batch
    Scaricamento file giornalieri  ${TSkcaj14}  ${progkcaj14}  CON
    # keyword per lo scaricamento e i controlli sui file generati dall'ultima esecuzione dei batch
    Fine test
    # keyword per il buon esito del test

KCAJ15
# Batch AUI quindicinale per contratti di tipo consumer
    [Tags]     AUI  QUADRATURA  CONSUMER
    Esecuzione batch  ${app}  kcaq2j15
    # keyword per l'esecuzione del batch
    ${progkcaj15}  Parametri batch quadratura  KCAJ15
    # keyword per ricavare il valore dell'ultimo progressivo generato in db
    Scaricamento file quadratura  ${progkcaj15}  CON
    # keyword per lo scaricamento e i controlli sul file generato
    Fine test
    # keyword per il buon esito del test

KCAJ16
# Batch AUI giornaliero per contratti di tipo business
    [Tags]     AUI  GIORNALIERO  BUSINESS
    ${TSkcaj16}  ${progkcaj16}  Parametri batch giornaliero  KCAJ16
    # keyword per ricavare da db i valori del timestamp iniziale e del progressivo iniziale
    Esecuzione batch  ${app}  kcag3j16
    # keyword per l'esecuzione del batch
    Scaricamento file giornalieri  ${TSkcaj16}  ${progkcaj16}  BUS
    # keyword per lo scaricamento e i controlli sui file generati dall'ultima esecuzione dei batch
    Fine test
    # keyword per il buon esito del test

KCAJ17
# Batch AUI quindicinale per contratti di tipo business
    [Tags]     AUI  QUADRATURA  BUSINESS
    Esecuzione batch  ${app}  kcaq3j17
    # keyword per l'esecuzione del batch
    ${progkcaj17}     Parametri batch quadratura  KCAJ17
    # keyword per ricavare il valore dell'ultimo progressivo generato in db
    Scaricamento file quadratura  ${progkcaj17}  BUS
    # keyword per lo scaricamento e i controlli sul file generato
    Fine test
    # keyword per il buon esito del test

KCAJ23
# Batch AUI giornaliero per contratti di tipo BigBang
    [Tags]     AUI  GIORNALIERO  BIGBANG
    ${TSkcaj23}  ${progkcaj23}  Parametri batch giornaliero  KCAJ23
    # keyword per ricavare da db i valori del timestamp iniziale e del progressivo iniziale
    Esecuzione batch  ${app}  kcag5j23
    # keyword per l'esecuzione del batch
    Scaricamento file giornalieri  ${TSkcaj23}  ${progkcaj23}  BB
    # keyword per lo scaricamento e i controlli sui file generati dall'ultima esecuzione dei batch
    Fine test
    # keyword per il buon esito del test

KCAJ24
# Batch AUI quindicinale per contratti di tipo BigBang
    [Tags]     AUI  QUADRATURA  BIGBANG
    Esecuzione batch  ${app}  kcaq4j24
    # keyword per l'esecuzione del batch
    ${progkcaj24}     Parametri batch quadratura  KCAJ24
    # keyword per ricavare il valore dell'ultimo progressivo generato in db
    Scaricamento file quadratura  ${progkcaj24}  BB
    # keyword per lo scaricamento e i controlli sul file generato
    Fine test
    # keyword per il buon esito del test

*** Keywords ***
Esecuzione batch
    [Arguments]  ${applicativo}  ${batch}
    ${esito}=  runbatch.batchrun  ${app}  ${batch}
    SHOULD BE EQUAL AS STRINGS  ${esito}  fine OK

Fine test
    SHOULD BE EQUAL  ${esito}   0