*** Settings ***
Resource          ../../ENA_TEST/LoadLibrary.txt
Library           String
Resource          ../../ENA_TEST/variablesBasepathEna.txt

*** Variables ***
${endpoint}       http://ena.test.gcp.telepass.com:8080
${obuID}          1000119691    #${EMPTY}    #1000119691    #1000012011
${obuID_business}    6000000092
${ctrID_str1}     ${EMPTY}
${userID_str1}    ${EMPTY}
${ctpCli_str1}    ${EMPTY}
${titleID_str1}    ${EMPTY}
#Device ID for Tpay and
@{registrationID_str1_tpay}    fUQkpXc1Q6a2JrwazUXkzY:APA91bGJGuGAXhEBrArI11QtDh5Uk9oCUyEibwag4rxtnNtMd5G8GYWqsf-f_KE28wc749LvF9zddHq4km_3nY8zn3f_P03NyHxhtADZduP0cV_hlC39FJmpoYdoD2eh10jI6s2Fz8_s    9e7aa77cb05ed901bbf06f6276f4e579b577f49f9689776bea0f37c6aefa1318
#Device ID for TPAYX
@{registrationID_str1_tpayx}    eEPnpwg7Tfua85AoIEpQAI:APA91bFgwPrpZAq0NJbmNPgfu_XRACb0MIxJW0BzdG8ObPycIkTZ68y3iRJT3zHhdib3XjVILC7K467ez1jOIOcAaJzJVVF5wjlAf9E4nx93YvYLt4E8a4C3hbzv75JvB3uRoGTbq2-l    0aa9b7cbe19b0b567a8c9e09244966dfdeaf98d22a6a72cedb6fc2ae208141a1
@{id_list}        ${EMPTY}
@{ListIDREST}
#Notification group for tpay
@{assocGroup_tpay}    KTH-an    KTH-ios
#Notification group for tpayX
@{assocGroup_tpayx}    KTP-an    KTP-ios
@{deviceModel}    android    iPhone
@{deviceType}     and    iOS
#applicationID TPAY/TPAYX
@{applicationID}    KTH    KTP
@{registrationID_business}    fUQkpXc1Q6a2JrwazUXkzY:APA91bGJGuGAXhEBrArI11QtDh5Uk9oCUyEibwag4rxtnNtMd5G8GYWqsf-f_KE28wc749LvF9zddHq4km_3nY8zn3f_P03NyHxhtADZduP0cV_hlC39FJmpoYdoD2eh10jI6s2Fz8_s    9e7aa77cb05ed901bbf06f6276f4e579b577f49f9689776bea0f37c6aefa1318
${ctrID_str1_business}    600000749

*** Test Cases ***
VerifyDB
    [Tags]    TPAY    TPAYX    TPAYB
    dbConnection    CPDB06
    Table Must Exist    TENA05_NOT
    Table Must Exist    TENA07_INV_NOT
    Table Must Exist    TETSJU_ATT_PNG
    Table Must Exist    TENA01_DEV
    CollectingINFO    CPDB06    ${obuID}

Logout
    [Tags]    TPAY
    Set Suite Variable    ${obuID}    1000119691
    CollectingINFO    CPDB06    ${obuID}
    ${body_logout}    Catenate    {\n    "applicationId":"${applicationID}[0]",\n    "userId":"${userID_str1}", \n    "obuType":"SM"    \n}
    ${body_logout_json}    evaluate    json.loads('''${body_logout}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_logout}    ${body_logout_json}

Register
    [Tags]    TPAY
    ${index}=    Set Variable    0
    FOR    ${index}    IN RANGE    2
        ${body_register}    Catenate    {\n    "deviceInfo" : \n    {\n    \    "applicationId" : "${applicationID}[0]",\n    \    "registrationId" : "${registrationID_str1_tpay}[${index}]",\n    \    "deviceType" : "${deviceType}[${index}]",\n    \    "registrationTime" : "2020-01-21",\n    \    "clientCode" : "${ctpCli_str1}",\n    "contractCode" : ${ctrID_str1},\n
        ...    \    "titleCode" : "${titleID_str1}",\n    \    "obuId" : "${obuID}",\n \ \ \ \ \ \ \ "obuType" : "SM",\n    \    "userId" : "${userID_str1}",\n    \    "deviceModel" : "${deviceModel}[${index}]",\n    \    "osDeviceVersion" : "13.3",\n    \    "appVersion" : "3.7.5",\n    \    "latitude" : null,\n    "longitude" : null,\n
        ...    \    "associatedGroups" : ["${assocGroup_tpay}[${index}]"]\n    }\n    }
        ${body_register_json}    evaluate    json.loads('''${body_register}''')    json
        ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_register}    ${body_register_json}
        ${index}=    Evaluate    ${index} + 1
    END

SendToCustomerOBU
    [Tags]    TPAY
    ${body_customerOBU}    Catenate    {\n \ \ "applicationId":"${applicationID}[0]",\n \ \ "obuId":"${obuID}",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerOBU_json}    evaluate    json.loads('''${body_customerOBU}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerOBU}    ${body_customerOBU_json}

SendToCustomerCTR
    [Tags]    TPAY
    ${body_customerCTR}    Catenate    {\n \ \ "applicationId":"${applicationID}[0]",\n \ \ "contractCode":"${ctrID_str1}",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerCTR_json}    evaluate    json.loads('''${body_customerCTR}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerCTR}    ${body_customerCTR_json}

SendToCustomerTIT
    [Tags]    TPAY
    ${body_customerTIT}    Catenate    {\n \ \ "applicationId":"${applicationID}[0]",\n \ \ "titleCode":"${titleID_str1}",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerTIT_json}    evaluate    json.loads('''${body_customerTIT}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerTIT}    ${body_customerTIT_json}
    [Teardown]    Log    ${response}

SendToCustomerUSR
    [Tags]    TPAY
    ${body_customerUSR}    Catenate    {\n \ \ "applicationId":"${applicationID}[0]",\n \ \    "userId":"${userID_str1}",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerUSR_json}    evaluate    json.loads('''${body_customerUSR}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerUSR}    ${body_customerUSR_json}

SendToCustomerDEV
    [Tags]    TPAY
    @{id_list}    Create List
    ${index}=    Set Variable    0
    FOR    ${index}    IN RANGE    2
        ${body_customerDEV}    Catenate    {"applicationId":"${applicationID}[0]",\n"deviceId":"${registrationID_str1_tpay}[${index}]",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
        ${body_customerDEV_json}    evaluate    json.loads('''${body_customerDEV}''')    json
        ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerDEV}    ${body_customerDEV_json}
        ${id}    Get From Dictionary    ${response.json()}    notificationId
        Append To List    ${id_list}    ${id}
        ${index}=    Evaluate    ${index} + 1
    END
    Set Suite Variable    ${id_list}    ${id_list}

GetLastNot
    [Tags]    TPAY
    ${response}    LoadLibrary.GetRequest    ${endpoint}    ${basepath_getLastNOT}
    Should Not Be Empty    ${response.json()['notifications']}
    FOR    ${item}    IN    ${response.json()['notifications']}
    FOR    ${subitem}    IN    @{item}
        IF    "${subitem}[deviceType]" == "IO"
        ${notification_ID_IO}    Get From Dictionary    ${subitem}    notificationId
        ELSE IF    "${subitem}[deviceType]" == "AN"
        ${notification_ID_AN}    Get From Dictionary    ${subitem}    notificationId
    END
    END
    END
    Should Be Equal As Numbers    ${notification_ID_IO}    ${id_list}[1]
    Should Be Equal As Numbers    ${notification_ID_AN}    ${id_list}[0]
    Log    ${id_list}

LogoutX
    [Tags]    TPAYX
    Set Suite Variable    ${obuID}    1000093573
    CollectingINFO    CPDB06    ${obuID}
    ${body_logout}    Catenate    {\n    "applicationId":"${applicationID}[1]",\n    "obuId" : "${obuID}", \n    "obuType":"SM"    \n}
    ${body_logout_json}    evaluate    json.loads('''${body_logout}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_logout_obu}    ${body_logout_json}

RegisterX
    [Tags]    TPAYX
    ${index}=    Set Variable    0
    FOR    ${index}    IN RANGE    2
        ${body_register}    Catenate    {\n    "deviceInfo" : \n    {\n    \    "applicationId" : "${applicationID}[1]",\n    \    "registrationId" : "${registrationID_str1_tpayx}[${index}]",\n    \    "deviceType" : "${deviceType}[${index}]",\n    \    "registrationTime" : "2020-01-21",\n    \    "clientCode" : "${ctpCli_str1}",\n    "contractCode" : ${ctrID_str1},\n
        ...    \    "titleCode" : "${titleID_str1}",\n    \    "obuId" : "${obuID}",\n \ \ \ \ \ \ \ "obuType" : "SM",\n    \    "userId" : "${userID_str1}",\n    \    "deviceModel" : "${deviceModel}[${index}]",\n    \    "osDeviceVersion" : "13.3",\n    \    "appVersion" : "3.7.5",\n    \    "latitude" : null,\n    "longitude" : null,\n
        ...    \    "associatedGroups" : ["${assocGroup_tpayx}[${index}]"]\n    }\n    }
        ${body_register_json}    evaluate    json.loads('''${body_register}''')    json
        ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_register}    ${body_register_json}
        ${index}=    Evaluate    ${index} + 1
    END
    [Teardown]    Log    ${response}

SendToCustomerOBUX
    [Tags]    TPAYX
    ${body_customerOBU}    Catenate    {\n \ \ "applicationId":"${applicationID}[1]",\n \ \ "obuId":"${obuID}",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerOBU_json}    evaluate    json.loads('''${body_customerOBU}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerOBU}    ${body_customerOBU_json}
    [Teardown]    Log    ${response}

SendToCustomerCTRX
    [Tags]    TPAYX
    ${body_customerCTR}    Catenate    {\n \ \ "applicationId":"${applicationID}[1]",\n \ \ "contractCode":"${ctrID_str1}",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerCTR_json}    evaluate    json.loads('''${body_customerCTR}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerCTR}    ${body_customerCTR_json}
    [Teardown]    Log    ${response}

SendToCustomerTITX
    [Tags]    TPAYX
    ${body_customerTIT}    Catenate    {\n \ \ "applicationId":"${applicationID}[1]",\n \ \ "titleCode":"${titleID_str1}",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerTIT_json}    evaluate    json.loads('''${body_customerTIT}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerTIT}    ${body_customerTIT_json}

SendToCustomerUSRX
    [Tags]    TPAYX
    ${body_customerUSR}    Catenate    {\n \ \ "applicationId":"${applicationID}[1]",\n \ \    "userId":"${userID_str1}",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerUSR_json}    evaluate    json.loads('''${body_customerUSR}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerUSR}    ${body_customerUSR_json}

SendToCustomerDEVX
    [Tags]    TPAYX
    @{id_list}    Create List
    ${index}=    Set Variable    0
    FOR    ${index}    IN RANGE    2
        ${body_customerDEV}    Catenate    {"applicationId":"${applicationID}[1]",\n"deviceId":"${registrationID_str1_tpayx}[${index}]",\n \ \ "deviceType": "SM",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
        ${body_customerDEV_json}    evaluate    json.loads('''${body_customerDEV}''')    json
        ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerDEV}    ${body_customerDEV_json}
        ${id}    Get From Dictionary    ${response.json()}    notificationId
        Append To List    ${id_list}    ${id}
        ${index}=    Evaluate    ${index} + 1
    END
    Set Suite Variable    ${id_list}    ${id_list}

GetLastNotX
    [Tags]    TPAYX
    ${response}    LoadLibrary.GetRequest    ${endpoint}    ${basepath_getLastNOT}
    Should Not Be Empty    ${response.json()['notifications']}
    FOR    ${item}    IN    ${response.json()['notifications']}
    FOR    ${subitem}    IN    @{item}
        IF    "${subitem}[deviceType]" == "IO"
        ${notification_ID_IO}    Get From Dictionary    ${subitem}    notificationId
        ELSE IF    "${subitem}[deviceType]" == "AN"
        ${notification_ID_AN}    Get From Dictionary    ${subitem}    notificationId
    END
    END
    END
    Should Be Equal As Numbers    ${notification_ID_IO}    ${id_list}[1]
    Should Be Equal As Numbers    ${notification_ID_AN}    ${id_list}[0]
    Log    ${id_list}

LogoutBusiness
    [Tags]    TPAYB
    ${body_logout}    Catenate    {\n    "applicationId":"${applicationID}[0]",\n    "obuId":"6000000092", \n    "obuType":"TP"    \n}
    ${body_logout_json}    evaluate    json.loads('''${body_logout}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_logout_obu}    ${body_logout_json}

RegisterBusiness
    [Tags]    TPAYB
    ${index}=    Set Variable    0
    FOR    ${index}    IN RANGE    2
        ${body_register}    Catenate    {\n    "deviceInfo" : \n    {\n    \    "applicationId" : "${applicationID}[0]",\n    \    "registrationId" : "${registrationID_business}[${index}]",\n    \    "deviceType" : "${deviceType}[${index}]",\n    \    "registrationTime" : "2020-01-21",\n    \    "clientCode" : "600000897",\n    "contractCode" : 600000749,\n
        ...    \    "titleCode" : null,\n    \    "obuId" : "6000000092",\n \ \ \ \ \ \ \ "obuType" : "TP",\n    \    "userId" : null,\n    \    "deviceModel" : "${deviceModel}[${index}]",\n    \    "osDeviceVersion" : "13.3",\n    \    "appVersion" : "3.7.5",\n    \    "latitude" : null,\n    "longitude" : null,\n
        ...    \    "associatedGroups" : ["${assocGroup_tpay}[${index}]"]\n    }\n    }
        ${body_register_json}    evaluate    json.loads('''${body_register}''')    json
        ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_register}    ${body_register_json}
        ${index}=    Evaluate    ${index} + 1
    END
    [Teardown]    Log    ${response}

SendToCustomerOBUBusiness
    [Tags]    TPAYB
    ${body_customerOBU}    Catenate    {\n \ \ "applicationId":"${applicationID}[0]",\n \ \ "obuId":"${obuID_business}",\n \ \ "deviceType": "TP",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerOBU_json}    evaluate    json.loads('''${body_customerOBU}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerOBU}    ${body_customerOBU_json}

SendToCustomerCTRBusiness
    [Tags]    TPAYB
    ${body_customerCTR}    Catenate    {\n \ \"deviceType": "TP", \n \ \ "applicationId":"${applicationID}[0]",\n \ \ "contractCode":"${ctrID_str1_business}",\n \ \ \n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
    ${body_customerCTR_json}    evaluate    json.loads('''${body_customerCTR}''')    json
    ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerCTR}    ${body_customerCTR_json}

SendToCustomerDEVBusiness
    [Tags]    TPAYB
    @{id_list}    Create List
    ${index}=    Set Variable    0
    FOR    ${index}    IN RANGE    2
        ${body_customerDEV}    Catenate    {"applicationId":"${applicationID}[0]",\n"deviceId":"${registrationID_business}[${index}]",\n \ \ "deviceType": "TP",\n \ \ "sendOnlyLastDev":"true",\n \ \ "notificationData":{\n \ \ \ \ \ "notificationTitle":"Notification Test",\n \ \ \ \ \ "notificationBodyText":"Test di ENA in corso",\n \ \ \ \ \ "notificationLink":"linkPerNotifica",\n \ \ \ \ \ "notificationType":"fuel",\n \ \ \ \ \ "txId":"KDB_1c4e6250-dc1d-496e-9694-5b4bd3ac8b11",\n \ \ \ \ \ "txAmount":142.01,\n \ \ \ \ \ "txTimestamp":1508778900000,\n \ \ \ \ \ "txResult":"ACCEPTED"\n \ \ }\n}
        ${body_customerDEV_json}    evaluate    json.loads('''${body_customerDEV}''')    json
        ${response}    LoadLibrary.PostRequest    ${endpoint}    ${basepath_sendCustomerDEV}    ${body_customerDEV_json}
        ${id}    Get From Dictionary    ${response.json()}    notificationId
        Append To List    ${id_list}    ${id}
        ${index}=    Evaluate    ${index} + 1
    END
    Set Suite Variable    ${id_list}    ${id_list}

GetLastNotBusiness
    [Tags]    TPAYB
    ${response}    LoadLibrary.GetRequest    ${endpoint}    ${basepath_getLastNOT}
    Should Not Be Empty    ${response.json()['notifications']}
    FOR    ${item}    IN    ${response.json()['notifications']}
    FOR    ${subitem}    IN    @{item}
        IF    "${subitem}[deviceType]" == "IO"
        ${notification_ID_IO}    Get From Dictionary    ${subitem}    notificationId
        ELSE IF    "${subitem}[deviceType]" == "AN"
        ${notification_ID_AN}    Get From Dictionary    ${subitem}    notificationId
    END
    END
    END
    Should Be Equal As Numbers    ${notification_ID_IO}    ${id_list}[1]
    Should Be Equal As Numbers    ${notification_ID_AN}    ${id_list}[0]
    Log    ${id_list}
