*** Settings ***
Resource       Resources/base_url.robot
Resource       Resources/libraries.robot

*** Variables ***
&{headers}         Content-Type=application/json      Accept=application/json     SystemCaller=KDB
&{headersBilling}    Content-Type=application/json      Accept=application/json


*** Keywords ***
###################### Keywords per Setup ######################
SetupImportoPositivo
    ${var}     Evaluate    "%.2f" % random.uniform(1,20)
    ${var1}    Evaluate    ${var}+random.uniform(-0.5,0.5)
    ${var1}    Evaluate    "%.2f" % $var1
    ${var}    Convert To Number   ${var}
    ${var1}   Convert To Number   ${var1}
    Set Suite Variable    ${amount}    ${var}
    Set Suite Variable    ${amount_new}    ${var1}

SetupImportoNegativo
    SetupImportoPositivo
    Set Suite Variable    ${amount}    -${amount}
    Set Suite Variable    ${amount_new}    -${amount_new}

SetupImportoPrenotazione
    [Arguments]    ${c_ser}
    SetupImportoPositivo
    ${infoPlafond}    info-plafond    ${c_ser}
    ${infoPlafond}   convert string to json    ${infoPlafond}
    ${tolerance}    Set Variable    ${infoPlafond['tolerance']}
    IF    ${tolerance}!=0.0
        IF    ${amount_new}>${amount}+${tolerance}
            ${amount_new}    Evaluate    ${amount}+${tolerance}-0.01
        END
    ELSE
        ${amount_new}    Set Variable    ${amount}
    END
    ${amount_new}    Evaluate    "%.2f" % $amount_new
    ${amount_new}    Convert To Number   ${amount_new}
    Set Suite Variable    ${amount_new}    ${amount_new}

SetupImportoNonNullo
    ${var}     Evaluate    "%.2f" % random.uniform(-10,20)
    ${var}    Set Variable If    ${var}==0    0.01    ${var}
    ${var1}    Evaluate    ${var}+random.uniform(-1,1)
    ${var1}    Evaluate    "%.2f" % $var1
    ${var}    Convert To Number   ${var}
    ${var1}   Convert To Number   ${var1}
    Set Suite Variable    ${amount}    ${var}
    Set Suite Variable    ${amount_new}    ${var1}


###################### Chiamate Integration Layer ######################
info-plafond
    [Arguments]    ${serviceCode}=${c_ser}    ${supportCode}=${c_sup}
    create session    mysession    ${base_url_integrationLayer}
    ${endpoint}=    convert to string    /authorizativeutils/info-plafond
    &{req_body}=    create dictionary    purchaseType=C    requestId=-    serviceCode=${serviceCode}    supportCode=${supportCode}    supportType=TP
    ${response}=        POST on session      mysession   ${endpoint}    json=${req_body}    headers=${headersBilling}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}      convert string to json    ${response.content}
    Should Not Be Equal As Strings    ${res_body['plafondList']}     ${null}
    [Return]    ${response.content}

PEGA
    [Arguments]    ${serviceCode}=${c_ser}    ${supportCode}=${c_sup}
    create session    mysession    ${base_url_integrationLayer}
    ${endpoint}=    convert to string    /authorizativeutils/client-verification-authorization
    &{req_body}=    create dictionary    purchaseType=C    supportType=TP    serviceCode=${serviceCode}    requestId=-    supportCode=${supportCode}    skipAuthorizationCheck=${false}    transactionId=-    verificationDate=${0}
    ${response}=        POST on session      mysession   ${endpoint}    json=${req_body}    headers=${headersBilling}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}      convert string to json    ${response.content}
    ${errorMessage}    Get Variable Value    ${res_body['errorMessage']}
    ${errorCode}    Get Variable Value    ${res_body['errorCode']}
    Should Be Equal As Strings    ${errorMessage}     ${null}
    Should Be Equal As Strings    ${errorCode}     ${null}
    [Return]    ${response.content}


###################### Chiamate KAE ######################
healthCheck
    create session  mysession   ${base_url_KAE}
    ${endpoint}=        convert to string    /authorization/healthCheck
    &{req_body}=        create dictionary    supportCode=6000000019    supportType=TP    serviceCode=18    transactionId=KDB_1651072709866    purchaseType=C
    ${response}=        POST on session      mysession   ${endpoint}    json=${req_body}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['result']}     00
    [Return]    ${response.content}


authorize
    [Documentation]    Input:   flagPrenotazione       importo
    [Arguments]     ${flagPrenotazione}     ${amount}
    ${timestamp}       get time    epoch
    ${paymentTime}     Evaluate    int(round(time.time() * 1000))     time

    create session    mysession     ${base_url_KAE}
    ${endpoint}=    convert to string    /authorization/authorize
    ${body_req}     Create Dictionary    supportCode=${c_sup}    supportType=TP    serviceCode=${c_ser}    purchaseType=C    transactionId=KDB_H783B10T-973H-${timestamp}    skipAuthorizationCheck=${false}    amount=${amount}    pointOfServiceDescription=STAZIONE Q8 CENTRO DEI BORGHI    provisionAddress=VIA ADENOCROMO    paymentTime=${paymentTime}    currency=EUR    merchantId=25267395    isBooking=${flagPrenotazione}    skipPlafondCheck=${false}    pointOfService=52    latitude=33    longitude=12    plateNumber=AA000ZZ    plateCountry=IT    agreementId=1    SCAFlag=N    mainAuthorizationId=${null}    mainTransactionId=${null}    installmentNumber=${null}    installmentTotalCount=${null}    transactionList=${null}

    ${response}=    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}   200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['errorCode']}     ${null}
    Should Be Equal As Strings    ${res_body['errorMessage']}     ${null}
    [Return]    ${response.content}

Autorizzazione Secca
    [Documentation]    Input:   importo
    [Arguments]    ${amount}
    ${result}     authorize    ${false}    ${amount}
    [Return]    ${result}

Prenotazione
    [Documentation]    Input:   importo
    [Arguments]    ${amount}
    ${result}     authorize    ${true}    ${amount}
    [Return]    ${result}


confermaPrenotazione
    [Documentation]    Input:   importo
    [Arguments]    ${amount}
    create session    mysession     ${base_url_KAE}
    ${endpoint}=    convert to string    /authorization/confirm
    ${body_req}     Create Dictionary    pointOfService=52    latitude=33    longitude=12    agreementId=1    confirmedAmount=${amount}    pointOfServiceDescription=STAZIONE Q8 CENTRO DEI BORGHI    merchantId=25267395    authorizationId=${d_aut}    transactionId=${transactionID}
    ${response}=    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['errorCode']}     ${null}
    Should Be Equal As Strings    ${res_body['errorMessage']}     ${null}
    [Return]    ${response.content}


modificaAutorizzazione
    [Documentation]    Input:   importo
    [Arguments]    ${amount}
    create session    mysession     ${base_url_KAE}
    ${endpoint}    convert to string    /authorization/modify

    ${paymentTime}     Evaluate    int(round(time.time() * 1000))     time
    ${body_req}     create dictionary    pointOfService=52    latitude=18    longitude=12    plateNumber=AA000ZZ    plateCountry=IT    agreementId=123456    SCAFlag=N    supportCode=${c_sup}    supportType=TP    serviceCode=${c_ser}    purchaseType=C    amount=${amount}    pointOfServiceDescription=Q8 PIENZA    paymentTime=${paymentTime}    currency=EUR    merchantId=25267395    provisionAddress=Via Garibaldi 1000    authorizationId=${d_aut}    transactionId=${transactionID}    skipAuthorizationCheck=${false}

    ${response}    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['errorCode']}     ${null}
    Should Be Equal As Strings    ${res_body['errorMessage']}     ${null}
    [Return]    ${response.content}



ottieniStorno
    [Documentation]    Input:    #identificativoTransazione
    [Arguments]     #${transactionID}
    create session    mysession     ${base_url_KAE}
    ${endpoint}=    convert to string    /authorization/refund

    ${body_req}     Create Dictionary    pointOfService=${null}    latitude=${null}    longitude=${null}    plateNumber=${null}    plateCountry=${null}    agreementId=${null}    scaFlag=${null}    authorizationId=${null}    transactionId=${transactionId}

    ${response}    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['errorCode']}     ${null}
    Should Be Equal As Strings    ${res_body['errorMessage']}     ${null}
    [Return]    ${response.content}


confermaMultipla
    [Arguments]    ${amount_new}
        ${d_aut_new} 	Get Variable Value	${d_aut_new}	${d_aut}
        ${transactionID_new} 	Get Variable Value	${transactionID_new}	${transactionID}
    create session    mysession     ${base_url_KAE}
    ${endpoint}    convert to string    /authorization/multipleConfirm

    ${amount}    Convert To Number    ${amount_new}
    ${amount1}    evaluate    "%.2f" % ($amount*random.uniform(0.2,0.5))
    ${var}    Convert To Number   ${amount1}
    ${amount2}    evaluate    "%.2f" % ($amount-$var)
    Set Suite Variable    @{listaImporti}    ${amount1}    ${amount2}

    ${paymentTime}     Evaluate    int(round(time.time() * 1000))     time
    ${transazione1}      Create Dictionary    paymentTime=${paymentTime}    provisionAddress=IMPIANTI ABETONE    amount=${amount1}    transactionId=${transactionID_new}-001    merchantId=25264311    agreementId=349
    ${paymentTime}     Evaluate    int(round(time.time() * 1000))     time
    ${transazione2}      Create Dictionary    paymentTime=${paymentTime}    provisionAddress=IMPIANTI QUERCIONE    amount=${amount2}    transactionId=${transactionID_new}-002    merchantId=25264311    agreementId=350
    ${listaTrx}    create list    ${transazione1}    ${transazione2}

    ${body_req}      Create Dictionary    authorizationId=${d_aut_new}    transactionId=${transactionID_new}    transactionList=${listaTrx}
    ${response}    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['errorCode']}     ${null}
    Should Be Equal As Strings    ${res_body['errorMessage']}     ${null}
    [Return]    ${response.content}


ottieniAutRateizzazione
    [Documentation]    Input:
    [Arguments]
    Set Suite Variable    ${amount}        ${42.5}
    Set Suite Variable    @{listaImporti}    ${27.5}    ${10.0}    ${5.0}

    ${timestamp}       get time    epoch
    ${paymentTime}     Evaluate    int(round(time.time() * 1000))     time
    ${transactionID}    Set Variable    KDB_ABB_H783B10T-973H-${timestamp}

    ${paymentTime_new}     Evaluate    int(round(time.time() * 1000))     time
    ${rata}        Create Dictionary    transactionId=${transactionID}_R1    amount=${27.5}    provisionAddress=Foro Bonaparte, 61    pointOfServiceDescription=STAZIONE Q8 CENTRO DEI BORGHI    paymentTime=${paymentTime_new}    installmentNumber=${1}
    ${paymentTime_new}     Evaluate    int(round(time.time() * 1000))     time
    ${spesa1}      Create Dictionary    transactionId=${transactionID}_S1    amount=${10.0}    provisionAddress=Foro Bonaparte, 61    pointOfServiceDescription=STAZIONE Q8 CENTRO DEI BORGHI    paymentTime=${paymentTime_new}    installmentNumber=${null}
    ${paymentTime_new}     Evaluate    int(round(time.time() * 1000))     time
    ${spesa2}      Create Dictionary    transactionId=${transactionID}_S2    amount=${5.0}    provisionAddress=Foro Bonaparte, 61    pointOfServiceDescription=STAZIONE Q8 CENTRO DEI BORGHI    paymentTime=${paymentTime_new}    installmentNumber=${null}
    ${transactionList}    create list    ${rata}    ${spesa1}    ${spesa2}

    create session    mysession     ${base_url_KAE}
    ${endpoint}=    convert to string    /authorization/authorize
    ${body_req}     Create Dictionary    supportCode=${c_sup}    supportType=TP    serviceCode=${c_ser}    purchaseType=C    transactionId=${transactionID}    skipAuthorizationCheck=${false}    amount=${42.5}    pointOfServiceDescription=STAZIONE Q8 CENTRO DEI BORGHI    provisionAddress=VIA ADENOCROMO    paymentTime=${paymentTime}    currency=EUR    merchantId=25267395    isBooking=${false}    skipPlafondCheck=${false}    pointOfService=52    latitude=33    longitude=12    plateNumber=AA000ZZ    plateCountry=IT    agreementId=1    SCAFlag=N    mainAuthorizationId=${null}    mainTransactionId=${transactionID}    installmentNumber=${12}    installmentTotalCount=${12}    transactionList=${transactionList}

    ${response}=    POST on session    mysession   ${endpoint}    json=${body_req}    headers=${headers}
    Should Be Equal As Strings    ${response.status_code}   200
    ${res_body}            convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['errorCode']}     ${null}
    Should Be Equal As Strings    ${res_body['errorMessage']}     ${null}
    [Return]    ${response.content}






