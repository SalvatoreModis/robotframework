import jwt
import datetime
##coded string è la chiave privata che in origine non era formattata con ----begin RSA ...
#aggiungere la stringa iniziale e finale alla chiave privata
#non serve trasformarla, si può passare direttamente come key quando si fa l'encoding del token
def tokenTpayX(act_tkn,c_tkn):
    stringPrivateKey = "-----BEGIN RSA PRIVATE KEY-----\n MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsDvYPNoCRTKzOKl6csjaW0UJvfsrREYzlATR7z3nJx09MGW5iI4QMB1BvyUUZwoDLQt5O31/KoFeMekvbZB6R4CSaF1qGxpg8d/FWD+MObzeAyZZGNvseRtcrXJiOfGvcDl1iLgfqg2FcGPmTx7pSWhLuLYC6C4A0fLR+6rFbDWcyau5KrcgIQwYFsAEfv12DXaQUDslycam24HwImvznI5MJAFXLVBbO0zdMwV8Y/syxnEytmLvg0NzqNWxx1iFVq2Q/eRW6FeTRzS9V+qjyZ1hrz+22QjWU05+yEmYsl2nZQrxsm9WAxg69QcA/fOlQ9T7zvdSt/fu/Tf3MhLF7AgMBAAECggEAZPti7wrN6nlV8qprXQw7fcjTdRy1bH8gckeCxie/Eu4nXEKQxoiRt32TeAhoxfWiIandT+1KC2KP/6LHYrwgNP2Yfde3CeME9nhu7FPmr1IXAT9PM2aRe/OX1ZtC7l2X8E99/2GEw97YBXWjBVjzweGbuq56HrchR6zpQpP8A7OjFHs9PcrbUvtk2LNT/IfGQwrKkcbNAXxxkpdS96xzZrJ7OzGDCU6f1YMBwGerFbM17RKiJQ6TbWYSerPDDgjJeGk5V+rSaDITrzlzkXdmafWOUlzAvvk99IKZtbqWxXzauYdIrNs53+YIIBCWSdynA/OyxhU+CPgk8CRx/v6KAQKBgQDiD+7vKHQyt3DZ+C1TS4LPRSixM1yib0I0veb2PAYei1oK4VceBSuk+SoAhqFW/coeERBinc+yCdH6zvUlDIMcfoLLRgAFboBLaPdf1JBE6AGkyKmV0Tv+9aOKJdlqCZZivprgb80jr+Fo37rH10/MeRZoYr5LzZSArjDEWBHMNwKBgQDC2CqFWz/OMgA609hsw92Md5j1PkpwpdaEogUzEFAAu80jwEgcjBw+erNINydo0OM/PNDRaVaBj3ppV7IGNmPmlu18AHk1OhceFhy2ujUzQm/53hIyOdRlmTiCN7+BCOPZKjCTp2CKPAWbLQREDRu/xIZNBhU7JsKgSG0TvBzK3QKBgAe9i+zccb4a0d5kxQiJGV2HfLE0lQNq09WCexoLTBpXoymkNZttieUIMwv5kCSuY/FLnEgEJ0xtUadI1tQB7atYGZpkNzP4aKQsX+wEFzyExSmjRUs/8tH0Dzl3uk4LLf8Lj7oHmfqS1yEEKB0Xnj/eFYpVdzaZP3B4hmryqQlZAoGAS6n6STM4QfB02mNuBj7BiEzhX0tDSR99gKASeNP63W4QqbN/zWYziafIe2C/sxp7RRFu3gOUDlgTZ1kgzvzMKr2tl5vvddYLKCnaGLOS+UbxiQzN8Cc47gNkSF0xxwAWyzxxBMXY3ZWVb785itu0TPIaXUrhb8kLh4bYpPNGExkCgYAnBy0ITM/i9hRlZqAVhPDRSSXZLprL6kRB7DQx/DAYSAiipVBwRdHLrNbbMcOsUbF84TmY2FU1HjWWG8KmtbNgbnI2hPSz3KCY9PAUxXmMUbaiXPtAU5vwP1Mj7kf+KjLmKT0N4ppaMRVq3+nu7Gm0OzkZr6h8J76AWCD/DwytuQ==\n-----END RSA PRIVATE KEY-----"
#costruisco payload
    payload_data= {
    "sub": act_tkn,
    "tp_id": c_tkn,
    "exp": datetime.datetime.utcnow() + datetime.timedelta(days=30, seconds=0),
    "iat": datetime.datetime.utcnow(),
    "aud": "TPEVO",
    "lvl": 3,
    "binduser_done": True,
    "iss": "https://opentech.com"
    }

    token= jwt.encode(
    payload=payload_data,
    key=stringPrivateKey,
    algorithm='RS256')
    return token
