*** Settings ***
Resource       Resources/base_url.robot
Resource       Resources/libraries.robot
Resource       KAE.robot
Resource       checkDB.robot


*** Variables ***
@{c_obu}          6000000019
@{c_ser_aut}        12
@{c_ser_aut_neg}    23
@{c_ser_pre}        18    23    56    64

${sleep_time}    2s

*** Test Cases ***
TC0
    [Tags]    testKAE
    FOR    ${c_sup}    IN    @{c_obu}
     Set Suite Variable    ${c_sup}    ${c_sup}
     ContrattoETitolo
        FOR    ${c_ser}             IN    @{c_ser_aut}
            SetupImportoPositivo
            CicloAutSecca    ${c_sup}    ${c_ser}

            Set Suite Variable    ${amount}    ${0}
            Set Suite Variable    ${amount_new}    ${0.01}
            CicloAutSecca    ${c_sup}    ${c_ser}

            Set Suite Variable    ${amount}    ${0.01}
            Set Suite Variable    ${amount_new}    ${0}
            CicloAutSecca    ${c_sup}    ${c_ser}
        END
        FOR    ${c_ser}             IN    @{c_ser_aut_neg}
            SetupImportoNegativo
            CicloAutSecca    ${c_sup}    ${c_ser}
        END
        FOR    ${c_ser}             IN    @{c_ser_pre}
            SetupImportoPrenotazione    ${c_ser}
            CicloPrenotazione        ${c_sup}    ${c_ser}
        END
    SetupImportoPrenotazione    56
    CicloSkipass        ${c_sup}    56

    CicloATM      ${c_sup}    56
    END



*** Keywords ***
CicloAutSecca
    [Arguments]    ${c_sup}    ${c_ser}
    Set Suite Variable    ${c_sup}     ${c_sup}
    Set Suite Variable    ${c_ser}          ${c_ser}
    ## setup controlli a DB ##
    ContrattoETitolo

    #################### Autorizzazione  ####################
    ${res_body_I}     Autorizzazione Secca      ${amount}

    #dati transazione
    ${res_body_I}   convert string to json    ${res_body_I}
    Set Suite Variable    ${transactionID}       ${res_body_I['transactionId']}
    Set Suite Variable    ${d_aut}    ${res_body_I['authorizationId']}

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     autorizzazione
    #controllo CPDB46
     ControlloDB46     autorizzazione
    ControlloRiskShield_BB    ${amount}   autorizzazione

    #################### Modifica autorizzazione  ####################
    ${res_body_M}     ModificaAutorizzazione    ${amount_new}

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     modifica
    #controllo CPDB46
     ControlloDB46     modifica
    ControlloRiskShield_BB    ${amount_new}   modifica

    #################### Storno  ####################
    ${res_body_S}     ottieniStorno

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     storno
    #controllo CPDB46
     ControlloDB46     storno
    ControlloRiskShield_BB    ${amount_new}   storno

CicloPrenotazione
    [Arguments]    ${c_sup}    ${c_ser}
    Set Suite Variable    ${c_sup}     ${c_sup}
    Set Suite Variable    ${c_ser}          ${c_ser}
     ## setup controlli a DB ##
    ContrattoETitolo

    #################### Prenotazione ####################
    ${res_body_P}     Prenotazione      ${amount}

    #dati transazione
    ${res_body_P}   convert string to json    ${res_body_P}
    Set Suite Variable    ${transactionID}       ${res_body_P['transactionId']}
    Set Suite Variable    ${d_aut}    ${res_body_P['authorizationId']}

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     prenotazione
    ## controllo CPDB46
     ControlloDB46     prenotazione
    ControlloRiskShield_BB    ${amount}   prenotazione

    #################### Conferma ####################
    ${res_body_C}          ConfermaPrenotazione    ${amount_new}

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     conferma
    ## controllo CPDB46
     ControlloDB46     conferma
    ControlloRiskShield_BB    ${amount_new}   conferma

    #################### Storno ####################
    ${res_body_R}       OttieniStorno

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     storno
    ## controllo CPDB46
     ControlloDB46     storno
    ControlloRiskShield_BB    ${amount_new}   storno

CicloSkipass
    [Arguments]    ${c_sup}    ${c_ser}
    Set Suite Variable    ${c_sup}     ${c_sup}
    Set Suite Variable    ${c_ser}          ${c_ser}
    ## setup controlli a DB ##
    ContrattoETitolo

    #################### Prenotazione ####################
    ${res_body_P}     Prenotazione      ${amount}

    #dati transazione
    ${res_body_P}   convert string to json    ${res_body_P}
    Set Suite Variable    ${transactionID}       ${res_body_P['transactionId']}
    Set Suite Variable    ${d_aut}       ${res_body_P['authorizationId']}

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     prenotazione
    ## controllo CPDB46
    ControlloDB46     prenotazione
    ControlloRiskShield_BB    ${amount}   prenotazione

    #################### Modifica Prenotazione  ####################
    ${res_body_M}          ModificaAutorizzazione     ${amount_new}
    #dati nuova transazione
    ${res_body_M}   convert string to json    ${res_body_M}
    Set Suite Variable    ${transactionID_new}    ${res_body_M['transactionId']}
    Set Suite Variable    ${d_aut_new}            ${res_body_M['authorizationId']}

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     modificaPrenotazione
    ## controllo CPDB46
    ControlloDB46     modificaPrenotazione
    ControlloRiskShield_BB    ${amount_new}   modificaPrenotazione

    #################### Conferma Multipla  ####################
    ${res_body_C}       ConfermaMultipla    ${amount_new}
    #dati lista nuove transazioni
    ${res_bodyC}   convert string to json    ${res_bodyC}
    CreaListeTrxDaResponse    ${res_bodyC}    transactionList    transactionId    authorizationId

    Sleep    ${sleep_time}
    ## controllo vkbb01
    ControlloVKBB01     confermaMultipla
    ## controllo CPDB46
    ControlloDB46     confermaMultipla
    ControlloRiskShield_BB    ${amount_new}   confermaMultipla


CicloATM
    [Arguments]    ${c_sup}    ${c_ser}
    Set Suite Variable    ${c_sup}     ${c_sup}
    Set Suite Variable    ${c_ser}     ${c_ser}
     ## setup controlli a DB ##
    ContrattoETitolo

    #################### autorizzazione abbonamento ####################
    ${res_body}     ottieniAutRateizzazione

    #dati transazione principale
    ${res_body}   convert string to json    ${res_body}
    Set Suite Variable    ${transactionID}    ${res_body['transactionId']}
    Set Suite Variable    ${d_aut}            ${res_body['authorizationId']}
    #dati lista nuove transazioni
    CreaListeTrxDaResponse    ${res_body}    transactionList    transactionId    authorizationId

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     abbonamento
    #controllo CPDB46
    ControlloDB46     abbonamento
    ControlloRiskShield_BB    ${amount}   abbonamento

    #################### storno abbonamento ####################
    ${res_body}     OttieniStorno

    Sleep    ${sleep_time}
    #controllo vkbb01
    ControlloVKBB01     stornoAbbonamento
    #controllo CPDB46
     ControlloDB46     stornoAbbonamento
    ControlloRiskShield_BB    ${amount}   stornoAbbonamento
