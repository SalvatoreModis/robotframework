*** Settings ***
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/Body.txt
Resource          ../KEYWORDS/checkDB.robot
Library           runbatchKBB.py


*** Variables ***
${obuID}        1000115129    #1000096139
${username}     3455867284    #1617651369    #3248971259

${amount}      ${500}    #in centesimi
${amountE}      ${5}    #in euro

${sleepTime}    1s
${c_ser}        28
${transactionID}    ${EMPTY}
${DAUT_str}    ${EMPTY}

${app}     KBB
${host}    ${EMPTY}


*** Test Cases ***
0- LOGIN TPAYX
    ${output}    PostRequestLoginX    ${endpoint}    ${basePath_loginX}    ${username}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

1- TURBORICARICA
    ContrattoETitolo    ${obuID}
    SetupSogliaTR

    ${turbo_json}    evaluate    json.loads('''${body_turboricarica}''')    json
    Set to Dictionary    ${turbo_json}    amount=${amount}
    ${output}    PostRequest    ${endpoint}    ${basepath_turboricarica}    ${turbo_json}    ${access_token}    41.852461    12.4669735    TPAYX
    ${trx}    Set Variable    ${output.json()['id']}
    Set Suite Variable    ${transactionID}    ${trx}

    Sleep    ${sleepTime}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30' and C_SUP='${obuID}'    1
    Disconnect from Database
    ControlloVKBB01     autorizzazione    ${c_ser}    ${DAUT_str}    ${transactionID}    ${obuID}
    CheckSogliaTR    ${amountE}
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB13_GUI_TRB_RIC WHERE D_INS_MOV IN (SELECT D_INS_MOV FROM KSBA.TKSB01_MOV WHERE C_IDE_TXN ='${transactionID}') AND C_STA_ELA='Y'    1
    Disconnect from Database

2- BATCH KBBC2J01
    ${host}  runbatchKBB.macchina  ${app}
    set suite variable    ${host}    ${host}
    Esecuzione batch  ${app}  kbbc2j01

    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB13_GUI_TRB_RIC WHERE D_INS_MOV IN (SELECT D_INS_MOV FROM KSBA.TKSB01_MOV WHERE C_IDE_TXN ='${transactionID}') AND C_STA_ELA='Z'    1
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB11_TRB_RIC WHERE ID_TRX ='${transactionID}' AND C_CTR ='${c_ctr}' AND I_IMP=${amountE} AND C_STA ='OK' AND C_CAU ='01' AND D_ELA_RIP_NOR IS NULL     1
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB12_SCA_RIP_PLF WHERE ID_TRX ='${transactionID}' AND C_STA_ELA ='Y'    1
    Disconnect from Database

3- INVIO ESITO SDD - 1 - storno ripresentabile
    # esito SDD A 03
    Login server    ${c_ser}
    Esito SDD    ${transactionID}    A    03    ${amountE}    ${0}

    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB14_ESI_SDD_TRB WHERE ID_TRX ='${transactionID}' AND C_OPE ='A' AND C_TIP_OPE ='03' AND C_CTR ='${c_ctr}' AND C_STA_ELA ='Y '    1
    Disconnect from Database

4- BATCH KBBC2J03 - 1
    Esecuzione batch  ${app}  kbbc2j03

    CheckSogliaTR    ${amountE}
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB14_ESI_SDD_TRB WHERE ID_TRX ='${transactionID}' AND C_OPE ='A' AND C_TIP_OPE ='03' AND C_CTR ='${c_ctr}' AND C_STA_ELA ='Z '    1
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB11_TRB_RIC WHERE ID_TRX ='${transactionID}' AND C_CTR ='${c_ctr}' AND I_IMP=${amountE} AND C_STA ='KO' AND C_CAU ='02' AND D_ELA_RIP_NOR IS NULL     1
    Row Count Is Equal To X    SELECT * FROM KBBA.VKBB01_MOV WHERE C_SER ='${c_ser}' AND C_IDE_TXN ='${transactionID}' AND F_ADD_ACC ='A' AND I_MOV IN (${amountE},-${amountE}) AND I_MOV_ORG =I_MOV AND C_SUP ='${obuID}' AND C_CTR ='${c_ctr}'    1
    Row Count Is 0    SELECT * FROM KBBA.TKBB12_SCA_RIP_PLF WHERE ID_TRX ='${transactionID}' AND C_STA_ELA ='Y'
    Disconnect from Database
    # Controllo servizio disattivato
    dbConnection    CPDB06
    Row Count Is Equal To X    SELECT * FROM EVDA.TETSRY_ADE_SER WHERE C_SER ='${c_ser}' AND C_SOG_ADE ='${c_ctr}' AND D_INI_VAL_SER > CURRENT date AND D_FIN_VAL_SER > CURRENT timestamp AND F_AUT ='N'     1
    Disconnect From Database

5- BATCH KBBC2J02
    Esecuzione batch  ${app}  kbbc2j02

    # non è cambiato nulla
    CheckSogliaTR    ${amountE}
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB14_ESI_SDD_TRB WHERE ID_TRX ='${transactionID}' AND C_OPE ='A' AND C_TIP_OPE ='03' AND C_CTR ='${c_ctr}' AND C_STA_ELA ='Z '    1
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB11_TRB_RIC WHERE ID_TRX ='${transactionID}' AND C_CTR ='${c_ctr}' AND I_IMP=${amountE} AND C_STA ='KO' AND C_CAU ='02' AND D_ELA_RIP_NOR IS NULL     1
    Row Count Is Equal To X    SELECT * FROM KBBA.VKBB01_MOV WHERE C_SER ='${c_ser}' AND C_IDE_TXN ='${transactionID}' AND F_ADD_ACC ='A' AND I_MOV IN (${amountE},-${amountE}) AND I_MOV_ORG =I_MOV AND C_SUP ='${obuID}' AND C_CTR ='${c_ctr}'    1
    Row Count Is 0    SELECT * FROM KBBA.TKBB12_SCA_RIP_PLF WHERE ID_TRX ='${transactionID}' AND C_STA_ELA ='Y'
    Disconnect from Database
    # Controllo servizio disattivato
    dbConnection    CPDB06
    Row Count Is Equal To X    SELECT * FROM EVDA.TETSRY_ADE_SER WHERE C_SER ='${c_ser}' AND C_SOG_ADE ='${c_ctr}' AND D_INI_VAL_SER > CURRENT date AND D_FIN_VAL_SER > CURRENT timestamp AND F_AUT ='N'     1
    Disconnect From Database

6- INVIO ESITO SDD- 2 - storno non ripresentabile
    # esito SDD B 03
    Login server    ${c_ser}
    Esito SDD    ${transactionID}    B    03    ${amountE}    ${0}

    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB14_ESI_SDD_TRB WHERE ID_TRX ='${transactionID}' AND C_OPE ='B' AND C_TIP_OPE ='03' AND C_CTR ='${c_ctr}' AND C_STA_ELA ='Y '    1
    Disconnect from Database

7- BATCH KBBC2J03 - 2
    Esecuzione batch  ${app}  kbbc2j03

    CheckSogliaTR    ${amountE}
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB14_ESI_SDD_TRB WHERE ID_TRX ='${transactionID}' AND C_OPE ='B' AND C_TIP_OPE ='03' AND C_CTR ='${c_ctr}' AND C_STA_ELA ='Z '    1
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB11_TRB_RIC WHERE ID_TRX ='${transactionID}' AND C_CTR ='${c_ctr}' AND I_IMP=${amountE} AND C_STA ='KO' AND C_CAU ='03' AND D_ELA_RIP_NOR IS NULL     1
    Row Count Is Equal To X    SELECT * FROM KBBA.VKBB01_MOV WHERE C_SER ='${c_ser}' AND C_IDE_TXN ='${transactionID}' AND F_ADD_ACC ='A' AND I_MOV IN (${amountE},-${amountE}) AND I_MOV_ORG =I_MOV AND C_SUP ='${obuID}' AND C_CTR ='${c_ctr}'    1
    Disconnect from Database
    # Controllo servizio disattivato
    dbConnection    CPDB06
    Row Count Is Equal To X    SELECT * FROM EVDA.TETSRY_ADE_SER WHERE C_SER ='${c_ser}' AND C_SOG_ADE ='${c_ctr}' AND D_INI_VAL_SER > CURRENT date AND D_FIN_VAL_SER > CURRENT timestamp AND F_AUT ='N'     1
    Disconnect From Database

8- INVIO RIABILITA TURBORICARICA
    Login server    ${c_ser}
    Forzatura Turboricarica

    # esito SDD D 03
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB14_ESI_SDD_TRB WHERE ID_TRX ='${transactionID}' AND C_OPE ='D' AND C_TIP_OPE ='03' AND C_CTR ='${c_ctr}' AND C_STA_ELA ='Y '    1
    Disconnect from Database

9- BATCH KBBC2J03 - 3
    Esecuzione batch  ${app}  kbbc2j03

    CheckSogliaTR    ${0}
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB14_ESI_SDD_TRB WHERE ID_TRX ='${transactionID}' AND C_OPE ='D' AND C_TIP_OPE ='03' AND C_CTR ='${c_ctr}' AND C_STA_ELA ='Z '    1
    Row Count Is Equal To X    SELECT * FROM KBBA.TKBB11_TRB_RIC WHERE ID_TRX ='${transactionID}' AND C_CTR ='${c_ctr}' AND I_IMP=${amountE} AND C_STA ='OK' AND C_CAU ='08' AND D_ELA_RIP_NOR IS NULL     1
    Row Count Is Equal To X    SELECT * FROM KBBA.VKBB01_MOV WHERE C_TIP_OPE_PAG ='C ' AND C_SER ='${c_ser}' AND C_IDE_TXN ='${transactionID}' AND F_ADD_ACC ='A' AND I_MOV =-${amountE} AND I_MOV_ORG =I_MOV AND C_SUP ='${obuID}' AND C_CTR ='${c_ctr}'    1
    Disconnect from Database

    # controllo servizio di nuovo attivo
    dbConnection    CPDB06
    Row Count Is 0    SELECT * FROM EVDA.TETSRY_ADE_SER WHERE C_SER ='${c_ser}' AND C_SOG_ADE ='${c_ctr}' AND D_INI_VAL_SER > CURRENT date AND D_FIN_VAL_SER > CURRENT timestamp AND F_AUT ='N'
    Disconnect From Database




*** Keywords ***
Esecuzione batch
    [Arguments]  ${applicativo}  ${batch}
    ${esito}=  runbatchKBB.batchrun  ${app}  ${batch}
    SHOULD BE EQUAL AS STRINGS  ${esito}  fine OK


Esito SDD
    [Arguments]    ${transactionID}    ${operazione}    ${tipoOperazione}    ${importo}    ${importoParziale}    ${c_ctr}=${c_ctr}
    Setup Iban    ${c_ctr}
    ${data}      get current date    result_format=%Y-%m-%d-%H.%M.%S.%f
    &{body_json}    Create Dictionary    idTransazione=${transactionID}    ibanCliente=${iban}    dataOperazioneOriginale=${d_aut}    dataOperazione=${data}    operazione=${operazione}    tipoOperazione=${tipoOperazione}    importo=${importo}    divisa=EUR    importoParziale=${importoParziale}
    &{headers}    create dictionary    Authorization=Bearer ${token_server}    Content-Type=application/json
    Create Session    mysession    ${endpoint_esitoSDD}
    ${response}=    POST On Session    mysession    ${basepath_esitoSDD}    json=${body_json}    headers=${headers}
    Should Be True    """${response.status_code}""" in """200201202"""
    Should Be True    """${response.status_code}""" in """200201"""
    [Return]    ${response}

Login server
    [Arguments]    ${c_ser}
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    ${c_ser}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${token_server}    ${token}

Setup Iban
    [Arguments]    ${c_ctr}=${c_ctr}
    dbConnection    CPDB06
    ${info}    Query    SELECT * FROM KSAA.TKSASF_CTR_PAY WHERE C_CTR ='${c_ctr}' AND D_FIN_VAL > CURRENT date     false    true
    Disconnect From Database
    ${iban}    Get From Dictionary    ${info}[0]    C_IBAN
    set suite variable    ${iban}    ${iban}



Forzatura Turboricarica
    dbConnection    CPDB06
    ${info}    Query    select * from EVDA.TETSJU_ATT_PNG where C_OBU_ID = ${obuID}    false    true
    Disconnect From Database
    ${c_old_ute}    Get From Dictionary    ${info}[0]    C_OLD_UTE
    ${c_old_ute}   convert to string    ${c_old_ute}
    &{body_json}    Create Dictionary    c_old_ute=${c_old_ute}
    &{headers}    create dictionary    Authorization=Bearer ${token_server}    Content-Type=application/json
    Create Session    mysession    ${endpoint_esitoSDD}
    ${response}=    POST On Session    mysession    ${basepath_riabilita}    json=${body_json}    headers=${headers}
    Should Be True    """${response.status_code}""" in """200201202"""
    Should Be True    """${response.status_code}""" in """200201"""
    [Return]    ${response}
