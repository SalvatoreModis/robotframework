*** Settings ***
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../DATA/Body.txt
Resource          ../KEYWORDS/checkDB.robot
Library           modifyFile.py
Library           convertxml.py
Library           modifyJson.py
Library           modifyJson_zero.py

*** Variables ***
${username}       bergac112
${password}       bergac112
${access_token}    ${EMPTY}
${tokenCheckinIP}    ${EMPTY}
${transactionID}    ${EMPTY}
${ctrID_str1}     ${EMPTY}
${C_SER}          18
${DAUT_str}       ${EMPTY}
${amount}         ${1329}

*** Test Cases ***
LOGIN
    Set Suite Variable    ${Product}    TPAY
    Set Suite Variable    ${obuID}    1000125615
    CHECKUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLogin    ${endpoint}    ${basePath_login}    ${username}    ${password}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

CHECKIN_IP
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_IP}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinIP}    ${checkin_json}    ${access_token}    41.255646465    9.984656    ${Product}
    ${tokenIP}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinIP}    ${tokenIP}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenIP}' and C_OBU_ID='${obuID}'    1
    Disconnect from Database

ORDER_IP
    ${order_json}    evaluate    json.loads('''${body_Order_IP}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinIP}
    Set to Dictionary    ${order_json}    amount=${amount}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderIP}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    #########

CALLBACK_IP
    dbConnection    CPDB32
    ${DautCode}    Query    SELECT D_AUT,C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${Daut}    Get From Dictionary    ${DautCode}[0]    D_AUT
    ${Daut}    Convert to String    ${Daut}
    ${Daut}    Replace String    ${Daut}    ${SPACE}    -
    ${Daut}    Replace String    ${Daut}    :    .
    ${body_Callback_IP}    Modify Json    ${tokenCheckinIP}    ${Daut}
    Create Session    mysession    ${endpoint_callbackIP}
    ${response}=    POST On Session    mysession    ${basePath_callbackIP}    json=${body_Callback_IP}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    100    conferma
    #########

CALLBACK_IP_ZERO
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_IP}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinIP}    ${checkin_json}    ${access_token}    41.255646465    9.984656    ${Product}
    ${tokenIP}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinIP}    ${tokenIP}
    ${order_json}    evaluate    json.loads('''${body_Order_IP}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinIP}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderIP}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    dbConnection    CPDB32
    ${DautCode}    Query    SELECT D_AUT,C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${Daut}    Get From Dictionary    ${DautCode}[0]    D_AUT
    ${Daut}    Convert to String    ${Daut}
    ${Daut}    Replace String    ${Daut}    ${SPACE}    -
    ${Daut}    Replace String    ${Daut}    :    .
    ${body_Callback_IP}    modifyJson_zero.Modify JsonZero    ${tokenCheckinIP}    ${Daut}
    Create Session    mysession    ${endpoint_callbackIP}
    ${response}=    POST On Session    mysession    ${basePath_callbackIP}    json=${body_Callback_IP}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97'    1
    Disconnect from Database

LOGINX
    Set Suite Variable    ${Product}    TPAYX
    Set Suite Variable    ${obuID}    1000096139
    Set Suite Variable    ${username}    3248971259
    CHECKUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLoginX    ${endpoint}    ${basePath_loginX}    ${username}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

CHECKIN_IPX
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_IP}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinIPX}    ${checkin_json}    ${access_token}    41.255646465    9.984656    ${Product}
    ${tokenIP}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinIP}    ${tokenIP}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenIP}'    1
    Disconnect from Database

ORDER_IPX
    ${order_json}    evaluate    json.loads('''${body_Order_IP}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinIP}
    Set to Dictionary    ${order_json}    amount=${amount}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderIPX}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    #########

CALLBACK_IPX
    dbConnection    CPDB32
    ${DautCode}    Query    SELECT D_AUT,C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${Daut}    Get From Dictionary    ${DautCode}[0]    D_AUT
    ${Daut}    Convert to String    ${Daut}
    ${Daut}    Replace String    ${Daut}    ${SPACE}    -
    ${Daut}    Replace String    ${Daut}    :    .
    ${body_Callback_IP}    Modify Json    ${tokenCheckinIP}    ${Daut}
    Create Session    mysession    ${endpoint_callbackIP}
    ${response}=    POST On Session    mysession    ${basePath_callbackIP}    json=${body_Callback_IP}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    100    conferma
    #########

CALLBACK_IPX_ZERO
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_IP}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinIPX}    ${checkin_json}    ${access_token}    41.255646465    9.984656    ${Product}
    ${tokenIP}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinIP}    ${tokenIP}
    ${order_json}    evaluate    json.loads('''${body_Order_IP}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinIP}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderIPX}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    dbConnection    CPDB32
    ${DautCode}    Query    SELECT D_AUT,C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${Daut}    Get From Dictionary    ${DautCode}[0]    D_AUT
    ${Daut}    Convert to String    ${Daut}
    ${Daut}    Replace String    ${Daut}    ${SPACE}    -
    ${Daut}    Replace String    ${Daut}    :    .
    ${body_Callback_IP}    modifyJson_zero.Modify JsonZero    ${tokenCheckinIP}    ${Daut}
    Create Session    mysession    ${endpoint_callbackIP}
    ${response}=    POST On Session    mysession    ${basePath_callbackIP}    json=${body_Callback_IP}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97'    1
    Disconnect from Database
