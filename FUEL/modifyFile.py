def Replace_line_in_file(file,searchExp1,replaceLine):
    """ Open a file (like input.txt) and find the line that
        contains the string searchExp1.
        and replace that complete line by replaceLine.
        When there are multiple lines that contain searchExp1
        then all those lines will be replaced
    """
    for line in fileinput.input(file, inplace=1):
        if searchExp1 in line:
            line = replaceLine+'\n'
        sys.stdout.write(line)