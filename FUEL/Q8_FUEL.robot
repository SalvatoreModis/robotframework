*** Settings ***
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../DATA/Body.txt
Resource          ../KEYWORDS/checkDB.robot
Library           modifyFile.py
Library           convertxml.py

*** Variables ***
${username}       dirfptst_103
${password}       dirfptst_103
${access_token}    ${EMPTY}
${transactionID}    ${EMPTY}
${ctrID_str1}     ${EMPTY}
${C_SER}          18
${DAUT_str}       ${EMPTY}
${amount}         ${1329}

*** Test Cases ***
LOGIN
    Set Suite Variable    ${Product}    TPAY
    Set Suite Variable    ${obuID}    1000119691
    CHECKUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLogin    ${endpoint}    ${basePath_login}    ${username}    ${password}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

ORDER_Q8
    dbConnection    CPDB28
    ${COOR}    Query    SELECT N_LAT,N_LON FROM KCCA.TKCC09_PUN_ERO_SER WHERE C_PUN_ERO_SER='628'    false    true
    ${LAT}    Get From Dictionary    ${COOR}[0]    N_LAT
    ${LON}    Get From Dictionary    ${COOR}[0]    N_LON
    ${checkin_json}    evaluate    json.loads('''${body_Order_Q8}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_orderQ8}    ${checkin_json}    ${access_token}    ${LAT}    ${LON}    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    100    prenotazione
    #########

CALLBACK_Q8
    dbConnection    CPDB32
    ${DautCode}    Query    SELECT D_AUT,C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${Daut}    Get From Dictionary    ${DautCode}[0]    D_AUT
    ${Daut}    Convert to String    ${Daut}
    ${Daut}    Replace String    ${Daut}    ${SPACE}    -
    ${Daut}    Replace String    ${Daut}    :    .
    ${Code}    Get From Dictionary    ${DautCode}[0]    C_IDE_TRX_ESE
    ${callback_json}    evaluate    json.loads('''${body_Callback_Q8}''')    json
    Set to Dictionary    ${callback_json}    AuthPartnerCode    ${Daut}
    Set to Dictionary    ${callback_json}    PayCode    ${Code}
    Create Session    mysession    ${endpoint_callbackQ8}
    ${response}=    POST On Session    mysession    ${basePath_callbackQ8}    json=${callback_json}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    100    conferma
    #########

CALLBACK_Q8_ZERO
    dbConnection    CPDB28
    ${COOR}    Query    SELECT N_LAT,N_LON FROM KCCA.TKCC09_PUN_ERO_SER WHERE C_PUN_ERO_SER='628'    false    true
    ${LAT}    Get From Dictionary    ${COOR}[0]    N_LAT
    ${LON}    Get From Dictionary    ${COOR}[0]    N_LON
    ${checkin_json}    evaluate    json.loads('''${body_Order_Q8}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_orderQ8}    ${checkin_json}    ${access_token}    ${LAT}    ${LON}    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    dbConnection    CPDB32
    ${DautCode}    Query    SELECT D_AUT,C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${Daut}    Get From Dictionary    ${DautCode}[0]    D_AUT
    ${Daut}    Convert to String    ${Daut}
    ${Daut}    Replace String    ${Daut}    ${SPACE}    -
    ${Daut}    Replace String    ${Daut}    :    .
    ${Code}    Get From Dictionary    ${DautCode}[0]    C_IDE_TRX_ESE
    ${callback_json}    evaluate    json.loads('''${body_Callback_Q8_ZERO}''')    json
    Set to Dictionary    ${callback_json}    AuthPartnerCode    ${Daut}
    Set to Dictionary    ${callback_json}    PayCode    ${Code}
    Create Session    mysession    ${endpoint_callbackQ8}
    ${response}=    POST On Session    mysession    ${basePath_callbackQ8}    json=${callback_json}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97'    1
    Disconnect from Database

LOGINX
    [Tags]    TPAYX
    Set Suite Variable    ${Product}    TPAYX
    Set Suite Variable    ${obuID}    1000096139
    Set Suite Variable    ${username}    3248971259
    CHECKUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLoginX    ${endpoint}    ${basePath_loginX}    ${username}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

ORDER_Q8X
    dbConnection    CPDB28
    ${COOR}    Query    SELECT N_LAT,N_LON FROM KCCA.TKCC09_PUN_ERO_SER WHERE C_PUN_ERO_SER='628'    false    true
    ${LAT}    Get From Dictionary    ${COOR}[0]    N_LAT
    ${LON}    Get From Dictionary    ${COOR}[0]    N_LON
    ${checkin_json}    evaluate    json.loads('''${body_Order_Q8}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_orderQ8X}    ${checkin_json}    ${access_token}    ${LAT}    ${LON}    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    100    prenotazione
    #########

CALLBACK_Q8X
    dbConnection    CPDB32
    ${DautCode}    Query    SELECT D_AUT,C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${Daut}    Get From Dictionary    ${DautCode}[0]    D_AUT
    ${Daut}    Convert to String    ${Daut}
    ${Daut}    Replace String    ${Daut}    ${SPACE}    -
    ${Daut}    Replace String    ${Daut}    :    .
    ${Code}    Get From Dictionary    ${DautCode}[0]    C_IDE_TRX_ESE
    ${callback_json}    evaluate    json.loads('''${body_Callback_Q8}''')    json
    Set to Dictionary    ${callback_json}    AuthPartnerCode    ${Daut}
    Set to Dictionary    ${callback_json}    PayCode    ${Code}
    Create Session    mysession    ${endpoint_callbackQ8}
    ${response}=    POST On Session    mysession    ${basePath_callbackQ8}    json=${callback_json}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    100    conferma
    #########

CALLBACK_Q8X_ZERO
    dbConnection    CPDB28
    ${COOR}    Query    SELECT N_LAT,N_LON FROM KCCA.TKCC09_PUN_ERO_SER WHERE C_PUN_ERO_SER='628'    false    true
    ${LAT}    Get From Dictionary    ${COOR}[0]    N_LAT
    ${LON}    Get From Dictionary    ${COOR}[0]    N_LON
    ${checkin_json}    evaluate    json.loads('''${body_Order_Q8}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_orderQ8X}    ${checkin_json}    ${access_token}    ${LAT}    ${LON}    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    dbConnection    CPDB32
    ${DautCode}    Query    SELECT D_AUT,C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${Daut}    Get From Dictionary    ${DautCode}[0]    D_AUT
    ${Daut}    Convert to String    ${Daut}
    ${Daut}    Replace String    ${Daut}    ${SPACE}    -
    ${Daut}    Replace String    ${Daut}    :    .
    ${Code}    Get From Dictionary    ${DautCode}[0]    C_IDE_TRX_ESE
    ${callback_json}    evaluate    json.loads('''${body_Callback_Q8_ZERO}''')    json
    Set to Dictionary    ${callback_json}    AuthPartnerCode    ${Daut}
    Set to Dictionary    ${callback_json}    PayCode    ${Code}
    Create Session    mysession    ${endpoint_callbackQ8}
    ${response}=    POST On Session    mysession    ${basePath_callbackQ8}    json=${callback_json}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97'    1
    Disconnect from Database
