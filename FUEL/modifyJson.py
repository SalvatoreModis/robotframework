import ast
import json

def modifyJson(token,daut):
    json1 = {    "header": {    "token": "AAAAA"    },    "body": {    "authorizationCode": "AAAA",    "amount": {    "price": 1.00,    "currency": "EUR"    },    "dispensed": "1.2 LTR",    "productName": "gasolio",    "pricePerUnit": {    "price": 1.00,    "currency": "EUR"    }    }}
    json2 = {   "token": token   }
    json3={    "authorizationCode": daut,    "amount": {    "price": 1.00,    "currency": "EUR"    },    "dispensed": "1.2 LTR",    "productName": "gasolio",    "pricePerUnit": {    "price": 1.00,    "currency": "EUR"    }    }
    json1['header'] = json2
    json1['body'] = json3
    json_data = ast.literal_eval(json.dumps(json1))
    return json_data
