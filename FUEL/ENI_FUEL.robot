*** Settings ***
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../DATA/Body.txt
Resource          ../KEYWORDS/checkDB.robot
Library           modifyFile.py
Library           convertxml.py
Library           convertxml_ZERO.py

*** Variables ***
${username}       dirfptst_103
${password}       dirfptst_103
${access_token}    ${EMPTY}
${tokenCheckinENI}    ${EMPTY}
${transactionID}    ${EMPTY}
${ctrID_str1}     ${EMPTY}
${C_SER}          18
${DAUT_str}       ${EMPTY}
${amount}         ${1329}

*** Test Cases ***
LOGIN
    [Tags]    PREPAY    POSTPAY
    Set Suite Variable    ${Product}    TPAY
    Set Suite Variable    ${obuID}    1000119691
    CHECKUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLogin    ${endpoint}    ${basePath_login}    ${username}    ${password}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

CHECKIN_ENI
    [Tags]    PREPAY
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_ENI}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinENI}    ${checkin_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${tokenENI}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinENI}    ${tokenENI}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenENI}' and C_OBU_ID='${obuID}' and C_USR_ID='${username}'    1
    Disconnect from Database

ORDER_ENI
    [Tags]    PREPAY
    ${order_json}    evaluate    json.loads('''${body_Order_ENI}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinENI}
    Set to Dictionary    ${order_json}    amount=${amount}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderENI}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenCheckinENI}' and C_OBU_ID='${obuID}' and C_USR_ID='${username}' and C_TRX='${id_TRX}'    1
    Disconnect from Database
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10' AND C_PRO='FP' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    #########
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10' AND I_TOT=4 AND C_PRO='FP' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database

CALLBACK_ENI
    [Tags]    PREPAY
    CollectingINFO    CPDB06    ${obuID}
    ${ctrID_str1}    Convert to String    ${ctrID_str1}
    convertxml.Converter XML    ${ctrID_str1}    ${transactionID}
    ${data}    Get Binary File    ${CURDIR}${/}RequestENI.xml
    Create Session    mysession    ${endpoint_callbackENI}
    ${response}=    POST On Session    mysession    ${basePath_callbackENI}    headers=${headersCALLBACK}    data=${data}
    Should Contain    ${response.text}    Messaggio ricevuto correttamente
    Should Contain    ${response.text}    MESSAGE_RECEIVED_200
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30' AND I_TOT=1 AND C_PRO='FP' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    100    conferma
    #########
    Sleep    60

CHECKIN_ENI_POSTPAY
    [Tags]    POSTPAY
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_ENI}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinENI}    ${checkin_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${tokenENI}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinENI}    ${tokenENI}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenENI}' and C_OBU_ID='${obuID}' and C_USR_ID='${username}'    1
    Disconnect from Database

GETLASTPAYMENT_ENI
    [Tags]    POSTPAY
    ${body_getLastPayment_json}    evaluate    json.loads('''${body_GetLastPayment_ENI}''')    json
    Set to Dictionary    ${body_getLastPayment_json}    token    ${tokenCheckinENI}
    ${output}    PostRequest    ${endpoint}    ${basePath_getLastPaymentENI}    ${body_getLastPayment_json}    ${access_token}    41.852461    12.4669735    ${Product}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenCheckinENI}' AND F_PST_PAG ='Y'    1
    Disconnect from Database
    Sleep    20

ORDER_POSTPAY_ENI
    [Tags]    POSTPAY
    ${body_PostPayment_json}    evaluate    json.loads('''${body_PostPay_ENI}''')    json
    Set to Dictionary    ${body_PostPayment_json}    token    ${tokenCheckinENI}
    ${output}    PostRequest    ${endpoint}    ${basePath_postPaidOrderENI}    ${body_PostPayment_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='30' AND C_TIP_TRX='I'    1
    Disconnect from Database
    Set Suite Variable    ${DAUT_str}    ${EMPTY}
    ControlloVKBB01    autorizzazione    ${C_SER}    ${DAUT_str}    ${id_TRX}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    1    autorizzazione
    #########

CALLBACK_ENI_ZERO
    [Tags]    PREPAY
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_ENI}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinENI}    ${checkin_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${tokenENI}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinENI}    ${tokenENI}
    ${order_json}    evaluate    json.loads('''${body_Order_ENI}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinENI}
    Set to Dictionary    ${order_json}    pumpId=26
    ${output}    PostRequest    ${endpoint}    ${basePath_orderENI}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10' AND C_PRO='FP' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10' AND I_TOT=4 AND C_PRO='FP' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database
    CollectingINFO    CPDB06    ${obuID}
    ${ctrID_str1}    Convert to String    ${ctrID_str1}
    convertxml_ZERO.Converter XML    ${ctrID_str1}    ${transactionID}
    ${data}    Get Binary File    ${CURDIR}${/}RequestENI_ZERO.xml
    Create Session    mysession    ${endpoint_callbackENI}
    ${response}=    POST On Session    mysession    ${basePath_callbackENI}    headers=${headersCALLBACK}    data=${data}
    Should Contain    ${response.text}    Transazione cancellata
    Should Contain    ${response.text}    MESSAGE_RECEIVED_200
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97' AND I_TOT=4 AND C_PRO='FP' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database

LOGINX
    [Tags]    PREPAYX    POSTPAYX
    Set Suite Variable    ${Product}    TPAYX
    Set Suite Variable    ${obuID}    1000096139
    Set Suite Variable    ${username}    3248971259
    Set Suite Variable    ${c_pro}    None
    CHECKUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLoginX    ${endpoint}    ${basePath_loginX}    ${username}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

CHECKIN_ENIX
    [Tags]    PREPAYX
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_ENI}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinENIX}    ${checkin_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${tokenENI}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinENI}    ${tokenENI}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenENI}' and C_OBU_ID='${obuID}'    1
    Disconnect from Database

ORDER_ENIX
    [Tags]    PREPAYX
    ${order_json}    evaluate    json.loads('''${body_Order_ENI}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinENI}
    Set to Dictionary    ${order_json}    pumpId=10
    Set to Dictionary    ${order_json}    amount=${amount}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderENIX}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenCheckinENI}' and C_OBU_ID='${obuID}' and C_TRX='${id_TRX}'    1
    Disconnect from Database
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10' AND C_PRO='FE' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    #########
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10' AND I_TOT=4 AND C_PRO='FE' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database

CALLBACK_ENIX
    [Tags]    PREPAYX
    CollectingINFO    CPDB06    ${obuID}
    ${ctrID_str1}    Convert to String    ${ctrID_str1}
    convertxml.Converter XML    ${ctrID_str1}    ${transactionID}
    ${data}    Get Binary File    ${CURDIR}${/}RequestENI.xml
    Create Session    mysession    ${endpoint_callbackENI}
    ${response}=    POST On Session    mysession    ${basePath_callbackENI}    headers=${headersCALLBACK}    data=${data}
    Should Contain    ${response.text}    Messaggio ricevuto correttamente
    Should Contain    ${response.text}    MESSAGE_RECEIVED_200
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30' AND I_TOT=1 AND C_PRO='FE' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    100    conferma
    #########
    Sleep    60

CALLBACK_ENIX_ZERO
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_ENI}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinENIX}    ${checkin_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${tokenENI}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinENI}    ${tokenENI}
    ${order_json}    evaluate    json.loads('''${body_Order_ENI}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinENI}
    Set to Dictionary    ${order_json}    pumpId=20
    ${output}    PostRequest    ${endpoint}    ${basePath_orderENIX}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10' AND I_TOT=4 AND C_PRO='FE' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database
    CollectingINFO    CPDB06    ${obuID}
    ${ctrID_str1}    Convert to String    ${ctrID_str1}
    convertxml_ZERO.Converter XML    ${ctrID_str1}    ${transactionID}
    ${data}    Get Binary File    ${CURDIR}${/}RequestENI_ZERO.xml
    Create Session    mysession    ${endpoint_callbackENI}
    ${response}=    POST On Session    mysession    ${basePath_callbackENI}    headers=${headersCALLBACK}    data=${data}
    Should Contain    ${response.text}    Transazione cancellata
    Should Contain    ${response.text}    MESSAGE_RECEIVED_200
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97' AND I_TOT=4 AND C_PRO='FE' AND C_SUP='${obuID}' AND C_TIP_TIT='SM'    1
    Disconnect from Database
    Sleep    60

CHECKIN_ENI_POSTPAYX
    [Tags]    POSTPAYX
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_ENI}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinENIX}    ${checkin_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${tokenENI}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinENI}    ${tokenENI}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenENI}' and C_OBU_ID='${obuID}'    1
    Disconnect from Database

GETLASTPAYMENT_ENIX
    [Tags]    POSTPAYX
    ${body_getLastPayment_json}    evaluate    json.loads('''${body_GetLastPayment_ENI}''')    json
    Set to Dictionary    ${body_getLastPayment_json}    token    ${tokenCheckinENI}
    ${output}    PostRequest    ${endpoint}    ${basePath_getLastPaymentENIX}    ${body_getLastPayment_json}    ${access_token}    41.852461    12.4669735    ${Product}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenCheckinENI}' AND F_PST_PAG ='Y'    1
    Disconnect from Database
    Sleep    10

ORDER_POSTPAY_ENIX
    [Tags]    POSTPAYX
    ${body_PostPayment_json}    evaluate    json.loads('''${body_PostPay_ENI}''')    json
    Set to Dictionary    ${body_PostPayment_json}    token    ${tokenCheckinENI}
    ${output}    PostRequest    ${endpoint}    ${basePath_postPaidOrderENIX}    ${body_PostPayment_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='30' AND C_TIP_TRX='I'    1
    Disconnect from Database
    ControlloVKBB01    autorizzazione    ${C_SER}    ${DAUT_str}    ${id_TRX}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    1    autorizzazione
    #########
