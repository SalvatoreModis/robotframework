*** Settings ***
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../DATA/Body.txt
Resource          ../KEYWORDS/checkDB.robot
Library           modifyFile.py
Library           convertxml.py
Library           convertxml_ZERO.py

*** Variables ***
${username}       bergac112
${password}       bergac112
${access_token}    ${EMPTY}
${tokenCheckinGilbarco}    ${EMPTY}
${transactionID}    ${EMPTY}
${ctrID_str1}     ${EMPTY}
${C_SER}          18
${DAUT_str}       ${EMPTY}
${amount}         ${1329}

*** Test Cases ***
LOGIN
    [Tags]    TPAY
    Set Suite Variable    ${Product}    TPAY
    Set Suite Variable    ${obuID}    1000125615
    CHECKUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLogin    ${endpoint}    ${basePath_login}    ${username}    ${password}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

CHECKIN_GILBARCO
    [Tags]    TPAY
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_Gilbarco}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinGilbarco}    ${checkin_json}    ${access_token}    43.7991754    11.1783982    ${Product}
    ${tokenGilbarco}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinGilbarco}    ${tokenGilbarco}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenGilbarco}'    1
    Disconnect from Database

ORDER_GILBARCO
    [Tags]    TPAY
    ${order_json}    evaluate    json.loads('''${body_Order_Gilbarco}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinGilbarco}
    Set to Dictionary    ${order_json}    amount=${amount}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderGilbarco}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    #########

CALLBACK_GILBARCO
    [Tags]    TPAY
    dbConnection    CPDB32
    ${TRXESE}    Query    SELECT C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${TRXESE_var}    Get From Dictionary    ${TRXESE}[0]    C_IDE_TRX_ESE
    ${callback_json}    evaluate    json.loads('''${body_Callback_Gilbarco}''')    json
    Set to Dictionary    ${callback_json}    correlationId=${TRXESE_var}
    Create Session    mysession    ${endpoint_callbackGilbarco}
    ${response}=    POST On Session    mysession    ${basePath_callbackGilbarco}    json=${callback_json}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30' and C_SUP=${obuID}    1
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30' and I_TOT=0.10 and C_SUP=${obuID}    1
    Disconnect from Database
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH07_LOG_MER_Q8 WHERE date(D_INS)= date(CURRENT timestamp) AND T_PAY_IN LIKE '%${TRXESE_var}%'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    10    conferma
    #########

CALLBACK_GILBARCO_ZERO
    [Tags]    TPAY
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_Gilbarco}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinGilbarco}    ${checkin_json}    ${access_token}    43.7991754    11.1783982    ${Product}
    ${tokenGilbarco}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinGilbarco}    ${tokenGilbarco}
    ${order_json}    evaluate    json.loads('''${body_Order_Gilbarco}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinGilbarco}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderGilbarco}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    dbConnection    CPDB32
    ${TRXESE}    Query    SELECT C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${TRXESE_var}    Get From Dictionary    ${TRXESE}[0]    C_IDE_TRX_ESE
    ${callback_json}    evaluate    json.loads('''${body_Callback_Gilbarco_ZERO}''')    json
    Set to Dictionary    ${callback_json}    correlationId=${TRXESE_var}
    Create Session    mysession    ${endpoint_callbackGilbarco}
    ${response}=    POST On Session    mysession    ${basePath_callbackGilbarco}    json=${callback_json}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97' and I_TOT=0.10 and C_SUP=${obuID}    1
    Disconnect from Database
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH07_LOG_MER_Q8 WHERE date(D_INS)= date(CURRENT timestamp) AND T_PAY_IN LIKE '%${TRXESE_var}%'    1
    Disconnect from Database

LOGINX
    [Tags]    TPAYX
    Set Suite Variable    ${Product}    TPAYX
    Set Suite Variable    ${obuID}    1000096139
    Set Suite Variable    ${username}    3248971259
    CHECKUSER    ${obuID}
    SetupSoglieServizio    ${C_SER}    ${obuID}
    ${output}    PostRequestLoginX    ${endpoint}    ${basePath_loginX}    ${username}
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

CHECKIN_GILBARCOX
    [Tags]    TPAYX
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_Gilbarco}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinGilbarcoX}    ${checkin_json}    ${access_token}    43.7991754    11.1783982    ${Product}
    ${tokenGilbarco}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinGilbarco}    ${tokenGilbarco}
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH12_TOK_API_IP WHERE C_TOK= '${tokenGilbarco}'    1
    Disconnect from Database

ORDER_GILBARCOX
    [Tags]    TPAYX
    ${order_json}    evaluate    json.loads('''${body_Order_Gilbarco}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinGilbarco}
    Set to Dictionary    ${order_json}    amount=${amount}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderGilbarcoX}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    #####    richiamo controllo
    ControlloVKBB01    prenotazione    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    ${amount}    prenotazione
    #########

CALLBACK_GILBARCOX
    [Tags]    TPAYX
    dbConnection    CPDB32
    ${TRXESE}    Query    SELECT C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${TRXESE_var}    Get From Dictionary    ${TRXESE}[0]    C_IDE_TRX_ESE
    ${callback_json}    evaluate    json.loads('''${body_Callback_Gilbarco}''')    json
    Set to Dictionary    ${callback_json}    correlationId=${TRXESE_var}
    Create Session    mysession    ${endpoint_callbackGilbarco}
    ${response}=    POST On Session    mysession    ${basePath_callbackGilbarco}    json=${callback_json}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='30' and I_TOT=0.10 and C_SUP=${obuID}    1
    Disconnect from Database
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH07_LOG_MER_Q8 WHERE date(D_INS)= date(CURRENT timestamp) AND T_PAY_IN LIKE '%${TRXESE_var}%'    1
    Disconnect from Database
    ControlloVKBB01    conferma    ${C_SER}    ${DAUT_str}    ${transactionID}    ${obuID}
    ####################soglie servizio
    CheckSoglieServizio    10    conferma
    #########

CALLBACK_GILBARCOX_ZERO
    [Tags]    TPAYX
    ${checkin_json}    evaluate    json.loads('''${body_CheckIn_Gilbarco}''')    json
    ${output}    PostRequest    ${endpoint}    ${basePath_checkinGilbarcoX}    ${checkin_json}    ${access_token}    43.7991754    11.1783982    ${Product}
    ${tokenGilbarco}    Set Variable    ${output.json()['token']}
    Set Suite Variable    ${tokenCheckinGilbarco}    ${tokenGilbarco}
    ${order_json}    evaluate    json.loads('''${body_Order_Gilbarco}''')    json
    Set to Dictionary    ${order_json}    token=${tokenCheckinGilbarco}
    ${output}    PostRequest    ${endpoint}    ${basePath_orderGilbarcoX}    ${order_json}    ${access_token}    41.852461    12.4669735    ${Product}
    ${id_TRX}    Set Variable    ${output.json()['txId']}
    Set Suite Variable    ${transactionID}    ${id_TRX}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${id_TRX}' AND F_STA_TRX='10'    1
    Disconnect from Database
    dbConnection    CPDB32
    ${TRXESE}    Query    SELECT C_IDE_TRX_ESE FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}'    false    true
    ${TRXESE_var}    Get From Dictionary    ${TRXESE}[0]    C_IDE_TRX_ESE
    ${callback_json}    evaluate    json.loads('''${body_Callback_Gilbarco_ZERO}''')    json
    Set to Dictionary    ${callback_json}    correlationId=${TRXESE_var}
    Create Session    mysession    ${endpoint_callbackGilbarco}
    ${response}=    POST On Session    mysession    ${basePath_callbackGilbarco}    json=${callback_json}
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA05_TRX WHERE C_IDE_TRX='${transactionID}' AND F_STA_TRX='97' and I_TOT=0.10 and C_SUP=${obuID}    1
    Disconnect from Database
    dbConnection    CPDB18
    Row Count Is Equal To X    SELECT * FROM KTHA.TKTH07_LOG_MER_Q8 WHERE date(D_INS)= date(CURRENT timestamp) AND T_PAY_IN LIKE '%${TRXESE_var}%'    1
    Disconnect from Database
