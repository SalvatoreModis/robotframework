*** Settings ***
Resource          ../DATA/BasePathMulesoft.txt
Resource          ../DATA/Body.txt
Resource          ../KEYWORDS/LibraryDBParamRestReq.txt

*** Variables ***
${C_SER}          33
${obuID}          1000125623
${importoCB}      ${10}

*** Test Cases ***
SELEZIONA TRANSAZIONE
    DbConnection    CPDB32
    ${dati_trx_originaria}    Query    SELECT * FROM KDAA.TKDA05_TRX WHERE C_SER ='${C_SER}' AND C_SUP='${obuID}' AND F_STA_AUT='A' AND F_STA_TRX='30' AND C_IDE_TRX NOT IN (SELECT C_IDE_TRX_ORI FROM KDAA.TKDA16_TRX_SCN WHERE C_SER_ORI ='${C_SER}' AND C_SUP='${obuID}') ORDER BY D_INS DESC    false    true
    Disconnect From Database
    ${transactionID}    Get From Dictionary    ${dati_trx_originaria}[0]    C_IDE_TRX
    ${importo}    Get From Dictionary    ${dati_trx_originaria}[0]    I_TOT
    ${importo}    Evaluate    "%.2f" % $importo
    Set Suite Variable    ${transactionID}    ${transactionID}
    Set Suite Variable    ${importo}    ${importo}

SELEZIONA INIZIATIVA
    DbConnection    CPDB29
    @{iniziativa}    Query    SELECT C_INI, I_IMP_ERO_INI FROM KBBA.TKBB10_INI_SCN_SER WHERE C_SER ='${C_SER}' AND D_FIN > CURRENT date    false    true
    Disconnect From Database
    ${C_INI}    get from dictionary    ${iniziativa}[0]    C_INI
    ${IMP_ERO}    get from dictionary    ${iniziativa}[0]    I_IMP_ERO_INI
    Set Suite Variable    ${C_INI}    ${C_INI}
    Set Suite Variable    ${IMP_ERO}    ${IMP_ERO}

LOGIN
    ${endpoint}    Set Variable    https://ws-test.telepass.com
    ${output}    PostRequestLogin3part    ${endpoint}    ${basepath_login3part}    24
    ${token}    Set Variable    ${output.json()['access_token']}
    Set Suite Variable    ${access_token}    ${token}

CASHBACK
    ${endpoint}    Set Variable    https://ktqval81.test.gcp.telepass.com    #http://rtf-test.com/ktq-cashback    #
    ${basepath_cashback}    Set Variable    /api-third-party/v1/cashbacks
    ${cashback_json}    evaluate    json.loads('''${body_cashback}''')    json    #nel file body
    ${timestamp}    get time    epoch
    ${timestamp1}    get current date    result_format=%Y%m%d%H%M%S
    ${some_uuid}    Uuid 4
    ${iniziativa}    Convert To Integer    ${C_INI}
    Set to Dictionary    ${cashback_json}    transactionId=${transactionID}    titleCode=${obuID}    serviceCode=${C_SER}    cashbackAmount=${importoCB}    initiativeCode=${iniziativa}    cashbackId=${timestamp}.0005-${some_uuid}    cashbackRuleId=CB_${timestamp1}
    ${output}    PostRequestCB    ${endpoint}    ${basepath_cashback}    ${cashback_json}    ${access_token}
    ${importoCB_cent}    Evaluate    ${importoCB}*0.01
    ${importoCB_cent}    Evaluate    "%.2f" % $importoCB_cent
    Set Suite Variable    ${importoCB_cent}    ${importoCB_cent}

CONTROLLO INIZIATIVA
    DbConnection    CPDB29
    @{iniziativa}    Query    SELECT I_IMP_ERO_INI FROM KBBA.TKBB10_INI_SCN_SER WHERE C_INI=${C_INI}    false    true
    Disconnect From Database
    ${IMP_ERO_new}    get from dictionary    ${iniziativa}[0]    I_IMP_ERO_INI
    ${IMP_ERO}    Evaluate    ${IMP_ERO}+${importoCB_cent}
    Should Be Equal As Numbers    ${IMP_ERO}    ${IMP_ERO_new}

CONTROLLO VKBB01
    ${I_MOV}    Evaluate    -${importoCB_cent}
    dbConnection    CPDB29
    Row Count Is Equal To X    SELECT * FROM KBBA.VKBB01_MOV WHERE C_SER=24 AND C_IDE_TXN like 'KBB%${transactionID}' AND C_TIP_OPE_PAG='R ' AND I_MOV=${I_MOV} AND I_MOV_ORG=${importo}    1
    Disconnect from Database

CONTROLLO TKDA16
    dbConnection    CPDB32
    Row Count Is Equal To X    SELECT * FROM KDAA.TKDA16_TRX_SCN WHERE C_SUP='${obuID}' AND C_IDE_TRX_ORI='${transactionID}' AND C_INI='${C_INI}'    1
    Disconnect from Database

CONTROLLO BNL
    DbConnection    CPDB06
    ${query}    query    SELECT C_PRO FROM EVDA.TETSJU_ATT_PNG WHERE C_OBU_ID=${obuID}    false    true
    Disconnect From Database
    ${c_pro}    Get from Dictionary    ${query}[0]    C_PRO
    IF    "${c_pro}"=="FC" or "${c_pro}"=="FE" or "${c_pro}"=="FF"
    DbConnection    CPDB17
    Row Count Is Equal To X    SELECT * FROM KAAA.TKAA06_LOG_AUT_BNL WHERE C_WS_3PA='06' AND C_IDE_TRX_EXT LIKE 'KBB%${transactionID}' AND C_ESI='OK'    1
    Disconnect From Database
    END

*** Keywords ***
PostRequestCB
    [Arguments]    ${endpoint}    ${basepath}    ${body}    ${access_token}
    &{headers_CB}    Create Dictionary    Authorization=Bearer ${access_token}    Content-Type=application/json
    Create Session    mysession    ${endpoint}
    ${response}=    POST On Session    mysession    ${basepath}    headers=${headers_CB}    json=${body}
    Should Be Equal As Strings    ${response.status_code}    200
    ${res_body}    convert string to json    ${response.content}
    Should Be Equal As Strings    ${res_body['result']}    0
    [Return]    ${response}
